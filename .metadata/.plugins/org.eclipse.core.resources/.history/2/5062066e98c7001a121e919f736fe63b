#include "os.h"
#include "Key.h"
#include "sound.h"
#include "uart.h"
#include "hdlc.h"
#include "lcd.h"
#include "util.h"
#include "qspi.h"
#include "flash.h"
#include "fs_app.h"


#define  DUMPMASK       0x00ff                  /* dump button mask */
#define  KEYMASK        0x073f                  /* key mask */
#define  K_DUMP         0x0080                  /* dump button */
#define  K_AUTO         0x0008
#define  K_PROG         0x0004
#define  K_GOTO         0x0002
#define  K_EMER         0x0001
#define  K_MUTE         0x0010
#define  K_BACK         0x0110
#define  K_SKIP         0x0210
#define  K_REPEAT       0x0310
#define  K_CANCEL       0x0410
#define  K_PLAY         0x0040
#define  CR             0x0d
#define  NO_DUMP_KEY    0x00ff

volatile uint32_t Sflags = 0 ;
volatile uint32_t Status = 0 ;

typedef  struct
   {
   const uint32_t  scancode ;
   const uint32_t  key ;
   }  KEYMAP ;


#define  TXBUFSZ               64
#define  BLKSZ                 56            // Transfer block size

#define  OCU_TIMEOUT           50            // Inter-character timeout

// Probing state and macros to maintain
/*
static   FPTR0  prb_state = NULL;
#define  next_state(ptr)   (prb_state = (FPTR)(ptr))
#define  null_state()      (prb_state = (FPTR)NULL)
*/

#define  INPUT_BUF_SIZE       32
typedef struct
   {
   uint32_t  idx ;
   uint32_t  nBytes ;
   uint8_t   buf[INPUT_BUF_SIZE] ;
   } INPUT_BUF ;

static INPUT_BUF  InputBuf ;

typedef struct
   {
   uint32_t  S ;                                 // SEND sequence number they expect from us (their R)
   uint32_t  R ;                                 // RECV sequence number we expect from them (their S)
   uint32_t  msgSize ;                           // Data length
   } HDLC_MSG ;

static HDLC_MSG msg ;                           // Input message header


static INLINE void ProcMsg(uint8_t*, uint32_t) ;
static INLINE void TransmitStatus(void);
static INLINE uint32_t SendFrames(void) ;

static const char* const logo_string_new  = "\\B\x15\x16\x17\x18\x19\x1A\x1B\x1C" ;
static const char* const logo_string_old  = "\\B\x01\x02\x03\x04\x05\x06\x07" ;
static const char* const logo_string_gfi  = "\\BGFi" ;
static const char* const logo_string_skip = "\\B\x14\x08\x0F\x10\x11\x12\x13" ;


/* Defines and typedefs */
#define  SYNC              (uint8_t)0xfe           /* sync char. (2 in a row) */
#define  FINAL             (uint8_t)0x10           /* final frame flag */
#define  SABM              (uint8_t)0x2f           /* set async balanced mode */
#define  RR                (uint8_t)0x01           /* receiver ready */
#define  RNR               (uint8_t)0x05           /* receiver not ready */
#define  REJ               (uint8_t)0x09           /* reject */
#define  MODULUS           8                    /* 7 frames (8-1) at once */

#define  Q_BUF_SZ          64                    /* read buffer size */
#define  TX_QUE_SZ         16                   /* max. TX  queued messages */

/* Flags contained in "Flags" and macros to manipulated them.
*/
#define  F_OFFLINE         0x0001               /* currently not probing     */
#define  F_MEMCLR          0x0002               /* memory clear   pending    */
#define  F_SABM            0x0004               /* SABM           pending    */
#define  F_RNR             0x0008               /* RNR            pending    */
#define  F_REJ             0x0010               /* REJ            pending    */
#define  F_RX_FLOWCNTL     0x0020               /* RX flow control in effect */
#define  F_TX_FLOWCNTL     0x0040               /* TX flow control in effect */
#define  F_DISCARD         0x0080               /* if we should discard remaining frames */


typedef struct
   {
   uint8_t nrns ;
   uint8_t count ;
   }  HEADER;

typedef  struct
   {
   uint32_t    sz;                              /* message size */
   uint8_t     b[Q_BUF_SZ];                     /* message buffer */
   }  BUF;

typedef struct
   {
   uint32_t    next;                            /* index to next */
   uint32_t    last;                            /* index to last */
   volatile uint32_t  cnt;                      /* count of messages queued */
   BUF         q[TX_QUE_SZ];                    /* message queue */
   } TXQUE;

/* Macros */
#define  AppendLrc( c )    (Lrc ^= (uint8_t)(c))
#define  IncSeq( n )       ((n) = (++(n)<MODULUS)?(n):0)
#define  NextRx( fptr )    (RxState = (fptr))
#define  SetFlag( s )      (Flags |= (s))
#define  ClrFlag( s )      (Flags &=~(s))
#define  TstFlag( s )      (Flags &  (s))
#define  GetNs( b )        ( ((b) >> 1) & 0x07 );
#define  GetNr( b )        ( ((b) >> 5) & 0x07 );

/* Prototypes  */
static void  Frame(uint8_t, BUF*, uint8_t) ;

/* module data */
static   uint32_t        S  = 0 ;                /* current send state */
static   uint32_t        R  = 0 ;                /* current receive state */
static   uint32_t        Fs = 0 ;

static   TXQUE          TxQ;                     /* tx message queue */
static   uint32_t       Flags =  0;              // maintained by macros below

__attribute__((aligned(4))) static uint8_t RX_BUF[RX_BUF_SZ] ;         // Receive buffer

#define Nputs(s, n)    Uart2_Put(s, n)

static HANDLE  QueueMutex = NULL ;               // TX queue access mutex

static OS_THREAD_STACK HdlcStack[HDLC_STACK_SIZE] ;

//static uint32_t  ColorBlue, ColorDarkBlue, ColorGray, ColorMask ;
#define MAX_SHIFT       140
#define FRAME_DELAY     60
#define SCREEN_DELAY    5000
static void ScreenMove(void) ;

//static uint32_t  NewBaudRate ;
static uint16_t  ScreenPos, MoveDirection ;

OCU_PARAMS OCUParameters ;

uint8_t    OCU_Debug = 0, TX_Debug = 0 ;

// New firmware buffer in D2 memory (no data cache)
NO_CACHE uint8_t fw_buf[MAX_PROGRAM_SIZE+8] ;


// Start screen shifting in screen saver mode
static void ScreenWait(void)
   {
   // Create auto re-load timer, so it will fire at every FRAME_DELAY interval
   CreateTmr(SCREEN_SAVER_TMR|TIMER_CREATE_AUTORELOAD|TIMER_CREATE_START, FRAME_DELAY, ScreenMove, NULL) ;
   }


// One step of screen shifting. Called every FRAME_DELAY ms interval
static void ScreenMove(void)
   {
   if (MoveDirection)
      {
      if (ScreenPos == 0)
         MoveDirection = 0 ;
      else
         ScreenPos-- ;
      }
   else
      {
      if (ScreenPos == MAX_SHIFT)
         MoveDirection = 1 ;
      else
         ScreenPos++ ;
      } ;
   LCD_ShiftScreen(ScreenPos) ;
   // After full cycle is done, wait for SCREEN_DELAY ms before re-starting the move
   if (ScreenPos == 0) CreateTmr(SCREEN_SAVER_TMR|TIMER_CREATE_START, SCREEN_DELAY, ScreenWait, NULL) ;
   }


static void DefaultParams(void)
   {
   memset(&OCUParameters, 0, sizeof(OCUParameters)) ;
   OCUParameters.BaudRate = 38400 ;
   OCUParameters.Brightness = MAX_DURATION ;
   OCUParameters.ColorSheme = 1 ;
   OCUParameters.crc = crc16((uint8_t*)&OCUParameters, sizeof(OCUParameters)-2) ;
   }


// Read OCU configuration parameters from file
int ReadConfig(void)
   {
   FRESULT  rc ;
   register uint32_t len ;
   register FIL     *fd = NULL ;

   // Read current baud rate from file
   rc = fs_mount("0") ;
   if (rc != FR_OK)
      {
      DbgPrintf("Error %u mounting volume\r\n", rc) ;
      DefaultParams() ;
      goto EXIT ;
      } ;
   fd = fs_open(CONFIG_FILE_NAME, FA_READ|FA_WRITE|FA_OPEN_ALWAYS, &rc) ;
   if (rc != FR_OK)
      {
      DbgPrintf("Error %u creating file\r\n", rc) ;
      DefaultParams() ;
      goto EXIT ;
      } ;
   len = f_size(fd) ;
   if (len != sizeof(OCUParameters))
      {
      DbgPrintf("Parameters length %u was not valid\r\n", len) ;
      goto NEW ;
      } ;
   rc = fs_read(fd, &OCUParameters, sizeof(OCUParameters)) ;
   if (rc != FR_OK)
      {
      DbgPrintf("Error %u reading file\r\n", rc) ;
      goto NEW ;
      } ;
   if (!crc16((uint8_t*)&OCUParameters, sizeof(OCUParameters))) goto EXIT ;
   DbgPrintf("Parameters CRC was not valid\r\n") ;

 NEW:
   DefaultParams() ;

   rc = f_lseek(fd, 0) ;
   rc = f_truncate(fd) ;
   if (rc != FR_OK)
      {
      DbgPrintf("Error %u truncating file\r\n", rc) ;
      goto EXIT ;
      } ;

   rc = fs_write(fd, &OCUParameters, sizeof(OCUParameters)) ;
   if (rc != FR_OK) DbgPrintf("Error %u writing file\r\n", rc) ;

  EXIT:
   if (fd) fs_close(fd) ;
   fs_unmount("0") ;
   return rc ;
   }


static void RenderQueueAdd(RENDER *q)
   {
   register uint32_t  cnt ;

   __disable_irq() ;
   cnt = RenderQueue.cnt ;
   if (cnt < RND_QUEUE_SZ)
      {
      memcpy(&RenderQueue.q[RenderQueue.last], q, sizeof(RENDER)) ;
      RenderQueue.last = (RenderQueue.last + 1) & (RND_QUEUE_SZ - 1) ;
      RenderQueue.cnt = cnt + 1 ;
      // Enable LCD interrupt, so we would start rendering
      LTDC->IER |= LTDC_IT_LI ;
      } ;
   __enable_irq() ;
   }


// Convert "%%" into "%"
static INLINE uint32_t UnEscapePercent(char *s, uint32_t sz)
   {
   register uint32_t len = 0 ;
   register char    *p = s ;

   do
      {
      register uint8_t  c = *s++ ;
      if (c == 0) break ;
      if (c == '%' && sz > 1)
         {
         sz-- ;
         c = *s++ ;
         if (c != '%')
            {
            *p++ = '%' ;
            len++ ;
            } ;
         } ;
      *p++ = c ;
      len++ ;
      }
   while (--sz) ;
   *p = 0 ;
   return len ;
   }


void LCDClear(void)
   {
   RENDER   q ;

   q.msgType = RND_MSG_CLEAR ;
   RenderQueueAdd(&q) ;
   }


void LCDprintf(uint16_t x, uint16_t y, char *pText, uint32_t len)
   {
   RENDER   q ;
   register uint32_t  f, k ;

   q.x = x * FONT_X_SMALL ;                    // X coordinate in pixels
   q.y = (y * FONT_Y_BIG) >> 1 ;               // Y coordinate in pixels
   // Check for Logo strings
   if (!strcmp(pText, logo_string_skip)) return ;
   if (!strcmp(pText, logo_string_new) || !strcmp(pText, logo_string_old) || !strcmp(pText, logo_string_gfi))
      {
      q.msgType = RND_MSG_LOGO ;
      if (OCU_Debug) DbgPrintf("Logo\r\n") ;
      goto FNC_END ;
      } ;
   q.msgType = RND_MSG_TXT ;
   q.pFont = &GUI_FontSwis721CnBT105 ;
   len = UnEscapePercent(pText, len) ;
   if (len == 0) return ;
   q.FontColor = FONT_COLOR ;
   q.BackColor = BACK_COLOR ;
   q.Reverse = 0 ;
   while (len > 1 && pText[0] == '\\')
      {
      switch (pText[1])
         {
         case 'S':
            q.pFont = &GUI_FontConsolas21x45 ;
            q.y = (y * FONT_Y_SMALL) >> 1 ;
            break ;

         case 'N':
            q.pFont = &GUI_FontConsolas21x30 ;
            q.y = y * 30 ;
            break ;

         case 'B':
            q.pFont = &GUI_FontSwis_BIG ;
            q.y = (y * FONT_Y_BIG) >> 1 ;
            break ;

         case 'R':
            q.Reverse = 1 ;
            break ;

         // Up to 3 digit font color code
         case 'F':
            f = k = 0 ;
            while (len > 2 && k < 3)
               {
               register uint8_t c = pText[2] ;
               if (c < '0' || c > '9') break ;
               f = 10 * f + (c - '0') ;
               len-- ;
               k++ ;
               pText++ ;
               } ;
            q.FontColor = f ;
            break ;

         // Up to 3 digit background color code
         case 'G':
            f = k = 0 ;
            while (len > 2 && k < 3)
               {
               register uint8_t c = pText[2] ;
               if (c < '0' || c > '9') break ;
               f = 10 * f + (c - '0') ;
               len-- ;
               k++ ;
               pText++ ;
               } ;
            q.BackColor = f ;
            break ;

         default:
            goto DONE ;
         } ;

      len -= 2 ;
      pText += 2 ;
      } ;
   if (len == 0) return ;
 DONE:
   q.sz = len ;
   memcpy(q.buf, pText, len + 1) ;
   if (OCU_Debug) DbgPrintf("x=%u, y=%u, sz=%u, msg: \"%s\"\r\n", q.x, q.y, q.sz, q.buf) ;
 FNC_END:
   RenderQueueAdd(&q) ;
   }


void oprintf(int16_t x, int16_t y, char *fmt, ...)
   {
   char    text[40] ;
   va_list my_list ;
   register int16_t len ;

   va_start(my_list, fmt) ;
   len = (int16_t)vsnprintf(text, sizeof(text), fmt, my_list) ;
   va_end(my_list) ;

   if (len == 0) return ;
   if (len >= sizeof(text)) len = sizeof(text) - 1 ;

   if (x == OCU_C || x == OCU_R)
      {
      register char *p = text ;
      register uint32_t n = len ;
      register char  c ;

      while (1)
         {
         c = *p ;
         if (c != '\\') break ;
         c = p[1] ;
         if (c == 'S' || c == 'N' || c == 'R' || c == 'B')
            {
            n -= 2 ;
            if (n <= 0) return ;
            p += 2 ;
            }
         else
            break ;
         } ;

      if (x == OCU_C)
         x = (OCU_R - n) >> 1 ;
      else
         x = OCU_R - n ;
      } ;

   if (y == OCU_B && strstr(text, "\\N")) y -= 4 ;

   LCDprintf(x, y, text, len) ;
   }


int InitializePPSProtocol(void)
   {
   int      err ;

   //OCU_Debug = 1 ;
   //TX_Debug = 1 ;
   crc_table32() ;
   // Initialize queues
   memset(&TxQ, 0, sizeof(TxQ)) ;
   // TX queue access mutex
   if (QueueMutex == NULL)
      {
      QueueMutex = OS_MutexCreate(FALSE, &err) ;
      if (QueueMutex == NULL) return err ;
      } ;
   // Read current baud rate from file
   if (ReadConfig() != FR_OK) return 1 ;
   // Start UART driver
   err = UART2_Init(57600) ;
   if (err) return err ;
   // Reset protocol variables
   Flags =  F_SABM ;
   S = R = Fs = 0 ;
//   prb_state = NULL ;
   Status = 0 ;
   InputBuf.nBytes = 0 ;
   // Queue first status messages from this device
   //OCUStatus() ;
   Set_Sflag(S_OCU_OFFLINE) ;
   //if (OCUParameters.UsingLED) Set_Sflag(S_USING_LED) ;
   oprintf(OCU_C, OCU_T, "\\SCOMPONENT TESTER - %05lu-%03d", Part_No, Version) ;
   oprintf(OCU_L+3, OCU_T+2, "\\BGFi") ;
   oprintf(OCU_L+12, OCU_T+12, "\\SAN SPX DIVISION") ;
   oprintf(OCU_C, OCU_B, "\\S%s SPX/Genfare", Copyright) ;

   // Start communication thread
   return OS_ThreadStart(HDLC, HDLC_PRIO, HdlcStack, HDLC_STACK_SIZE, NULL, NULL) ;
   }


// Process data message from the host
static INLINE void ProcMsg(uint8_t *s, uint32_t n)
   {
   RENDER   q ;
   register uint32_t  y, f, k ;

   switch (s[1])
      {
      case TRIM_OCU_CLR:
         q.msgType = RND_MSG_CLEAR ;
         break ;

      case TRIM_OCU_BOX:
         if (n < sizeof(TRIM_BOX))
            {
            DbgPrintf("Invalid size %u for BOX command\r\n", n) ;
            return ;
            } ;
         q.msgType = RND_MSG_BOX ;
         q.x  = ((TRIM_BOX*)s)->x ;
         q.y  = ((TRIM_BOX*)s)->y ;
         q.x2 = ((TRIM_BOX*)s)->x2 ;
         q.y2 = ((TRIM_BOX*)s)->y2 ;
         q.FontColor = FONT_COLOR ;
         q.BackColor = BACK_COLOR ;
         if (OCU_Debug) DbgPrintf("BOX x=%u, y=%u, x2=%u, y2=%u\r\n", q.x, q.y, q.x2, q.y2) ;
         break ;

      case TRIM_OCU_TEXT:
         if (n < 5 || n > sizeof(TRIM_TEXT))
            {
            DbgPrintf("Invalid size %u for TEXT command\r\n", n) ;
            return ;
            } ;
         s[n] = 0 ;
         q.x = ((TRIM_TEXT*)s)->x * FONT_X_SMALL ;    // X coordinate
         y = ((TRIM_TEXT*)s)->y ;                     // Y coordinate
         if (y > 19) return ;
         q.y = (y * FONT_Y_SMALL) >> 1 ;

         // Check for Logo strings
         if (!strcmp(((TRIM_TEXT*)s)->text, logo_string_skip)) return ;
         if (!strcmp(((TRIM_TEXT*)s)->text, logo_string_new) || !strcmp(((TRIM_TEXT*)s)->text, logo_string_old) || !strcmp(((TRIM_TEXT*)s)->text, logo_string_gfi))
            {
            q.msgType = RND_MSG_LOGO ;
            if (OCU_Debug) DbgPrintf("Logo\r\n") ;
            break ;
            } ;

         q.msgType = RND_MSG_TXT ;
         n = UnEscapePercent(((TRIM_TEXT*)s)->text, n - 4) ;              // Unescape and length
         if (n == 0) return ;
         s = (uint8_t*)((TRIM_TEXT*)s)->text ;
         q.pFont = &GUI_FontConsolas21x45 ;           // Default font "S"
         q.FontColor = FONT_COLOR ;                   // Default color
         q.BackColor = BACK_COLOR ;
         q.Reverse = 0 ;                              // No reverse

         while (n > 1 && s[0] == '\\')
            {
            switch (s[1])
               {
               case 'B':
                  if (q.x)
                     {
                     q.pFont = &GUI_FontSwis721CnBT105 ;
                     q.y = (y * FONT_Y_BIG) >> 1 ;
                     } ;
                  break ;

               case 'R':
                  q.Reverse = 1 ;
                  break ;

               // Up to 3 digit font color code
               case 'F':
                  f = k = 0 ;
                  while (n > 2 && k < 3)
                     {
                     register uint8_t c = s[2] ;
                     if (c < '0' || c > '9') break ;
                     f = 10 * f + (c - '0') ;
                     n-- ;
                     k++ ;
                     s++ ;
                     } ;
                  q.FontColor = f ;
                  break ;

               // Up to 3 digit background color code
               case 'G':
                  f = k = 0 ;
                  while (n > 2 && k < 3)
                     {
                     register uint8_t c = s[2] ;
                     if (c < '0' || c > '9') break ;
                     f = 10 * f + (c - '0') ;
                     n-- ;
                     k++ ;
                     s++ ;
                     } ;
                  q.BackColor = f ;
                  break ;

               default:
                  goto DONE ;
               } ;

            n -= 2 ;
            s += 2 ;
            } ;
         if (n == 0) return ;
       DONE:
         q.sz = n ;
         memcpy(q.buf, s, n + 1) ;
         if (OCU_Debug) DbgPrintf("x=%u, y=%u, sz=%u, msg: \"%s\"\r\n", q.x, q.y, q.sz, q.buf) ;
         break ;

      default:
         return ;
      } ;
   RenderQueueAdd(&q) ;
   }


int TrimDiagnostic(uint8_t cmd, uint8_t key, uint8_t spare)
   {
   register TRIM_MSG  *m ;

   // Obtain exclusive access to TX queue
   if (OS_MutexClaim(QueueMutex, INFINITY)) return -1 ;
   // Exit, if queue is full (should never happen)
   if (TxQ.cnt == TX_QUE_SZ)
      {
      OS_MutexRelease(QueueMutex) ;
      DbgPrintf("TX queue is full\r\n") ;
      return -1 ;
      } ;

   m = (TRIM_MSG*)TxQ.q[TxQ.next].b ;
   TxQ.q[TxQ.next].sz = sizeof(m) ;
   TxQ.next = (TxQ.next + 1) & (TX_QUE_SZ - 1) ;

   m->dest = TRIM_ID ;
   m->src  = OCU_ID ;

   if (cmd == TRIMCLRJAM)
      {
      m->cmd  = cmd ;
      m->fnct = m->key = m->spar = 0 ;
      }
   else
      {
      m->cmd  = TRIM_DIAGNOSTIC_MODE ;
      m->fnct = cmd ;
      m->key  = key ;
      m->spar = spare ;
      } ;

   TxQ.cnt++ ;
   OS_MutexRelease(QueueMutex) ;
   return 0 ;
   }


/*----------------------------------------------\
|  Function:   TransmitStatus()                 |
|  Purpose :   send special frame when no data  |
|  Synopsis:   int   TransmitStatus();          |
|  Input   :   None.                            |
|  Output  :   always zero.                     |
|  Comments:                                    |
\----------------------------------------------*/
static INLINE void TransmitStatus(void)
   {
   uint8_t b[4] ;
   register uint8_t  c ;

   b[0] = b[1] = SYNC ;
   if (TstFlag(F_SABM))                     /* SABM pending ? */
      {
      c = SABM ;
      S = R = Fs = 0 ;
      ClrFlag(F_SABM) ;
      DbgPrintf("Sending SABM\r\n") ;
      }
   else
      c = (R << 5) | RR ;         /* default to receive reader */

   b[3] = b[2] = c ;
   if (TX_Debug) DbgPrintf("Status R=%u S=%u: %02X %02X %02X %02X\r\n", R, S, b[0], b[1], b[2], b[3]) ;
   Nputs(b, 4) ;
   Uart2_WaitIdle() ;
   }



/*-------------------------------------------------------------\
|  Function:   Frame( S, R, p, f );                            |
|  Purpose :   Put queued up frames into the transmit buffer.  |
|  Synopsis:   int error = frame( S, R, p, f );                |
|              p - (BUF *) points to the data                  |
|              S - (uint8_t) send sequence number (N(s))       |
|              R - (uint8_t) receive sequence number (N(r))    |
|              f - (uint8_t) final frame flag                  |
|  Return  :                                                   |
|                                                              |
|  Comments:   The "lrc" will be calculated as well as the     |
|              mapping of the N(s), N(r) sequence numbers.     |
\-------------------------------------------------------------*/
static void Frame(uint8_t S, BUF *p, uint8_t f)
   {
   register uint8_t  *lpBuf, *s ;
   register uint8_t   lrc ;
   register uint32_t  len, size ;
   static   uint8_t   TxBuf[Q_BUF_SZ + 6] ;

   lpBuf = TxBuf ;
   *lpBuf++ = SYNC ;                     /* synchronization */
   *lpBuf++ = SYNC ;
   lrc = (R << 5) | (S << 1) | f ;
   *lpBuf++ = lrc ;
   s = p->b ;
   len = p->sz ;
   size = len + 5 ;
   *lpBuf++ = (uint8_t)len ;
   lrc ^= (uint8_t)len ;
   while (len--)
      {
      register uint8_t  c = *s++ ;
      lrc ^= c ;
      *lpBuf++ = c ;
      } ;
   *lpBuf = lrc ;
   Nputs(TxBuf, size) ;
   if (TX_Debug)
      {
      DbgPrintf("R=%u, S=%u: ", R, S) ;
      len = 0 ;
      do
         {
         DbgPrintf("%02X ", TxBuf[len++]) ;
         }
      while (--size) ;
      DbgPrintf("\r\n") ;
      } ;
   }


static INLINE uint32_t SendFrames(void)
   {
   register uint32_t  fs ;       // count of frames sent
   register uint32_t  ndx ;      // queue entry index
   register uint8_t   Ns ;       // send sequence number

   if (TxQ.cnt == 0 || TstFlag(F_SABM)) return 0 ;
   fs = 0 ;
   Ns = S ;
   ndx = TxQ.last ;
   while (1)
      {
      fs++ ;
      if (fs == 7 || fs == TxQ.cnt)
         {
         /* Last frame */
         Frame(Ns, &TxQ.q[ndx], FINAL) ;
         Uart2_WaitIdle() ;
         return fs ;
         } ;
      Frame(Ns, &TxQ.q[ndx], 0) ;
      Ns = (Ns + 1) & 0x7 ;
      ndx = (ndx + 1) & (TX_QUE_SZ - 1) ;
      } ;
   }



static int GetByte(uint32_t timeout)
   {
   if (InputBuf.nBytes == 0)
      {
      InputBuf.nBytes = Uart2_Get(InputBuf.buf, sizeof(InputBuf.buf), timeout) ;
      if (InputBuf.nBytes == 0)
         {
         DbgPrintf("OCU timeout\r\n") ;
         return -1 ;
         } ;
      InputBuf.idx = 0 ;
      } ;
   InputBuf.nBytes-- ;
   return (uint32_t)(InputBuf.buf[InputBuf.idx++]) ;
   } ;


/*******************************************************************************
 *
 *                        Get single HDLC message
 *   Returns 0, if message was not received within specified timeout
 *   Returns 1, if message was received, but this is not a final frame
 *   Returns 2, if final frame was received
 *   Returns 3, if S-frame was received
 *
 *******************************************************************************/
static INLINE uint32_t GetHDLCMsg(HDLC_MSG *pMsg, uint32_t bFlag)
   {
   register uint32_t StartTime = GetTickCount() ;
   register int      c ;
   register uint8_t  Lrc, RS ;
   register uint32_t RxCnt ;
   register uint32_t timeout ;

   if (bFlag)
      timeout = 250 ;
   else
      timeout = 500 ;

   while (1)
      {
    START:
      RxCnt = TicksSince(StartTime) ;
      if (RxCnt >= timeout) return 0 ;
      // Wait for message to start
      c = GetByte(timeout - RxCnt) ;
      if (c < 0) continue ;
      if (c != SYNC)
         {
         DbgPrintf("SYNC1 <> %X\r\n", c) ;
         continue ;
         } ;
      // SYNC
      c = GetByte(OCU_TIMEOUT) ;
      if (c < 0) continue ;
      if (c != SYNC)
         {
         DbgPrintf("SYNC2 <> %X\r\n", c) ;
         continue ;
         } ;
      // NrNs
      c = GetByte(OCU_TIMEOUT) ;
      if (c < 0) continue ;
      pMsg->S = (c >> 5) & 0x7 ;
      if (c & 0x01)
         {
         // S-frame
         pMsg->R = c ;
         c = GetByte(OCU_TIMEOUT) ;
         if (c < 0) continue ;
         if (c == pMsg->R) return 3 ;
         SetFlag(F_DISCARD) ;
         DbgPrintf("Lrc %X <> %X\r\n", pMsg->R, c) ;
         continue ;
         } ;

      // I-frame
      pMsg->R = (c >> 1) & 0x7 ;
      Lrc = (uint8_t)c ;
      // RS is set to 2, if this is final frame
      RS = 1 + ((c >> 4) & 0x1) ;
      // Message length
      RxCnt = GetByte(OCU_TIMEOUT) ;
      if ((int)RxCnt < 0) continue ;
      if (RxCnt > RX_BUF_SZ)
         {
         DbgPrintf("Invalid length %u\r\n", RxCnt) ;
         continue ;
         } ;
      AppendLrc(RxCnt) ;
      pMsg->msgSize = 0 ;
      // Read data
      while (RxCnt--)
         {
         c = GetByte(OCU_TIMEOUT) ;
         if (c < 0) goto START ;
         AppendLrc(c) ;
         RX_BUF[pMsg->msgSize++] = (uint8_t)c ;
         } ;

      // Lrc
      c = GetByte(OCU_TIMEOUT) ;
      if (c < 0) continue ;
      if ((uint8_t)c != Lrc)
         {
         SetFlag(F_DISCARD) ;
         DbgPrintf("Lrc %X <> %X\r\n", Lrc, c) ;
         continue ;
         } ;

      return RS ;
      } ;
   }



/*****************************************************************************
 *
 *                    HDLC Communication Protocol Thread
 *
 *   This is totally passive side of the protocol.  This device will only
 *   respond to correctly formatted frames.  All time-outs should be handled
 *   by the active side (FBX).
 *
 *   The device waits for message to start.  When new message starts, 75 ms
 *   time-out is allowed between characters of the message.  The protocol
 *   resets back to its idle state (wait for message to start), if this
 *   time-out occurs.
 *
 *******************************************************************************/
void HDLC(void *dummy)
   {
   register uint32_t  rc, nMsg ;

   while (1)
      {
      ClrFlag(F_DISCARD) ;
      nMsg = 0 ;
      // Read all incoming messages
      while (1)
         {
         rc = GetHDLCMsg(&msg, nMsg) ;
         // Timeout?
         if (rc == 0) break ;
         // New message
         nMsg = 1 ;
         if (rc == 3)
            {
            // S-frame
            if (msg.R == SABM)
               {
               S = R = Fs = 0 ;
               DbgPrintf("SABM received\r\n") ;
               break ;
               } ;
            if (TX_Debug) DbgPrintf("S-frame: msg.S=%u S=%u\r\n", msg.S, S) ;
            }
         else
            {
            if (TX_Debug) DbgPrintf("msg.R=%u R=%u, msg.S=%u S=%u\r\n", msg.R, R, msg.S, S) ;
            if (!TstFlag(F_DISCARD))
               {
               if (msg.R == R)
                  {
                  // Increment receive number
                  R = (R + 1) & 0x7 ;
                  // Process received frame
                  ProcMsg(RX_BUF, msg.msgSize) ;
                  }
               else
                  {
                  SetFlag(F_DISCARD) ;
                  DbgPrintf("msg.R=%u <> R=%u, msg.S=%u S=%u\r\n", msg.R, R, msg.S, S) ;
                  } ;
               } ;
            } ;
         // Delete our confirmed frames
         if (Fs)
            {
            register uint32_t  Len ;

            if (msg.S < S)
               Len = 8 + msg.S - S ;
            else
               Len = msg.S - S ;
            if (Len > Fs)
               {
               DbgPrintf("Asked to remove %u frames? Have %u\r\n", Len, Fs) ;
               SetFlag(F_SABM) ;
               }
            else
               {
               S = (S + Len) & 0x7 ;
               OS_MutexClaim(QueueMutex, INFINITY) ;
               if (TxQ.cnt < Len) Len = TxQ.cnt ;
               TxQ.last = (TxQ.last + Len) & (TX_QUE_SZ - 1) ;
               if (TX_Debug) DbgPrintf("Removing %u frames out of %u\r\n", Len, TxQ.cnt) ;
               TxQ.cnt -= Len ;
               OS_MutexRelease(QueueMutex) ;
               } ;
            Fs = 0 ;
            } ;
         /*  Continue, if not the last frame */
         if (rc != 1) break ;
         } ;

      // Timeout?
      if (rc == 0)
         TransmitStatus() ;
      // End of packet
      else if (!(Fs = SendFrames()))
         {
         // Nothing to transfer
         OS_Sleep(250) ;
         TransmitStatus() ;
         } ;
      } ;
   }


#if 0
// This is called when baud rate display timer is expired
void ExitBaudRateChange(void)
   {
   SetBackLite(BackLiteLevel) ;
   clr_status(OCU_DISPLAY_BAUD|OCU_BAUD_SELECTED) ;
   RenderQueue.q[RenderQueue.last].msgType = RND_MSG_CLEAR ;
   RenderQueueAdd() ;
   }


static void ShowCurrentBaudRate(uint32_t OCUBaudRate)
   {
   register RENDER  *q ;

   RenderQueue.q[RenderQueue.last].msgType = RND_MSG_CLEAR ;
   RenderQueueAdd() ;
   q = &RenderQueue.q[RenderQueue.last] ;
   q->msgType = RND_MSG_TXT ;
   q->pFont = &GUI_FontSwis_BIG ;
   q->y = 120 ;
   q->sz = sprintf((char*)(q->buf), "%lu", OCUBaudRate) ;
   q->x = (DISPLAY_WIDTH - q->sz * GUI_FontSwis_BIG.CellWidth) >> 1 ;
   q->BackColor = BACK_COLOR ;
   q->FontColor = tst_status(OCU_BAUD_SELECTED) ? COLOR_RED : FONT_COLOR ;
   q->Reverse = 0 ;
   RenderQueueAdd() ;
   }


// Display/Change OCU baud rate
void ProcessBaudRateChange(void)
   {
   if (!tst_status(OCU_DISPLAY_BAUD))
      {
      set_status(OCU_DISPLAY_BAUD) ;
      SetBackLite(7) ;
      NewBaudRate = OCUParameters.BaudRate ;
      }
   else
      {
      switch (NewBaudRate)
         {
         case 38400:
            NewBaudRate = 57600 ;
            break ;
         case 57600:
            NewBaudRate = 115200 ;
            break ;
         case 115200:
            NewBaudRate = 38400 ;
         } ;
      } ;
   clr_status(OCU_BAUD_SELECTED) ;
   ShowCurrentBaudRate(NewBaudRate) ;
   }



void  SelectNewBaudRate(void)
   {
   if (!tst_status(OCU_DISPLAY_BAUD) || tst_status(OCU_BAUD_SELECTED)) return ;
   set_status(OCU_BAUD_SELECTED) ;
   ShowCurrentBaudRate(NewBaudRate) ;
   }
#endif
