#include "os.h"
#include "qspi.h"

extern uint32_t MoveCRC(const uint8_t *p, uint32_t cnt, void *DR) ;

#define MDMA_CHAN             MDMA_Channel0                    // Will use MDMA channel 0 for QSPI controller
#define MDMA_CRC              MDMA_Channel1                    // Will use MDMA channel 1 for CRC controller

#define QUADSPI_CR            ((0 << 24) | QUADSPI_CR_EN)      // Assuming external clock is 100 MHz. So divisor is 0 ==> QSPI frequency is 100 MHz

static HANDLE  QSPI_EventReady = NULL ;
// Global synchronization mutex to control access to CRC controller
static HANDLE  CRC_Mutex = NULL ;
static HANDLE  CRC_EventReady = NULL ;

static volatile uint8_t  TransferStatus ;
static volatile uint8_t  CRCStatus ;
static uint8_t  MDMA_Init = 0 ;

#define ulPolynomial       (uint32_t)0x04c11db7



#define IsRegionD2(addr)      (((uint32_t)addr & ~0x3FFFF)==0x30000000)


/******************************************************************************
 *
 * QUADSPI GPIO Configuration:
 *   PE2     ------> IO2
 *   PB6     ------> CS
 *   PB2     ------> CLK
 *   PD11    ------> IO0
 *   PD12    ------> IO1
 *   PD13    ------> IO3
 *
 *   Flash: W25Q128JV - 16 MB
 ******************************************************************************/
int QSPI_Init(void)
   {
   GPIO_InitTypeDef GPIO_InitStruct ;
   int  err ;

   // Already initialized?
   if (QSPI_EventReady) return 0 ;

   // Enable clock
   __HAL_RCC_QSPI_CLK_ENABLE() ;
   // Disable controller
   QUADSPI->CR = 0 ;

   // Configure pins
   GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
   GPIO_InitStruct.Alternate = GPIO_AF9_QUADSPI;

   GPIO_InitStruct.Pin = GPIO_PIN_2;
   HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

   //GPIO_InitStruct.Pin = GPIO_PIN_2;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13;
   HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

   GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
   GPIO_InitStruct.Pin = GPIO_PIN_6;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   // Flash size and CS mode (5 clocks between commands)
   QUADSPI->DCR = (23 << 16) | (4 << 8) ;

   // Initialize MDMA
   MDMA_Enable() ;

   // Enable QSPI interrupts
   HAL_NVIC_SetPriority(QUADSPI_IRQn, QUADSPI_IRQ_PRIO, 0) ;
   NVIC_EnableIRQ(QUADSPI_IRQn) ;

   // Create MDMA ready event
   QSPI_EventReady = OS_EventCreate(0, 0, &err) ;
   return err ;
   }


void QSPI_Disable(void)
   {
   if (QSPI_EventReady)
      {
      // Disable interrupts
      NVIC_DisableIRQ(QUADSPI_IRQn) ;
      // Disable controller
      QUADSPI->CR = 0 ;
      // Disable clock
      __HAL_RCC_QSPI_CLK_DISABLE() ;
      // Delete event
      OS_EventDelete(QSPI_EventReady) ;
      QSPI_EventReady = NULL ;
      } ;
   }



// One-time MDMA initialization
void MDMA_Enable(void)
   {
   register MDMA_Channel_TypeDef *MDMA_ChannelX ;
   register uint32_t  i ;

   if (MDMA_Init) return ;
   // Enable MDMA clock
   __HAL_RCC_MDMA_CLK_ENABLE() ;
   // Reset all channels
   for (i = 0; i < 16; i++)
      {
      MDMA_ChannelX = (MDMA_Channel_TypeDef*)(MDMA_BASE + 0x40 + 0x40*i) ;
      MDMA_ChannelX->CCR = 0 ;
      while (MDMA_ChannelX->CCR & MDMA_CCR_EN) __DSB() ;
      MDMA_ChannelX->CIFCR = 0x1F ;
      MDMA_ChannelX->CBRUR = 0 ;
      MDMA_ChannelX->CLAR =  0 ;
      } ;
   // Enable MDMA interrupts
   HAL_NVIC_SetPriority(MDMA_IRQn, MDMA_IRQ_PRIO, 0) ;
   NVIC_EnableIRQ(MDMA_IRQn) ;
   // Mark initialized
   MDMA_Init = 1 ;
   }


int QSPI_Status(void)
   {
   if (QSPI_EventReady != NULL) return 0 ;
   return 1 ;
   }


// Returns total number of available logical 4096-byte sectors
uint32_t QSPI_NumberOfSectors(void)
   {
   register uint32_t  n = (1 << ((QUADSPI->DCR >> 16) + 1)) >> 12 ;
   return n ;
   }



// MDMA interrupt
// For QSPI this will be called on error for both TX and RX, or for completion of RX
void RAM_FNC MDMA_IRQHandler(void)
   {
   register uint32_t  source ;
   register uint32_t  status ;

   // Get interrupt source mask
   source = MDMA->GISR0 ;
   // Process all sources
   while (source)
      {
      // Calculate channel number
      status = 31 - __CLZ(source) ;
      // Clear processed channel in interrupt mask
      source ^= 1 << status ;
      // Process by channel
      switch (status)
         {
         // Channel 0 - QSPI
         case 0:
            // Get channel status
            status = MDMA_CHAN->CISR ;
            // Reset MDMA flags
            MDMA_CHAN->CIFCR = 0x1F ;
            // Disable MDMA channel
            MDMA_CHAN->CCR = 0 ;
            // Set status, if error
            if (status & MDMA_CISR_TEIF)
               {
               QUADSPI->CR = QUADSPI_CR_ABORT ;
               TransferStatus = 1 ;
               } ;
            // Release waiting thread
            OS_EventSet(QSPI_EventReady) ;
            break ;

         // Channel 1 - CRC
         case 1:
            // Get channel status
            status = MDMA_CRC->CISR ;
            // Reset MDMA flags
            MDMA_CRC->CIFCR = 0x1F ;
            // Disable MDMA channel
            MDMA_CRC->CCR = 0 ;
            // Set status, if error
            if (status & MDMA_CISR_TEIF) CRCStatus = 1 ;
            // Release waiting thread
            OS_EventSet(CRC_EventReady) ;
         } ;
      } ;
   }



// QUADSPI interrupt
// This will be called on error for QAUDSPI for both RX and TX, or for completion of TX
void RAM_FNC QUADSPI_IRQHandler(void)
   {
   register uint32_t  status = QUADSPI->SR ;

   if (status & QUADSPI_SR_TEF)
      {
      // Error
      TransferStatus = 2 ;
      MDMA_CHAN->CIFCR = 0x1F ;
      MDMA_CHAN->CCR = 0 ;
      QUADSPI->CR = 0 ;
      } ;
   QUADSPI_ResetFlags ;
   // Release waiting thread
   OS_EventSet(QSPI_EventReady) ;
   }



static INLINE uint8_t QSPI_ReadStatus1(void)
   {
   QUADSPI_ResetFlags ;
   QUADSPI->CR = QUADSPI_CR ;
   QUADSPI->DLR = 0 ;
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_READ | QSPI_DATA_SINGLE | QSPI_ADDR_NONE | QSPI_INSTR_SINGLE | 0x05 ;
   while (0 == (QUADSPI->SR & QSPI_FLAG_TC)) ;
   return *(volatile uint8_t*)&QUADSPI->DR ;
   }



// Get chip ID
uint32_t QSPI_GetID(void)
   {
   register uint32_t  id ;

   QUADSPI_ResetFlags ;
   QUADSPI->CR = QUADSPI_CR ;
   // Data length
   QUADSPI->DLR = 1 ;
   // Set instruction
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_READ | QSPI_DATA_SINGLE | QSPI_ADDR_SINGLE| QSPI_ADR_SIZE_24 | QSPI_INSTR_SINGLE | 0x90 ;
   QUADSPI->AR = 0 ;
   while (0 == (QUADSPI->SR & QSPI_FLAG_FT)) ;
   id = *(volatile uint8_t*)&QUADSPI->DR << 8 ;
   while (0 == (QUADSPI->SR & QSPI_FLAG_TC)) ;
   id |= *(volatile uint8_t*)&QUADSPI->DR ;
   return id ;
   }


// Write enable
static uint8_t QSPI_WEN(void)
   {
   QUADSPI_ResetFlags ;
   QUADSPI->CR = QUADSPI_CR ;
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_WRITE | QSPI_DATA_NONE | QSPI_ADDR_NONE| QSPI_INSTR_SINGLE | 0x06 ;
   while (0 == (QUADSPI->SR & QSPI_FLAG_TC)) ;
   return (QSPI_ReadStatus1() & FLASH_WEL) ;
   }


static int QSPI_Wait(void)
   {
   register int  err ;

   TransferStatus = 0 ;
   QUADSPI_ResetFlags ;
   QUADSPI->CR = QUADSPI_CR | QUADSPI_CR_APMS | QUADSPI_CR_SMIE  ;
   QUADSPI->DLR = 0 ;
   QUADSPI->PSMKR = FLASH_BUSY ;
   QUADSPI->PSMAR = 0 ;
   QUADSPI->PIR = 0 ;
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_POLL | QSPI_DATA_SINGLE | QSPI_ADDR_NONE| QSPI_INSTR_SINGLE | 0x05 ;
   err = OS_EventWait(QSPI_EventReady, QUADSPI_TIMEOUT) ;
   while (QUADSPI->SR & QUADSPI_SR_BUSY) ;
   if (err || TransferStatus) return 1 ;
   return 0 ;
   }



// Erase sector of 4K
uint32_t QSPI_Erase(uint32_t addr)
   {
   // Enable write
   if (!QSPI_WEN()) return 1 ;
   // Set erase command
   QUADSPI_ResetFlags ;
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_WRITE | QSPI_DATA_NONE | QSPI_ADDR_SINGLE| QSPI_ADR_SIZE_24 | QSPI_INSTR_SINGLE | 0x20 ;
   QUADSPI->CR = QUADSPI_CR ;
   QUADSPI->AR = addr ;
   while (0 == (QUADSPI->SR & QSPI_FLAG_TC)) ;
   // Poll while erase in progress
   return QSPI_Wait() ;
   }


// Set MDMA CTBR register
static INLINE uint32_t MDMA_SetCTBR(uint32_t request, uint32_t source, uint32_t destination)
   {
   // Source bus type
   source = source >> 24 ;
   if (source == 0x00 || source == 0x20) request |= MDMA_CTBR_SBUS ;
   // Destination bus type
   destination = destination >> 24 ;
   if (destination == 0x00 || destination == 0x20) request |= MDMA_CTBR_DBUS ;
   return request ;
   }


// Transfers nSectors bytes (QSPI_FLASH_SECTOR_SIZE bytes per sector) from QSPI to RAM
// Maximum 4096 sectors can be transfered
static INLINE void MDMA_SetChannel_RX(void *pData, uint32_t nSectors)
   {
   // Clear status flags
   MDMA_CHAN->CIFCR = 0x1F ;
   // Source is QUADSPI
   MDMA_CHAN->CSAR = (uint32_t)&QUADSPI->DR ;
   // Destination is memory
   MDMA_CHAN->CDAR = (uint32_t)pData ;
   // Destination increment, repeated block transfer
   MDMA_CHAN->CTCR = MDMA_DEST_INC_BYTE | MDMA_REPEAT_BLOCK_TRANSFER ;
   // Request signal and address types
   MDMA_CHAN->CTBR = MDMA_SetCTBR(MDMA_REQUEST_QUADSPI_FIFO_TH, (uint32_t)&QUADSPI->DR, (uint32_t)pData) ;
   // Size of single block is QSPI_FLASH_SECTOR_SIZE bytes.  Number of blocks is nSectors
   MDMA_CHAN->CBNDTR = ((nSectors - 1) << 20) | QSPI_FLASH_SECTOR_SIZE ;
   // Enable MDMA channel with error interrupt and complete interrupt enabled.  This will start transfer with interrupt at the end
   MDMA_CHAN->CCR = MDMA_PRIORITY_HIGH | MDMA_CCR_TEIE | MDMA_CCR_CTCIE | MDMA_CCR_EN ;
   }


// Transfers QSPI_FLASH_PAGE_SIZE bytes from RAM to QSPI
static INLINE void MDMA_SetChannel_TX(const void *pData)
   {
   // Clear status flags
   MDMA_CHAN->CIFCR = 0x1F ;
   // Source is memory
   MDMA_CHAN->CSAR = (uint32_t)pData ;
   // Destination is QUADSPI
   MDMA_CHAN->CDAR = (uint32_t)&QUADSPI->DR ;
   // Source increment, repeated block transfer
   MDMA_CHAN->CTCR = MDMA_SRC_INC_BYTE | MDMA_REPEAT_BLOCK_TRANSFER ;
   // Request signal and address types
   MDMA_CHAN->CTBR = MDMA_SetCTBR(MDMA_REQUEST_QUADSPI_FIFO_TH, (uint32_t)pData, (uint32_t)&QUADSPI->DR) ;
   // Number of bytes
   MDMA_CHAN->CBNDTR = QSPI_FLASH_PAGE_SIZE ;
   // Enable MDMA channel with error interrupt enabled.  Termination interrupt is given by QSPI
   MDMA_CHAN->CCR = MDMA_PRIORITY_HIGH | MDMA_CCR_TEIE | MDMA_CCR_EN ;
   }


// Write single page of QSPI_FLASH_PAGE_SIZE bytes
// End of operation detected in QUADSPI TC flag
static INLINE int QSPI_WritePage(uint32_t addr, const uint8_t *pData)
   {
   register int  err ;

   // Enable write
   if (!QSPI_WEN()) return 1 ;
   // Setup QSPI write data command and start it with transfer complete interrupt enabled
   TransferStatus = 0 ;
   QUADSPI_ResetFlags ;
   QUADSPI->DLR = QSPI_FLASH_PAGE_SIZE - 1 ;
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_WRITE | QSPI_DATA_QUAD | QSPI_ADDR_SINGLE| QSPI_ADR_SIZE_24 | QSPI_INSTR_SINGLE | 0x32 ;
   QUADSPI->CR = QUADSPI_CR | QUADSPI_CR_TCIE ;
   QUADSPI->AR = addr ;
   // Start DMA
   MDMA_SetChannel_TX(pData) ;
   // Wait transfer to complete
   err = OS_EventWait(QSPI_EventReady, QUADSPI_TIMEOUT) ;
   if (err || TransferStatus)
      {
      QUADSPI->CR = QUADSPI_CR_ABORT ;
      return 1 ;
      } ;
   // Wait for programming complete
   return QSPI_Wait() ;
   }


/*******************************************************************************
 *
 *                     Reads specified number of sectors
 *
 *  Start    - start sector number
 *  pData    - pointer to data buffer to receive read data
 *  nSectors - number of sectors to read
 *
 *  Sector size is QSPI_FLASH_SECTOR_SIZE (4096 bytes)
 *
 *******************************************************************************/
int QSPI_Read(uint32_t Start, uint8_t *pData, uint32_t nSectors)
   {
   register int   err ;
   register uint8_t   bCache = !IsRegionD2(pData) ;
   register uint32_t  size = QSPI_SECT_TO_ADDR(nSectors) ;

   // Make sure that event is reset
   OS_EventReset(QSPI_EventReady) ;

   // Check, if region is cacheable and synchronize RAM and cache, if so
   if (bCache)
      {
      register uint32_t  aligned = (uint32_t)pData & ~0x1F ;
      register uint32_t  pEnd = (uint32_t)(pData + size) ;
      register uint32_t  aligned2 = pEnd & ~0x1F ; ;
      // If buffer is not 32-byte aligned, sync cache and memory (write to memory
      if (aligned != (uint32_t)pData)
         {
         __DSB() ;
         SCB->DCCMVAC = aligned ;
         err = 1 ;
         }
      else
         err = 0 ;
      if (aligned2 != pEnd && aligned2 != aligned)
         {
         if (err == 0) __DSB() ;
         SCB->DCCMVAC = aligned2 ;
         err = 1 ;
         } ;
      if (err)
         {
         __ISB() ;
         __DSB() ;
         } ;
      } ;

   // Reset status flags
   TransferStatus = 0 ;
   QUADSPI_ResetFlags ;
   // Data length
   QUADSPI->DLR = size - 1 ;
   // Alternate byte value
   QUADSPI->ABR = 0xFF ;
   // Modes (4 dummy clocks before data out)
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_READ | QSPI_INSTR_SINGLE | QSPI_ADR_SIZE_24 | QSPI_ADDR_QUAD| QSPI_ALTERNATE_BYTES_1 | QSPI_ALTERNATE_QUAD | (4 << 18) | QSPI_DATA_QUAD | 0xEB ;
   // Enable without interrupts
   QUADSPI->CR = QUADSPI_CR ;
   // Address (this will start command execution)
   QUADSPI->AR = QSPI_SECT_TO_ADDR(Start) ;
   // Start DMA
   MDMA_SetChannel_RX(pData, nSectors) ;
   // Wait for interrupt or timeout
   err = OS_EventWait(QSPI_EventReady, QUADSPI_TIMEOUT) ;
   if (err || TransferStatus)
      {
      QUADSPI->CR = QUADSPI_CR_ABORT ;
      return 1 ;
      } ;

   // Synchronize RAM and cache, if needed
   if (bCache) SCB_InvalidateDCache_by_Addr((uint32_t)pData, size) ;
   return 0 ;
   }



/*******************************************************************************
 *
 *                     Writes specified number of sectors
 *
 *  Start    - start sector number
 *  pData    - pointer to data to write
 *  nSectors - number of sectors to write
 *
 *  Sector size is QSPI_FLASH_SECTOR_SIZE (4096 bytes) - this is minimal erase
 *  unit. Write should be done in 256-byte pages.
 *
 *******************************************************************************/
int QSPI_Write(uint32_t Start, const uint8_t *pData, uint32_t nSectors)
   {
   register int      err ;
   register uint32_t addr = QSPI_SECT_TO_ADDR(Start) ;

   // Make sure that event is reset
   OS_EventReset(QSPI_EventReady) ;
   // Synchronize RAM and cache, if memory is cacheable
   if (!IsRegionD2(pData)) SCB_CleanDCache_by_Addr((uint32_t)pData, QSPI_SECT_TO_ADDR(nSectors)) ;
   // Loop by sectors
   while (nSectors--)
      {
      register uint32_t nPages ;
      // Erase sector before writing it
      if ((err = QSPI_Erase(addr))) return err ;
      nPages = QSPI_PAGES_PER_SECTOR ;
      // Write pages in sector
      do
         {
         if ((QSPI_WritePage(addr, pData))) return err ;
         addr += QSPI_FLASH_PAGE_SIZE ;
         pData += QSPI_FLASH_PAGE_SIZE ;
         }
      while (--nPages) ;
      } ;
   return 0 ;
   }


// CPU-only in RAM flash read for firmware upgrade
/*
__attribute__((section(".fncram"))) void QSPI_ReadRAM(uint32_t addr, uint8_t *pData, uint32_t len)
   {
   QUADSPI->CR = QUADSPI_CR ;
   QUADSPI_ResetFlags ;
   QUADSPI->DLR = len - 1 ;
   // Alternate byte value
   QUADSPI->ABR = 0xFF ;
   //QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_READ | QSPI_DATA_SINGLE | QSPI_ADDR_SINGLE| QSPI_ADR_SIZE_24 | QSPI_INSTR_SINGLE | 0x03 ;
   QUADSPI->CCR = QSPI_FUNCTIONAL_MODE_INDIRECT_READ | QSPI_INSTR_SINGLE | QSPI_ADR_SIZE_24 | QSPI_ADDR_QUAD| QSPI_ALTERNATE_BYTES_1 | QSPI_ALTERNATE_QUAD | (4 << 18) | QSPI_DATA_QUAD | 0xEB ;
   QUADSPI->AR = addr ;
   while (len--)
      {
      while (0 == (QUADSPI->SR & QSPI_FLAG_FT)) ;
      *pData++ = *(volatile uint8_t*)&QUADSPI->DR ;
      } ;
   }
*/


#ifdef _TEST_
#define S_START         171
#define S_END           (S_START+50)

void TestQSPI(void)
   {
   uint32_t  start, *p, i ;
   int  err ;
   uint8_t  *buf ;
   static uint32_t cnt = 0 ;

   QSPI_Init() ;
   buf = malloc(QSPI_FLASH_SECTOR_SIZE) ;

   start = S_START ;
   cnt++ ;

   while (start <= S_END)
      {
      p = (uint32_t*)buf ;
      for (i = 0; i < QSPI_FLASH_SECTOR_SIZE >> 2; i++) *p++ = start + i + cnt ;
      err = QSPI_Write(start, buf, 1) ;
      if (err)
         {
         DbgPrintf("Write error %u\r\n", err) ;
         free(buf) ;
         return ;
         } ;
      start++ ;
      } ;

   start = S_START ;
   while (start <= S_END)
        {
        //DbgPrintf("Sector %u\r\n", start) ;
        p = (uint32_t*)buf ;
        err = QSPI_Read(start, buf, 1) ;
        if (err)
           {
           DbgPrintf("Read error %u\r\n", err) ;
           free(buf) ;
           return ;
           } ;
        p = (uint32_t*)buf ;
        for (i = 0; i < QSPI_FLASH_SECTOR_SIZE >> 2; i++)
           if (p[i] != start + i + cnt)
              {
              DbgPrintf("sector=%u i=%u v=%08X e=%08X\r\n", start, i, p[i], start+i+cnt) ;
              } ;
        start++ ;
        } ;

   free(buf) ;
   DbgPrintf("Done. cnt=%u\r\n", cnt) ;
   }
#endif




static NO_CACHE MDMA_LinkNodeTypeDef List ;

// Global one-time initialization of CRC controller
int crc_table32(void)
   {
   int  err ;

   if (CRC_Mutex) return 0 ;
   // Create access mutex
   CRC_Mutex = OS_MutexCreate(FALSE, &err) ;
   if (err) return err ;
   // Create automatic ready event
   CRC_EventReady = OS_EventCreate(0, 0, &err) ;
   if (err)
      {
      OS_MutexDelete(CRC_Mutex) ;
      CRC_Mutex = NULL ;
      } ;
   // Linked list element setup
   memset(&List, 0, sizeof(List)) ;
   List.CTCR = (3<<18) | MDMA_REQUEST_SW | MDMA_SRC_DATASIZE_BYTE| MDMA_SRC_INC_BYTE | MDMA_DATAALIGN_PACKENABLE| MDMA_DEST_DATASIZE_WORD | MDMA_DEST_INC_DISABLE| MDMA_FULL_TRANSFER ;
   List.CDAR = (uint32_t)&CRC->DR ;

//   crc_table16() ;
   return err ;
   }



/********************************************************************************
 *
 *                     Partial CRC-32 (Ethernet standard)
 *
 * src    - pointer to the source data; may not be WORD-aligned
 *          Data must be committed to memory, or no cache memory region is used
 * cnt    - length of data pointed by "src" in BYTEs; size limit is 256MB + 64KB
 * crc    - initial CRC to start
 * bFinal - if this is not 0, this is the last portion of data
 *
 * Also, see macro crc32() for single block of data.
 *
 ********************************************************************************/
uint32_t crc32ex(const uint8_t *src, uint32_t cnt, uint32_t crc, uint32_t bFinal)
   {
   register uint32_t  nBlocks ;

   // Take control over CRC controller
   if (cnt == 0 || OS_MutexClaim(CRC_Mutex, INFINITY)) return crc ;
   // Power it on
   __HAL_RCC_CRC_CLK_ENABLE() ;
   // CRC controller initialization
   CRC->INIT = crc ;
   CRC->POL = ulPolynomial ;
   // If this is final call, reverse the result
   CRC->CR = CRC_CR_REV_IN | CRC_CR_RESET | bFinal ;

   // Enable MDMA, if necessary.  Will use channel 1
   MDMA_Enable() ;
   // Clear status flags
   MDMA_CRC->CIFCR = 0x1F ;
   // Source is memory
   MDMA_CRC->CSAR = (uint32_t)src ;
   // Destination is CRC controller
   MDMA_CRC->CDAR = (uint32_t)&CRC->DR ;
   // Request signal and address types
   MDMA_CRC->CTBR = MDMA_SetCTBR(0, (uint32_t)src, (uint32_t)&CRC->DR) ;
   MDMA_CRC->CBRUR = 0 ;

   // Number of full 64K blocks
   nBlocks = cnt >> 16 ;
   // Set transfer size for full 64K blocks
   if (nBlocks)
      {
      // Software triggered, source increment, pack bytes to word, linked list transfer
      MDMA_CRC->CTCR = (3<<18) | MDMA_REQUEST_SW | MDMA_SRC_DATASIZE_BYTE| MDMA_SRC_INC_BYTE | MDMA_DATAALIGN_PACKENABLE| MDMA_DEST_DATASIZE_WORD | MDMA_DEST_INC_DISABLE| MDMA_FULL_TRANSFER ;
      // Transfer size in 64K blocks
      MDMA_CRC->CBNDTR = ((nBlocks - 1) << 20) | 0x10000 ;
      // Source address after transfer
      src = src + (cnt & ~0xFFFF) ;
      // Byte count after transfer
      cnt = cnt & 0xFFFF ;
      // Size of last partial block (whole WORDs)
      nBlocks = cnt & ~0x3 ;
      // Create list entry for the last block
      if (nBlocks)
         {
         // Fill list entry
         List.CSAR = (uint32_t)src ;               // Source address
         List.CBNDTR = nBlocks ;                   // Transfer size
         List.CTBR = MDMA_CRC->CTBR ;              // Addresses types
         // Link it to the current transfer
         MDMA_CRC->CLAR = (uint32_t)&List ;
         // Address after transfer
         src = src + nBlocks ;
         // Byte count after transfer
         cnt = cnt & 0x3 ;
         }
      else
         // No partial block
         MDMA_CRC->CLAR = 0 ;
      // Start MDMA channel with error and complete interrupts enabled
      CRCStatus = 0 ;
      MDMA_CRC->CCR = MDMA_CCR_SWRQ | MDMA_PRIORITY_VERY_HIGH | MDMA_CCR_TEIE | MDMA_CCR_CTCIE | MDMA_CCR_EN ;
      // Wait to complete. Exit on error
      if (OS_EventWait(CRC_EventReady, INFINITY) || CRCStatus) return crc ;
      }
   else
      {
      // Single transfer of block less than 64K
      nBlocks = cnt & ~0x3 ;
      if (nBlocks)
         {
         // Software triggered, source increment, pack bytes to word, single block transfer
         MDMA_CRC->CTCR = (3<<18) | MDMA_REQUEST_SW | MDMA_SRC_DATASIZE_BYTE| MDMA_SRC_INC_BYTE | MDMA_DATAALIGN_PACKENABLE| MDMA_DEST_DATASIZE_WORD | MDMA_DEST_INC_DISABLE| MDMA_BLOCK_TRANSFER ;
         // Block size
         MDMA_CRC->CBNDTR = nBlocks ;
         // Address after transfer
         src = src + nBlocks ;
         // Byte count after transfer
         cnt = cnt & 0x3 ;
         // Start MDMA channel with error and complete interrupts enabled
         CRCStatus = 0 ;
         MDMA_CRC->CCR = MDMA_CCR_SWRQ | MDMA_PRIORITY_VERY_HIGH | MDMA_CCR_TEIE | MDMA_CCR_CTCIE | MDMA_CCR_EN ;
         // Wait to complete. Exit on error
         if (OS_EventWait(CRC_EventReady, INFINITY) || CRCStatus) return crc ;
         } ;
      } ;

   // Remaining bytes
   if (cnt)
      {
      CRC->CR = CRC_CR_REV_IN_BYTES | CRC_CR_REV_OUT | bFinal ;
      do
         {
         *(uint8_t*)&CRC->DR = *src++ ;
         }
      while (--cnt) ;
      } ;

   // Return result
   crc = CRC->DR ;
   if (bFinal) crc = 0xFFFFFFFF ^ crc ;
   // Power down CRC controller
   __HAL_RCC_CRC_CLK_DISABLE() ;
   // Release CRC controller
   OS_MutexRelease(CRC_Mutex) ;
   return crc ;
   }




// CRC-16-ANSI
// bFinal should be 0 for partial CRC, or CRC_FINAL for the final CRC
uint16_t crc16ex(const uint8_t *p, uint32_t len, uint16_t crc, uint32_t bFinal)
   {
   if (len == 0 || OS_MutexClaim(CRC_Mutex, INFINITY)) return crc ;
   __HAL_RCC_CRC_CLK_ENABLE() ;
   CRC->INIT = crc ;
   CRC->POL = 0x8005 ;
   CRC->CR = CRC_CR_POLYSIZE_16 | CRC_CR_REV_IN_WORDS | CRC_CR_RESET | bFinal ;
   crc = MoveCRC(p, len, (void*)&CRC->DR) ;
   __HAL_RCC_CRC_CLK_DISABLE() ;
   OS_MutexRelease(CRC_Mutex) ;
   return crc ;
   }
