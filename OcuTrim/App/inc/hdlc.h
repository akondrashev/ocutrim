#ifndef __HDLC_H__
#define __HDLC_H__

#include "stm32h7xx_hal.h"
#include "fonts.h"
#include "qspi.h"

extern const uint16_t  Version ;               /* farebox software version */
extern const uint32_t  Part_No ;
extern const char *const Copyright ;

extern volatile uint32_t Sflags ;

extern uint8_t OCU_Debug ;
extern uint8_t TX_Debug ;

// Message types
#define  MT_KEY            0xa0                    /* keyboard message */
#define  MT_DISP1          0xa1                    /* display line 1 */
#define  MT_DISP2          0xa2                    /* display line 2 */
#define  MT_ONLINE         0xa3                    /* unit online */
#define  MT_NUMDISP        0xa4                    /* numeric display */
#define  MT_STATUS         0xa5                    /* status */
#define  MT_SCREENCTRL     0xa6                    /* screen control */
#define  MT_KEYABORT       0xa7                    /* abort key sequence */
#define  MT_MENU           0xa8                    /* built in screen control LA */
#define  MT_DISPLAY        0xa9                    /* display a line */
#define  MT_LAMPON         0xaa                    /* turn backlite on */
#define  MT_LAMPOFF        0xab                    /* turn backlite on */
#define  MT_REQSTATUS      0xac                    /* UNUSED */
#define  MT_CLEARSCREEN    0xad                    /* clear screen      */
#define  MT_BOX2           0xae                    /* make box based on x and y */
#define  MT_OCU_MSG        0xaf                    /* message from OCU */
#define  BLK_TR_CNTL       0xb0                    /* block transfer control    */
#define  BLK_TR_DATA       0xb1                    /* block transfer data       */
#define  BLK_TR_LAST       0xb2                    /* block transfer last       */
#define  PRB_REQ_PGM       0xb3                    /* program data control block (first) */
#define  PRB_DATA_CNTL     0xb4                    /* program data control block (first) */
#define  PRB_DATA_BLK      0xb5                    /* program data block        */
#define  PRB_LAST_BLK      0xb6                    /* program last block (complete) */
#define  MT_KEYOPTION      0xb7                    /* key options               */
#define  MT_ONOFF_LED      0xb9                    /* turn all leds on/off      */
#define  MT_BACK_STAT      0xbc                    /* back program status       */
#define  MT_SOUND          0xbd
#define  MT_DIM_BACKLITE   0xbe                    /* backlight adjustment */
#define  MT_VOLUME         0xbf                    /* adjust volume (Winnipeg) */

#define  TRIMCLRJAM            0x05  /* clear jam                  OCU->TRIM */

#define  TRIM_DIAGNOSTIC_MODE  0x37  /* send TRiM diagnostics cmd  OCU->TRIM */
#define     STOP_DIAGNOSTICS   0x00  /* take TRiM out of diag mode OCU->TRIM */
#define     DIAGNOSTICS        0x01  /* place TRiM in diag. mode   OCU->TRIM */
#define     OCU_KEY            0x14  /* diagnostic key pressed for OCU->TRiM */
#define  TRIM_OCU_TEXT         0x38  /* LCD message                TRIM->OCU */
#define  TRIM_OCU_BOX          0x3d  /* draw a "box" on the screen TRIM->OCU */
#define  TRIM_OCU_CLR          0x3e  /* clear display              TRIM->OCU */

// ID types
#define  ID_ALL         (uint8_t)'0'               /* broadcast */
#define  ID_DCU         (uint8_t)'1'               /* driver control unit */
#define  ID_PIU         (uint8_t)'2'               /* passenger info. unit */
#define  ID_FBX         (uint8_t)'3'               /* farebox */
#define  ID_EXTSIGN     (uint8_t)'4'               /* external sign */
#define  ID_INTSIGN     (uint8_t)'5'               /* internal sign */

#define  OCU_ID                1
#define  TRIM_ID               2     /* TRiM's "device" identifer            */
#define  GEN_ID                3     /* to be compatable with older firmware */
#define  FBX_ID                4     /* Farebox's NEW "device" identifer     */
#define  TVM_ID                5     /* TVM's "device" identifer             */
#define  PEM_ID                6     /* PEM's "device" identifer             */

#define  OCU_KEYREPEAT_DIS     1
#define  OCU_BLUE_OPTION       2

// Virtual LEDs settings via command
#define    NO_LED         0x00
#define    R_RED          0x01
#define    R_YELLOW       0x02
#define    R_GREEN        0x04
#define    L_RED          0x08
#define    L_YELLOW       0x10
#define    L_GREEN        0x20

#define OCU_C         -1 /* OCU screen "auto" center text    */
#define OCU_T          0 /* OCU screen top reference         */
#define OCU_L          0 /* OCU screen left reference        */
#define OCU_R         30 /* OCU screen right reference       */
#define OCU_B         19 /* OCU screen bottom line reference */

// Screen types based on document from Nextek dated
// 06/25/99
#define  SCREEN_0      0
#define  SCREEN_1      1
#define  SCREEN_2      2
#define  SCREEN_2A     3
#define  SCREEN_3      4
#define  SCREEN_4      5
#define  SCREEN_5      6
#define  SCREEN_6      7
#define  SCREEN_7      8
#define  SCREEN_8      9
#define  SCREEN_9      10
#define  SCREEN_10     11
#define  SCREEN_11     12
#define  SCREEN_12     13
#define  SCREEN_13     14
#define  SCREEN_14     15
#define  SCREEN_15     16

// OCU status
typedef struct
   {
   char  bus[6];                        /* bus no                        */
   char  ver[6];                        /* fb version                    */
   char  driver[6];                     /* driver no.                    */
   char  route[6];                      /* route no.                     */
   char  run[6];                        /* run                           */
   char  trip[6];                       /* trip no.                      */
   char  stop[6];                       /* stop no.                      */
   char  fset;                          /* full fare set                 */
   char  dir;                           /* direction                     */
   } OCU_STATUS ;


typedef struct
   {
   uint8_t  id ;
   uint8_t  cmd ;
   uint8_t  x ;
   uint8_t  y ;
   char     text[40] ;
   } TRIM_TEXT ;


typedef struct
   {
   uint8_t  id ;
   uint8_t  cmd ;
   uint8_t  x ;
   uint8_t  y ;
   uint8_t  x2 ;
   uint8_t  y2 ;
   } TRIM_BOX ;


typedef struct
   {
   uint8_t  dest ;
   uint8_t  cmd ;
   uint8_t  src ;
   uint8_t  fnct ;
   uint8_t  key ;
   uint8_t  spar ;
   } TRIM_MSG ;

extern volatile uint32_t Status ;

/* Probing Status and macros to maintain. */
#define  OFFLINE           0x00000001               /* no GMS present */
#define  OCU_PROGRAM       0x00000002
#define  OCU_ERR_MSG       0x00000004
#define  OCU_PROG_READY    0x00000008
#define  OCU_STATUS        0x00000010
#define  OCU_DISPLAY_BAUD  0x00000020
#define  OCU_BAUD_SELECTED 0x00000040
#define  ALLOW_REVERSE     0x00010000
#define  REVERSE_ON        0x00020000
#define  LAMP_OFF          0x00040000


#define  set_status( s )   (Status |= (s))
#define  clr_status( s )   (Status &=~(s))
#define  tst_status( s )   (Status &  (s))


#define MIN_PROGRAM_SIZE         100000
#define MAX_PROGRAM_SIZE         200000

#define START_PROGRAM_ADDRESS    0x8000000

typedef struct
   {
   uint32_t  ImageSize ;
   uint32_t  crc ;
   } FW_IMAGE ;

#define MAGIC_HEADER    0xCACACACA


// Rendering messages
typedef enum
   {
   RND_MSG_CLEAR = 0,
   RND_MSG_TXT,
   RND_MSG_BOX,
   RND_MSG_LOGO,
   RND_MSG_LED
   } RND_MSG_TYPE ;

#define  RX_BUF_SZ      256

// Rendering queue element
typedef struct
   {
   GUI_FONT      *pFont ;
   uint32_t       sz ;
   uint16_t       x ;
   uint16_t       y ;
   uint16_t       x2 ;
   uint16_t       y2 ;
   uint8_t        msgType ;
   uint8_t        FontColor ;
   uint8_t        BackColor ;
   uint8_t        Reverse ;
   uint8_t        buf[RX_BUF_SZ-4] ;
   } RENDER ;


#define RND_QUEUE_SZ    16

// Rendering queue
typedef struct
   {
   volatile uint32_t  cnt ;
   uint16_t  first ;
   uint16_t  last ;
   RENDER    q[RND_QUEUE_SZ] ;
   } RND_QUEUE ;

extern RND_QUEUE  RenderQueue ;

extern const char CONFIG_FILE_NAME[] ;

// OCU static parameters
typedef struct
   {
   uint32_t  BaudRate ;
   uint8_t   ColorSheme ;
   uint8_t   Brightness ;
   uint8_t   SoundLevel ;
   uint8_t   UsingLED ;
   uint8_t   spare[10] ;
   uint16_t  crc ;
   } OCU_PARAMS ;

extern OCU_PARAMS OCUParameters ;

#define BackLiteLevel      OCUParameters.Brightness


//int   PPSMsg(uint8_t type, const uint8_t *data, uint32_t size) ;
void  OCUStatus(void) ;
int   InitializePPSProtocol(void) ;
void  HDLC(void*) ;
int   FlashVerify(void) ;
void  ProcessBaudRateChange(void) ;
void  ExitBaudRateChange(void) ;
void  SelectNewBaudRate(void) ;

int   TrimDiagnostic(uint8_t cmd, uint8_t key, uint8_t spare) ;
void  LCDClear(void) ;

#endif
