#ifndef __SYSDEF_H__
#define __SYSDEF_H__

#include "stm32h7xx_hal.h"


#define  PIN2_CLEAR_0      0xFFFFFFFC
#define  PIN2_CLEAR_1      0xFFFFFFF3
#define  PIN2_CLEAR_2      0xFFFFFFCF
#define  PIN2_CLEAR_3      0xFFFFFF3F
#define  PIN2_CLEAR_4      0xFFFFFCFF
#define  PIN2_CLEAR_5      0xFFFFF3FF
#define  PIN2_CLEAR_6      0xFFFFCFFF
#define  PIN2_CLEAR_7      0xFFFF3FFF
#define  PIN2_CLEAR_8      0xFFFCFFFF
#define  PIN2_CLEAR_9      0xFFF3FFFF
#define  PIN2_CLEAR_10     0xFFCFFFFF
#define  PIN2_CLEAR_11     0xFF3FFFFF
#define  PIN2_CLEAR_12     0xFCFFFFFF
#define  PIN2_CLEAR_13     0xF3FFFFFF
#define  PIN2_CLEAR_14     0xCFFFFFFF
#define  PIN2_CLEAR_15     0x3FFFFFFF

#define  PIN_0             ((uint32_t)1)
#define  PIN_1             ((uint32_t)1 << 1)
#define  PIN_2             ((uint32_t)1 << 2)
#define  PIN_3             ((uint32_t)1 << 3)
#define  PIN_4             ((uint32_t)1 << 4)
#define  PIN_5             ((uint32_t)1 << 5)
#define  PIN_6             ((uint32_t)1 << 6)
#define  PIN_7             ((uint32_t)1 << 7)
#define  PIN_8             ((uint32_t)1 << 8)
#define  PIN_9             ((uint32_t)1 << 9)
#define  PIN_10            ((uint32_t)1 << 10)
#define  PIN_11            ((uint32_t)1 << 11)
#define  PIN_12            ((uint32_t)1 << 12)
#define  PIN_13            ((uint32_t)1 << 13)
#define  PIN_14            ((uint32_t)1 << 14)
#define  PIN_15            ((uint32_t)1 << 15)


#define  PIN_CLEAR_0       (~PIN_0)
#define  PIN_CLEAR_1       (~PIN_1)
#define  PIN_CLEAR_2       (~PIN_2)
#define  PIN_CLEAR_3       (~PIN_3)
#define  PIN_CLEAR_4       (~PIN_4)
#define  PIN_CLEAR_5       (~PIN_5)
#define  PIN_CLEAR_6       (~PIN_6)
#define  PIN_CLEAR_7       (~PIN_7)
#define  PIN_CLEAR_8       (~PIN_8)
#define  PIN_CLEAR_9       (~PIN_9)
#define  PIN_CLEAR_10      (~PIN_10)
#define  PIN_CLEAR_11      (~PIN_11)
#define  PIN_CLEAR_12      (~PIN_12)
#define  PIN_CLEAR_13      (~PIN_13)
#define  PIN_CLEAR_14      (~PIN_14)
#define  PIN_CLEAR_15      (~PIN_15)


#endif
