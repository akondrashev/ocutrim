#ifndef __KEY_H__
#define __KEY_H__

#include "stm32h7xx_hal.h"


#define PinSet(PORT, Pin)           (PORT)->BSRR = (uint16_t)(Pin)
#define PinClear(PORT, Pin)         (PORT)->BSRR = ((uint32_t)(Pin) << 16)
#define PinRead(PORT, Pin)          ((PORT)->IDR & (Pin))

#define ColPort               GPIOE

#define E_Pin                 GPIO_PIN_1     // PORTE
#define C_Pin                 GPIO_PIN_0     // PORTE

#define Col0                  GPIO_PIN_3     // PORTE
#define Col1                  GPIO_PIN_4     // PORTE
#define Col2                  GPIO_PIN_5     // PORTE
#define Col3                  GPIO_PIN_6     // PORTE
#define Col4                  GPIO_PIN_7     // PORTE
#define Col5                  GPIO_PIN_8     // PORTE

#define RowPort               GPIOD

#define Row0                  GPIO_PIN_0     // PORTD
#define Row1                  GPIO_PIN_1     // PORTD
#define Row2                  GPIO_PIN_2     // PORTD
#define Row3                  GPIO_PIN_4     // PORTD
#define Row4                  GPIO_PIN_5     // PORTD

#define  Set_Sflag( f )          ( Sflags |= (f) )
#define  Clr_Sflag( f )          ( Sflags &=~(f) )
#define  Tst_Sflag( f )          ( Sflags &  (f) )


#define  S_DUMP            (uint32_t)0x00000001    /* dump key active         */
#define  S_BLUE            (uint32_t)0x00000002    /* Blue key active         */
#define  S_DOWNLOADDATA    (uint32_t)0x00000004    /* download data received  */
#define  S_RS232           (uint32_t)0x00000008    /* rs232 comm on line      */
#define  S_NO_REPEAT_KEY   (uint32_t)0x00000010    /* repeat enable/disable   */
#define  S_USING_LED       (uint32_t)0x00000020    /* render LEDs on screen   */
#define  S_BLUE_OPTION     (uint32_t)0x00000040    /* BLUE option flag        */
#define  S_BACKLITE_OFF    (uint32_t)0x00000080    /* back light on           */
#define  S_OVERRUN         (uint32_t)0x00000100
#define  S_FRAMING         (uint32_t)0x00000200
#define  S_LED_CHANGED     (uint32_t)0x00000400    // Set, when LED mode changed
#define  S_PROBING         (uint32_t)0x00001000    /* currently probing       */
#define  S_COMPLETEPENDING (uint32_t)0x00002000    /* probe just completed    */
#define  S_OFFLINE         (uint32_t)0x00040000    /* comm. offline           */
#define  S_CONFIGURE       (uint32_t)0x00700000    /* configure               */
#define  S_HOPPER          (uint32_t)0x00100000    /* Hopper                  */
#define  S_ALARM           (uint32_t)0x00200000    /* alarm                   */
#define  S_COIN            (uint32_t)0x00300000    /* coin                    */
#define  S_BILL            (uint32_t)0x00400000    /* bill                    */
#define  S_TRIM            (uint32_t)0x00500000    /* TRIM                    */
#define  S_PROBED          (uint32_t)0x08000000    /* probed with alarm delay */
#define  S_OCU_OFFLINE     (uint32_t)0x80000000    /* OCU off line            */

#define  OCU_CONF          ((Sflags & S_CONFIGURE ) == 0)
#define  HOPPER_CONF       ((Sflags & S_CONFIGURE ) == S_HOPPER)
#define  ALARM_CONF        ((Sflags & S_CONFIGURE ) == S_ALARM)
#define  COIN_CONF         ((Sflags & S_CONFIGURE ) == S_COIN)
#define  BILL_CONF         ((Sflags & S_CONFIGURE ) == S_BILL)
#define  TRIM_CONF         ((Sflags & S_CONFIGURE ) == S_TRIM)
#define  MAX_DURATION       7

int  KeyBoardInit(void) ;

#endif
