#ifndef __DBG_H__
#define __DBG_H__

int      InitDebug(void) ;
void     DbgPrintf(char *fmt, ...) ;
void     Tlog(char *fmt, ...) ;
uint32_t DbgGet(uint8_t *pBuf, uint32_t size, uint32_t timeout) ;

#endif
