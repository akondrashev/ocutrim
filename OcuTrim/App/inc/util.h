#ifndef __UTIL_H__
#define __UTIL_H__

#include "stm32h7xx_hal.h"

#define  CRC_FINAL      CRC_CR_REV_OUT
#define  CRC_PARTIAL    0


static __attribute__((always_inline)) inline void swab(uint16_t *pSrc, uint16_t *pDst, uint32_t len)
   {
   len >>= 1 ;
   while (len--) *pDst++ = __REV16(*pSrc++) ;
   }

void     crc_table32(void) ;
uint32_t crc32ex(const uint8_t *ptr, uint32_t cnt, uint32_t crc, uint32_t bFinal) ;
uint16_t crc16ex(const uint8_t *ptr, uint32_t len, uint16_t crc, uint32_t bFinal) ;


#define  crc16(ptr, cnt)            (crc16ex(ptr, cnt, 0xFFFF, CRC_FINAL))
#define  crc32(ptr, cnt)            (crc32ex(ptr, cnt, 0xFFFFFFFF, CRC_FINAL))

#endif
