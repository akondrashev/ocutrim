#ifndef __QSPI_H__
#define __QSPI_H__

#include "stm32h7xx_hal.h"

#define QSPI_FUNCTIONAL_MODE_INDIRECT_READ            (((uint32_t)1) << 26)
#define QSPI_FUNCTIONAL_MODE_INDIRECT_WRITE           0
#define QSPI_FUNCTIONAL_MODE_INDIRECT_POLL            (((uint32_t)2) << 26)

#define QSPI_DATA_SINGLE                              (((uint32_t)1) << 24)
#define QSPI_DATA_DOUBLE                              (((uint32_t)2) << 24)
#define QSPI_DATA_QUAD                                (((uint32_t)3) << 24)

#define QSPI_ADR_SIZE_24                              (((uint32_t)2) << 12)

#define QSPI_ADDR_NONE                                0
#define QSPI_ADDR_SINGLE                              (((uint32_t)1) << 10)
#define QSPI_ADDR_DOUBLE                              (((uint32_t)2) << 10)
#define QSPI_ADDR_QUAD                                (((uint32_t)3) << 10)

#define QSPI_INSTR_SINGLE                             (((uint32_t)1) << 8)

#define QSPI_ALTERNATE_BYTES_1                        (((uint32_t)0) << 16)
#define QSPI_ALTERNATE_BYTES_2                        (((uint32_t)1) << 16)
#define QSPI_ALTERNATE_BYTES_3                        (((uint32_t)2) << 16)
#define QSPI_ALTERNATE_BYTES_4                        (((uint32_t)3) << 16)

#define QSPI_ALTERNATE_SINGLE                         (((uint32_t)1) << 14)
#define QSPI_ALTERNATE_DOUBLE                         (((uint32_t)2) << 14)
#define QSPI_ALTERNATE_QUAD                           (((uint32_t)3) << 14)

#define QUADSPI_FTF                                   (0x00000004)
//#define QADSPI_CR_DMA_EN                              0x4


#define QUADSPI_ResetFlags                            QUADSPI->FCR = 0x1B

#define FLASH_BUSY                                    0x1
#define FLASH_WEL                                     0x2

#define QSPI_FLASH_SECTOR_SIZE                        4096           // 0x1000
#define QSPI_FLASH_PAGE_SIZE                          256            // 0x0100
#define QSPI_PAGES_PER_SECTOR                         16             // QSPI_FLASH_SECTOR_SIZE / QSPI_FLASH_PAGE_SIZE
#define QSPI_SECT_TO_ADDR(sect)                       (sect<<12)

#define QUADSPI_TIMEOUT                               2000

int      QSPI_Init(void) ;
void     QSPI_Disable(void) ;
void     MDMA_Enable(void) ;
uint32_t QSPI_GetID(void) ;
uint32_t QSPI_Erase(uint32_t addr) ;
int QSPI_Read(uint32_t Start, uint8_t *pData, uint32_t nSectors) ;
int      QSPI_Write(uint32_t Start, const uint8_t *pData, uint32_t nSectors) ;
int      QSPI_Status(void) ;
uint32_t QSPI_NumberOfSectors(void) ;
void     TestQSPI(void) ;

//void     QSPI_ReadRAM(uint32_t addr, uint8_t *pData, uint32_t len) ;

#endif
