#ifndef __LCD_H__
#define __LCD_H__

#include "stm32h7xx_hal.h"

#define LTDC_LAYER0            ((LTDC_Layer_TypeDef *)((uint32_t)(((uint32_t)(LTDC)) + 0x84 + (0x80*(0)))))
#define LTDC_LAYER1            ((LTDC_Layer_TypeDef *)((uint32_t)(((uint32_t)(LTDC)) + 0x84 + (0x80*(1)))))


#define  DISPLAY_WIDTH              640                           // LCD PIXEL WIDTH
#define  DISPLAY_HEIGHT             480                           // LCD PIXEL HEIGHT
#define  COLOR_DEPTH                8                             // Bits per pixel

#define  KOE640480_HSYNC            ((uint16_t)30)                // Horizontal synchronization
#define  KOE640480_HBP              ((uint16_t)114)               // Horizontal back porch
#define  KOE640480_HFP              ((uint16_t)16)                // Horizontal front porch
#define  KOE640480_VSYNC            ((uint16_t)3)                 // Vertical synchronization
#define  KOE640480_VBP              ((uint16_t)32)                // Vertical back porch
#define  KOE640480_VFP              ((uint16_t)10)                // Vertical front porch

#define  KOE640480_WIDTH            ((uint16_t)DISPLAY_WIDTH)     // LCD PIXEL WIDTH
#define  KOE640480_HEIGHT           ((uint16_t)DISPLAY_HEIGHT)    // LCD PIXEL HEIGHT

#define  X_OFFSET                   4
#define  Y_OFFSET                   0

#define  MAX_DURATION               7

// Virtual LEDs state
#define    LCD_OFF          0x01
#define    LCD_RED          0x02
#define    LCD_YELLOW       0x04
#define    LCD_GREEN        0x08

#define    LED_LINE          136
#define    LED_LEFT_POS      (12 * FONT_X_SMALL)
#define    LED_RIGHT_POS     (17 * FONT_X_SMALL)


#define DMA2D_ALL_INTERRUPTS    (DMA2D_CR_CEIE | DMA2D_CR_CTCIE | DMA2D_CR_CAEIE | DMA2D_CR_TCIE | DMA2D_CR_TEIE)
#define DMA2D_ClearFlags         DMA2D->IFCR = 0x3F

int  LCDInit(void) ;
void SetBackLite(uint8_t value) ;
void LoadPalette(uint8_t PalIndex, uint8_t bPermanent) ;
void LCDcls(uint8_t Color) ;
void LCDTestScreenOff(void) ;
void LCD_ShiftScreen(uint32_t Shift) ;

void SetPaletteColor(uint32_t index, uint32_t rgb) ;
uint32_t GetPaletteColor(uint32_t index) ;

#define BackLiteOn()          SetBackLite(BackLiteLevel)
#define BackLiteOff()         SetBackLite(0)

#define NORM      0
#define SMALL     1
#define BIG       2


#endif
