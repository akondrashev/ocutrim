#ifndef __FLASH_H__
#define __FLASH_H__

#include "stm32h7xx_hal.h"

#define FLASH_KEY1               ((uint32_t)0x45670123U)
#define FLASH_KEY2               ((uint32_t)0xCDEF89ABU)



void  ProgramFlash(FW_IMAGE *pProgram) ;

#endif
