/**************************************************************************************************
 *
 *                                       UART2 Driver
 *                                     FBX serial line
 *
 *  Pin configuration: USART2_TX - PA2; USART2_RX - PD6
 *  DMA operations:    RX - DMA1, Stream 5, Channel 4;  TX - DMA1, Stream 6, Channel 4.
 *  Exported functions:
 *                      UART2_Init()  - initializes and configures UART for a given baud rate;
 *                      Uart2_Get()   - reads data from UART
 *                      Uart2_Put()   - puts data into UART (may block)
 *
 ***************************************************************************************************/
#include "os.h"
#include "Uart.h"

#define  UART2_RX_BUF_SIZE        512
#define  UART2_TX_BUF_SIZE        128

// UART2 control block
UART_typ Uart2 ;
// UART1 RX buffer
static uint8_t __attribute__((section(".ram_d3"))) UART2_RX_BUF[UART2_RX_BUF_SIZE] ;
// UART1 TX buffer
static uint8_t __attribute__((section(".ram_d3"))) UART2_TX_BUF[UART2_TX_BUF_SIZE] ;


// USART2 initialization
int UART2_Init(uint32_t BaudRate)
   {
   GPIO_InitTypeDef  GPIO_InitStruct ;
   //register uint32_t  temp ;

   // Disable interrupts
   NVIC_DisableIRQ(USART2_IRQn) ;
   // Set interrupt priority
   HAL_NVIC_SetPriority(USART2_IRQn, USART2_IRQ_PRIO, 0) ;

   // Setup UART2 control block
   memset(&Uart2, 0, sizeof(Uart2)) ;
   Uart2.hUart = USART2 ;
   Uart2.IRQn = USART2_IRQn ;
   Uart2.UART_RX_BUF_SIZE = UART2_RX_BUF_SIZE ;
   Uart2.UART_TX_BUF_SIZE = UART2_TX_BUF_SIZE ;
   Uart2.Rx_Buff = UART2_RX_BUF ;
   Uart2.Tx_Buff = UART2_TX_BUF ;

   // Peripheral clock enable for UART
   __HAL_RCC_USART2_CLK_ENABLE() ;

   // Disable UART
   USART2->CR1 = 0 ;

   // USART2 GPIO Configuration:
   //    PA2     ------> USART2_TX;  AF value 7
   //    PD6     ------> USART2_RX;  AF value 7
#if 0
   temp = GPIOA->AFR[0] ;
   temp &= 0xFFFFF0FF ;
   temp |= 0x00000700 ;
   GPIOA->AFR[0] = temp ;
   // IO Direction mode: Alternate
   temp = GPIOA->MODER ;
   temp &= 0xFFFFFFCF ;
   temp |= 0x00000020 ;
   GPIOA->MODER = temp ;
   // Configure the IO Speed: GPIO_SPEED_FREQ_VERY_HIGH (3)
   temp = GPIOA->OSPEEDR;
   temp |= 0x00000030 ;
   GPIOA->OSPEEDR = temp ;
   // Configure the IO Output Type: Push-Pull (0)
   temp = GPIOA->OTYPER ;
   temp &= 0xFFFB ;
   GPIOA->OTYPER = temp ;
   // No push-pull mode (0)
   temp = GPIOA->PUPDR ;
   temp &= 0xFFFFFFCF ;
   GPIOA->PUPDR = temp ;

   //    PD6     ------> USART2_RX;  AF value 7
   temp = GPIOD->AFR[0] ;
   temp &= 0xF0FFFFFF ;
   temp |= 0x07000000 ;
   GPIOD->AFR[0] = temp ;
   // IO Direction mode: Alternate
   temp = GPIOD->MODER ;
   temp &= 0xFFFFCFFF ;
   temp |= 0x00002000 ;
   GPIOD->MODER = temp ;
   // Configure the IO Speed: GPIO_SPEED_FREQ_VERY_HIGH (3)
   temp = GPIOD->OSPEEDR;
   temp |= 0x00003000 ;
   GPIOD->OSPEEDR = temp ;
   // Configure the IO Output Type: Push-Pull (0)
   temp = GPIOD->OTYPER ;
   temp &= 0xFFBF ;
   GPIOD->OTYPER = temp ;
   // No push-pull mode (0)
   temp = GPIOD->PUPDR ;
   temp &= 0xFFFFCFFF ;
   GPIOD->PUPDR = temp ;
#endif

   GPIO_InitStruct.Mode = GPIO_MODE_AF_PP ;
   GPIO_InitStruct.Pull = GPIO_NOPULL ;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH ;
   GPIO_InitStruct.Alternate = GPIO_AF7_USART2 ;

   GPIO_InitStruct.Pin = GPIO_PIN_2 ;
   HAL_GPIO_Init(GPIOA, &GPIO_InitStruct) ;

   GPIO_InitStruct.Pin = GPIO_PIN_6 ;
   HAL_GPIO_Init(GPIOD, &GPIO_InitStruct) ;

   // Configure UART
   return SetUARTConfig(&Uart2, BaudRate) ;
   }

