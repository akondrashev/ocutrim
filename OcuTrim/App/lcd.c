#include "os.h"
#include "lcd.h"
#include "fonts.h"
#include "sysdef.h"
#include "hdlc.h"
#include "key.h"

// Screen buffer at 0x24000000.  Cache: no cache
static uint8_t  __attribute__((section(".ram_lcd"))) __attribute__((aligned(32))) VRAM_L8[DISPLAY_WIDTH*DISPLAY_HEIGHT] ;

static void PrintStringNew(uint32_t x, uint32_t y, uint8_t FontColor, uint8_t BackColor, const GUI_FONT *pFont, volatile uint8_t *pRAM, const uint8_t *buf, uint32_t len) ;
static void LCDRenderThread(void *dummy) ;

RND_QUEUE  RenderQueue ;

uint8_t  TestScreenOn = 0 ;
uint8_t  LEDDisplayed = 0 ;
static volatile uint32_t LTDC_ISR = 0 ;

// Thread stack
static OS_THREAD_STACK LcdStack[LCD_STACK_SIZE] ;

// Rendering signal: when this event is set we have something to draw
static HANDLE  RenderEvent = NULL ;
static HANDLE  DMAReadyEvent = NULL ;
// Levels of brightness (the 8 values are non-linear but screen brightness is approximately linear)
#define PULSE  1000
static const uint16_t bright_levels[MAX_DURATION+1] = {10, 20, 40, 80, 160, 300, 500, 1000} ;

static const uint8_t ColorLed[8] = {BACK_COLOR, COLOR_RED, COLOR_YELLOW, COLOR_BLUE, COLOR_GREEN, COLOR_MAGENTA, COLOR_CYAN, COLOR_GREY} ;
static const uint8_t led_char = 4 ;

#define SetDMAMode(Mode)      DMA2D->CR = (Mode) | 1


static INLINE void RunDMA(uint32_t mode)
   {
   SetDMAMode(mode | DMA2D_ALL_INTERRUPTS) ;
   OS_EventWait(DMAReadyEvent, INFINITY) ;
   }


static void LTDC_Reload(void)
   {
   LTDC_ISR = 0 ;
   LTDC->IER |= LTDC_IT_RR ;
   LTDC->SRCR = LTDC_RELOAD_VERTICAL_BLANKING ;
   while ((LTDC_ISR & LTDC_IT_RR) == 0) ;
   LTDC->IER &= ~(uint32_t)LTDC_IT_RR ;
   }


/*
   LTDC GPIO Configuration:
       PC0     ------> LTDC_R5
       PA1     ------> LTDC_R2
       PA3     ------> LTDC_B5
       PA4     ------> LTDC_VSYNC
       PA5     ------> LTDC_R4
       PA6     ------> LTDC_G2
       PB0     ------> LTDC_R3
       PB1     ------> LTDC_R6
       PE11    ------> LTDC_G3
       PE12    ------> LTDC_B4
       PE13    ------> LTDC_DE
       PE14    ------> LTDC_CLK
       PE15    ------> LTDC_R7
       PB10    ------> LTDC_G4
       PB11    ------> LTDC_G5
       PD10    ------> LTDC_B3
       PC6     ------> LTDC_HSYNC
       PC7     ------> LTDC_G6
       PC9     ------> LTDC_B2
       PD3     ------> LTDC_G7
       PB8     ------> LTDC_B6
       PB9     ------> LTDC_B7
*/
int LCDInit(void)
   {
   register uint32_t  temp ;
   int  err ;
   GPIO_InitTypeDef GPIO_InitStruct ;

   memset(&RenderQueue, 0, sizeof(RenderQueue)) ;

   // Create rendering event
   if (RenderEvent == NULL)
      {
      RenderEvent = OS_EventCreate(0, 0, &err) ;
      if (RenderEvent == NULL) return 1 ;
      } ;
   // Create DMA ready event
   if (DMAReadyEvent == NULL)
      {
      DMAReadyEvent = OS_EventCreate(0, 0, &err) ;
      if (DMAReadyEvent == NULL) return 1 ;
      } ;

   // Enable LTDC clock
   __HAL_RCC_LTDC_CLK_ENABLE();

   // Configure LTDC pins
   GPIO_InitStruct.Mode = GPIO_MODE_AF_PP ;
   GPIO_InitStruct.Pull = GPIO_NOPULL ;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW ;
   GPIO_InitStruct.Alternate = GPIO_AF14_LTDC ;

   GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6 ;
   HAL_GPIO_Init(GPIOA, &GPIO_InitStruct) ;

   GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_8|GPIO_PIN_9 ;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct) ;

   GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_9 ;
   HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

   GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_3 ;
   HAL_GPIO_Init(GPIOD, &GPIO_InitStruct) ;

   GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
   HAL_GPIO_Init(GPIOE, &GPIO_InitStruct) ;

   GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1 ;
   GPIO_InitStruct.Alternate = GPIO_AF9_LTDC ;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   // Configures the HS, VS, DE and PC polarity
   LTDC->GCR &= ~(LTDC_GCR_HSPOL | LTDC_GCR_VSPOL | LTDC_GCR_DEPOL | LTDC_GCR_PCPOL);
   LTDC->GCR |=  LTDC_HSPOLARITY_AL | LTDC_VSPOLARITY_AL | LTDC_DEPOLARITY_AL | LTDC_PCPOLARITY_IPC ;

   // Sets Synchronization size
   LTDC->SSCR &= ~(LTDC_SSCR_VSH | LTDC_SSCR_HSW);
   LTDC->SSCR |= ((KOE640480_HSYNC - 1) << 16) | (KOE640480_VSYNC - 1) ;

   // Sets Accumulated Back porch
   LTDC->BPCR &= ~(LTDC_BPCR_AVBP | LTDC_BPCR_AHBP);
   LTDC->BPCR |= ((KOE640480_HSYNC + KOE640480_HBP - 1) << 16) | (KOE640480_VSYNC + KOE640480_VBP - 1) ;

   // Sets Accumulated Active Width
   LTDC->AWCR &= ~(LTDC_AWCR_AAH | LTDC_AWCR_AAW);
   LTDC->AWCR |= ((KOE640480_WIDTH + KOE640480_HSYNC + KOE640480_HBP - 1) << 16) | (KOE640480_HEIGHT + KOE640480_VSYNC + KOE640480_VBP - 1) ;

   // Sets Total Width
   LTDC->TWCR &= ~(LTDC_TWCR_TOTALH | LTDC_TWCR_TOTALW) ;
   LTDC->TWCR |= ((KOE640480_WIDTH + KOE640480_HSYNC + KOE640480_HBP + KOE640480_HFP - 1) << 16) | (KOE640480_HEIGHT + KOE640480_VSYNC + KOE640480_VBP + KOE640480_VFP - 1) ;

   // Sets the background color value
   LTDC->BCCR = 0 ;

   // Enable LTDC by setting LTDCEN bit
   LTDC->GCR |= LTDC_GCR_LTDCEN ;

   // Layer 0 configuration
   temp = ((640 + ((LTDC->BPCR & LTDC_BPCR_AHBP) >> 16)) << 16) ;
   LTDC_LAYER0->WHPCR &= ~(LTDC_LxWHPCR_WHSTPOS | LTDC_LxWHPCR_WHSPPOS) ;
   LTDC_LAYER0->WHPCR = (0 + ((LTDC->BPCR & LTDC_BPCR_AHBP) >> 16) + 1) | temp ;

   // Configures the vertical start and stop position
   temp = ((480 + (LTDC->BPCR & LTDC_BPCR_AVBP)) << 16) ;
   LTDC_LAYER0->WVPCR &= ~(LTDC_LxWVPCR_WVSTPOS | LTDC_LxWVPCR_WVSPPOS) ;
   LTDC_LAYER0->WVPCR  = (0 + (LTDC->BPCR & LTDC_BPCR_AVBP) + 1) | temp ;

   // Specifies the pixel format
   LTDC_LAYER0->PFCR = LTDC_PIXEL_FORMAT_L8 ;

   // Configures the default color values
   LTDC_LAYER0->DCCR = 0 ;

   // Specifies the constant alpha value
   LTDC_LAYER0->CACR = 255 ;

   // Specifies the blending factors */
   LTDC_LAYER0->BFCR = LTDC_BLENDING_FACTOR1_PAxCA | LTDC_BLENDING_FACTOR2_PAxCA ;

   // Configures the color frame buffer start address
   LTDC_LAYER0->CFBAR = (uint32_t)&VRAM_L8[0] ;

   // Configures the color frame buffer pitch in byte
   LTDC_LAYER0->CFBLR  = (640 << 16) | (640  + 3) ;

   // Configures the frame buffer line number
   LTDC_LAYER0->CFBLNR  = 480 ;

   // Enable LTDC_Layer by setting LEN bit
   LTDC_LAYER0->CR |= (uint32_t)LTDC_LxCR_LEN ;

   // Sets the Reload type
   LTDC->SRCR = LTDC_SRCR_IMR;

   // Graphics DMA init
   __HAL_RCC_DMA2D_CLK_ENABLE() ;
   DMA2D->CR = DMA2D_CR_ABORT ;
   while (DMA2D->CR & DMA2D_CR_START) ;
   DMA2D->OPFCCR = DMA2D_OUTPUT_RGB565 ;
   DMA2D->FGPFCCR = DMA2D_OUTPUT_RGB565 ;
   DMA2D_ClearFlags ;
   HAL_NVIC_SetPriority(DMA2D_IRQn, DMA2D_IRQ_PRIO, 0) ;
   NVIC_EnableIRQ(DMA2D_IRQn) ;

   // Enable interrupts from LCD
   LTDC->ICR = 0xF ;
   LTDC->LIPCR = 500 ;                 // Sets the Line Interrupt position

   // Configure LCD refresh interrupt (highest priority)
   HAL_NVIC_SetPriority(LTDC_IRQn, LTDC_IRQ_PRIO, 0) ;
   // Enable interrupt
   NVIC_EnableIRQ(LTDC_IRQn) ;

   // Make sure that settings are in effect
   LTDC_Reload() ;
   // Clear screen
   LCDcls(BACK_COLOR) ;

   // Configure back light PWM on TIM1
   // Enable clock for TIM1
   __HAL_RCC_TIM1_CLK_ENABLE() ;
   // Counter-up, clock division by 1, no buffered pre-load.  Disable counter
   TIM1->CR1 = 0 ;
   TIM1->CR2 = 0 ;
   // Counter value
   TIM1->ARR = PULSE ;
   // Frequency pre-scaler
   TIM1->PSC = 40 ;
   // Clock source: internal 16MHz
   TIM1->SMCR = 0 ;
   // Disable channel. Output polarity high.  Complementary polarity low
   TIM1->CCER = 0x8 ;
   // Configure channel 1 in output PWM mode
   TIM1->CCMR1 = TIM_OCMODE_PWM1 | TIM_CCMR1_OC1FE | TIM_CCMR1_OC1PE ;
   TIM1->BDTR = TIM_BREAKPOLARITY_HIGH | TIM_BREAK2POLARITY_HIGH ;
   // Set brightness level
   TIM1->CCR1 = bright_levels[OCUParameters.Brightness] ;

   // Configure back light pin: PIN9 in PORTE, Fnc=1
#if 0
   temp = GPIOE->AFR[1] ;
   temp &= 0xFFFFFF0F ;
   temp |= 0x00000010 ;
   GPIOE->AFR[1] = temp ;
   // IO Direction mode: Alternate (2)
   temp = GPIOE->MODER ;
   temp &= 0xFFF3FFFF ;
   temp |= 0x00080000 ;
   GPIOE->MODER = temp ;
   // Configure the IO Speed: GPIO_SPEED_FREQ_LOW (0)
   temp = GPIOE->OSPEEDR;
   temp &= 0xFFF3FFFF ;
   GPIOE->OSPEEDR = temp ;
   // Configure the IO Output Type: Push-Pull (0)
   temp = GPIOE->OTYPER ;
   temp &= 0xFDFF ;
   GPIOE->OTYPER = temp ;
   // No push-pull mode (0)
   temp = GPIOE->PUPDR ;
   temp &= 0xFFF3FFFF ;
   GPIOE->PUPDR = temp ;
#endif

   GPIO_InitStruct.Alternate = GPIO_AF1_TIM1 ;
   GPIO_InitStruct.Pin = GPIO_PIN_9 ;
   HAL_GPIO_Init(GPIOE, &GPIO_InitStruct) ;

   // Start PWM
   TIM1->CCER |= 1 ;
   TIM1->CR1  |= 1 ;
   TIM1->BDTR |= TIM_BDTR_MOE ;

   // Display test screen
   //TestScreenOn = LCDTestScreen() ;
   LoadPalette(OCUParameters.ColorSheme, 1) ;
   LTDC_Reload() ;
   LCDcls(BACK_COLOR) ;
   TestScreenOn = 0 ;

   // Start rendering thread
   return OS_ThreadStart(LCDRenderThread, LCD_PRIO, LcdStack, LCD_STACK_SIZE, NULL, NULL) ;
   }


void LCD_ShiftScreen(uint32_t Shift)
   {
   __disable_irq() ;
   // Configures the color frame buffer start address
   LTDC_LAYER0->CFBAR = (uint32_t)&VRAM_L8[640*Shift] ;
   // Configures the frame buffer line number
   LTDC_LAYER0->CFBLNR  = 480 - Shift ;
   // Apply parameters
   LTDC->SRCR = LTDC_RELOAD_VERTICAL_BLANKING ;
   __enable_irq() ;
   }


void SetBackLite(uint8_t value)
   {
   TIM1->CCR1 = bright_levels[value] ;
   }


static INLINE void LCDBox2(uint32_t Start_x, uint32_t Start_y, uint32_t End_x, uint32_t End_y)
   {
   register uint32_t  w, h ;

   Start_x = (Start_x * FONT_X_SMALL) & 0xFFFFFFFE ;
   End_x = (8 + (End_x + 2) * FONT_X_SMALL) & 0xFFFFFFFE ;
   w = End_x - Start_x ;
   h = 2 + End_y - Start_y ;
   End_y = h * FONT_Y_SMALL ;
   Start_y = (Start_y * FONT_Y_SMALL) >> 1 ;

   DMA2D->OMAR = (uint32_t)&VRAM_L8[Start_x + DISPLAY_WIDTH * Start_y] ;
   DMA2D->NLR = ((w >> 1) << 16) | End_y ;
   DMA2D->OOR = (DISPLAY_WIDTH - w) >> 1 ;
   DMA2D->OCOLR = BACK_COLOR | (BACK_COLOR << 8) ;
   RunDMA(DMA2D_R2M) ;

   Start_x = (Start_x + (FONT_X_SMALL >> 1)) & 0xFFFFFFFE ;
   Start_y += FONT_Y_SMALL >> 1 ;
   End_x = (End_x - (FONT_X_SMALL >> 1)) & 0xFFFFFFFE ;
   w = End_x - Start_x ;

   DMA2D->OMAR = (uint32_t)&VRAM_L8[Start_x + DISPLAY_WIDTH * Start_y] ;
   DMA2D->NLR = ((w >> 1) << 16) | 4 ;
   DMA2D->OCOLR = FONT_COLOR | (FONT_COLOR << 8) ;
   DMA2D->OOR = (DISPLAY_WIDTH - w) >> 1 ;
   RunDMA(DMA2D_R2M) ;

   DMA2D->OMAR = DMA2D->OMAR + (End_y - FONT_Y_SMALL) * DISPLAY_WIDTH ;
   RunDMA(DMA2D_R2M) ;

   DMA2D->OMAR = (uint32_t)&VRAM_L8[Start_x + DISPLAY_WIDTH * Start_y] ;
   DMA2D->NLR = ((4 >> 1) << 16) | (End_y - FONT_Y_SMALL) ;
   DMA2D->OOR = (DISPLAY_WIDTH - 4) >> 1 ;
   RunDMA(DMA2D_R2M) ;

   DMA2D->OMAR += w - 4 ;
   RunDMA(DMA2D_R2M) ;
   }


void RAM_FNC LCDcls(uint8_t Color)
   {
   DMA2D->OOR = 0 ;
   DMA2D->NLR = ((DISPLAY_WIDTH >> 1) << 16) | DISPLAY_HEIGHT ;
   DMA2D->OMAR = (uint32_t)VRAM_L8 ;
   DMA2D->OCOLR = Color | (Color << 8) ;
   RunDMA(DMA2D_R2M) ;
   LEDDisplayed = 0 ;
   }


static INLINE void DrawBitmap(uint32_t x, uint32_t y, const void *pBits, uint32_t width, uint32_t height)
   {
   DMA2D->OMAR = (uint32_t)&VRAM_L8[x + y * DISPLAY_WIDTH] ;
   DMA2D->NLR = ((width >> 1) << 16) | height ;
   DMA2D->OOR = (DISPLAY_WIDTH - width) >> 1 ;
   DMA2D->FGMAR = (uint32_t)pBits ;
   DMA2D->FGOR = 0 ;
   RunDMA(DMA2D_M2M) ;
   }


#define DrawLogo()      DrawBitmap((DISPLAY_WIDTH - LOGO_WIDTH) >> 1, ((DISPLAY_HEIGHT - LOGO_HEIGHT) >> 1)-75, &logo, LOGO_WIDTH, LOGO_HEIGHT)


// Sets current values of virtual LEDs
static INLINE void DrawLeds(uint8_t Leds)
   {
   register uint8_t left, right ;

   right = ColorLed[Leds & 0x7] ;
   left  = ColorLed[(Leds >> 3) & 0x7] ;
   if (right == BACK_COLOR && left == BACK_COLOR)
      {
      // Request to erase
      if (!LEDDisplayed) return ;
      LEDDisplayed = 0 ;
      }
   else
      LEDDisplayed = 1 ;
   PrintStringNew(LED_LEFT_POS,  LED_LINE+3, left,  BACK_COLOR, &GUI_FontConsolas21x16, VRAM_L8 + X_OFFSET, &led_char, 1) ;
   PrintStringNew(LED_RIGHT_POS, LED_LINE+3, right, BACK_COLOR, &GUI_FontConsolas21x16, VRAM_L8 + X_OFFSET, &led_char, 1) ;
   }



static INLINE void ProcessLCDFrames(void)
   {
   while (RenderQueue.cnt)
      {
      register RENDER *pElem = &RenderQueue.q[RenderQueue.first] ;

      switch (pElem->msgType)
         {
         case RND_MSG_CLEAR:
            LCDcls(BACK_COLOR) ;
            break ;

         case RND_MSG_TXT:
            if (pElem->Reverse)
               PrintStringNew(pElem->x, pElem->y, pElem->BackColor, pElem->FontColor, pElem->pFont, VRAM_L8 + X_OFFSET, pElem->buf, pElem->sz) ;
            else
               PrintStringNew(pElem->x, pElem->y, pElem->FontColor, pElem->BackColor, pElem->pFont, VRAM_L8 + X_OFFSET, pElem->buf, pElem->sz) ;
            break ;

         case RND_MSG_BOX:
            LCDBox2(pElem->x, pElem->y, pElem->x2, pElem->y2) ;
            break ;

         case RND_MSG_LOGO:
            DrawLogo() ;
            break ;

         case RND_MSG_LED:
            DrawLeds(pElem->sz) ;
         } ;

      RenderQueue.first = (RenderQueue.first + 1) & (RND_QUEUE_SZ - 1) ;
      __disable_irq() ;
      RenderQueue.cnt-- ;
      __enable_irq() ;
      } ;
   }

#ifdef _TEST_
volatile uint32_t lcd_start ;
#endif

// This interrupt happens when LCD is not rendering its memory buffer.
// We enable this interrupt, when we have something to render to wake up drawing thread
void RAM_FNC LTDC_IRQHandler(void)
   {
   register uint32_t isr = LTDC->ISR ;

   if (isr)
      {
      LTDC_ISR = isr ;
      LTDC->ICR = 0xF ;
      // Render stuff on the screen
      if (isr & LTDC_IT_LI)
         {
         // Disable interrupt from LCD
         LTDC->IER &= ~(uint32_t)LTDC_IT_LI ;
         // Release rendering thread
#ifdef _TEST_
         lcd_start = GetTickCount() ;
#endif
         OS_EventSet(RenderEvent) ;
         } ;
      } ;
   }


// This interrupt happens, when DMA2D completes processing.
// We signal this by rising event
void RAM_FNC DMA2D_IRQHandler(void)
   {
   DMA2D_ClearFlags ;
   OS_EventSet(DMAReadyEvent) ;
   }



static INLINE void OutChar(GUI_CHARINFO *pChar, uint32_t skipLines, uint32_t bytesPerLine, uint32_t lineTail, uint8_t FontColor, volatile uint8_t *pRAM)
   {
   register uint32_t height ;
   register const uint8_t *pbits ;
   uint32_t line_feed ;

   height = pChar->ImageLines ;
   if (!height) return ;
   pbits = pChar->pData ;
   line_feed = DISPLAY_WIDTH - (bytesPerLine << 3) - lineTail ;

   // Number of empty lines on the top (adjust for 24 bytes because of __CLZ() result used below)
   pRAM += DISPLAY_WIDTH * (pChar->BlankLines - skipLines) - 24 ;

   while (1)
      {
      register uint32_t  cnt = bytesPerLine ;
      do
         {
         register uint32_t c = *pbits++ ;
         // Scan value of "c". Set only bytes that correspond to bits set to 1 in "c".
         while (c)
            {
            register uint32_t  n = __CLZ(c) ;
            pRAM[n] = FontColor ;
            c ^= 0x80000000 >> n ;
            } ;
         pRAM += 8 ;
         }
      while (--cnt) ;

      // Line tail (less than 1 byte)
      if (lineTail)
         {
         register uint32_t c = *pbits++ ;
         // Scan value of "c". Set only bytes that correspond to bits set to 1 in "c".
         while (c)
            {
            register uint32_t  n = __CLZ(c) ;
            pRAM[n] = FontColor ;
            c ^= 0x80000000 >> n ;
            } ;
         pRAM += lineTail ;
         } ;

      // Next line
      if (--height == 0) return ;
      pRAM += line_feed ;
      } ;
   }


static INLINE GUI_CHARINFO* FindChar(uint8_t c, const GUI_FONT *pFont)
   {
   register const GUI_FONT_PROP *pProp = pFont->pProp ;

   while (c < pProp->First ||  pProp->Last < c)
      {
      pProp = pProp->pNext ;
      if (!pProp) return NULL ;
      } ;

   return &pProp->paCharInfo[c - pProp->First] ;
   } ;



static NO_INLINE RAM_FNC void PrintStringNew(uint32_t x, uint32_t y, uint8_t FontColor, uint8_t BackColor, const GUI_FONT *pFont, volatile uint8_t *pRAM, const uint8_t *buf, uint32_t len)
   {
   uint32_t  height = pFont->CellHeight ;
   uint32_t  pixelsPerLine = pFont->CellWidth ;
   uint32_t  bytesPerLine = pixelsPerLine >> 3 ;
   uint32_t  lineTail = pixelsPerLine & 0x7 ;
   uint32_t  skipLines = pFont->LinesToSkip ;
   register uint32_t maxLen, width ;

   // Does not fit vertically?
   if (y + height > DISPLAY_HEIGHT) return ;
   // Cut line to fit horizontally
   maxLen = (DISPLAY_WIDTH - x) / pixelsPerLine ;
   if (maxLen < len) len = maxLen ;
   if (len == 0) return ;
   // Start VRAM address
   pRAM += x + (y * DISPLAY_WIDTH) ;

   // Erase string background
   DMA2D->OMAR = (uint32_t)(pRAM + 1) & 0xFFFFFFFE ;
   width = maxLen = len * pixelsPerLine ;
   if (x & 0x1) width-- ;
   if (width & 0x1) width-- ;
   DMA2D->NLR = ((width >> 1) << 16) | height ;
   DMA2D->OOR = (DISPLAY_WIDTH - width) >> 1 ;
   DMA2D->OCOLR = BackColor | (BackColor << 8) ;
   RunDMA(DMA2D_R2M) ;
   // Make sure that RAM cache is updated after DMA operation, so we could render using CPU
   // SCB_InvalidateDCache_by_Addr((uint32_t)addr, H) ;

   // Edges (left and right vertical lines at non-even positions)
   if (x & 0x1)
      {
      register uint32_t  n = height ;
      register uint8_t  *p = (uint8_t*)pRAM ;
      do
         {
         *p = BackColor ;
         p += DISPLAY_WIDTH ;
         }
      while (--n) ;
      } ;
   // Right edge
   x += maxLen ;
   if (x & 0x1)
      {
      register uint32_t  n = height ;
      register uint8_t  *p = (uint8_t*)&pRAM[maxLen] ;
      do
         {
         *p = BackColor ;
         p += DISPLAY_WIDTH ;
         }
      while (--n) ;
      } ;
   // Render, if color is different from background
   if (FontColor != BackColor)
      {
      // Loop by characters
      do
         {
         register GUI_CHARINFO *bits = FindChar(*buf++, pFont) ;
         if (!bits)
            {
            bits = FindChar('?', pFont) ;
            if (!bits) return ;
            } ;
         OutChar(bits, skipLines, bytesPerLine, lineTail, FontColor, pRAM) ;
         pRAM += pixelsPerLine ;
         }
      while (--len) ;
      } ;
   // Make sure that RAM is updated, so LCD will render our changes
   // SCB_CleanDCache_by_Addr((uint32_t)addr, H) ;
   }


#ifdef _TEST_
uint32_t volatile lcd_max_time = 0 ;
#endif

/*********************************************************************
 *
 *                      LCD Rendering Thread
 *   LCD interrupt triggers semaphore when screen drawing is allowed
 *
 *********************************************************************/
static RAM_FNC void LCDRenderThread(void *dummy)
   {
#ifdef _TEST_
   register uint32_t  t ;
#endif

   while (!OS_EventWait(RenderEvent, INFINITY))
      {
      ProcessLCDFrames() ;
#ifdef _TEST_
      t = GetTickCount() - lcd_start ;
      if (t > lcd_max_time) lcd_max_time = t ;
#endif
      } ;
   // Cannot happen
   //DbgPrintf("Rendering thread exited?!\r\n") ;
   }


// Erase test screen
void LCDTestScreenOff(void)
   {
   if (TestScreenOn)
      {
      LoadPalette(OCUParameters.ColorSheme, 1) ;
      LTDC_Reload() ;
      LCDcls(BACK_COLOR) ;
      TestScreenOn = 0 ;
      } ;
   }
