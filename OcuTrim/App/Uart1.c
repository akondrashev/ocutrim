/**************************************************************************************************
 *
 *                                       UART1 Driver
 *                                   Debugging terminal
 *
 *  Pin configuration: USART11_TX - PB14; USART11_RX - PB15
 *  DMA operations:    RX - DMA2, Stream 5, Channel 4;  TX - DMA2, Stream 7, Channel 4.
 *  Exported functions:
 *                      UART1_Init()  - initializes and configures UART for a given baud rate;
 *                      Uart1_Get()   - reads data from UART
 *                      Uart1_Put()   - puts data into UART (may block)
 *
 ***************************************************************************************************/
#include "os.h"
#include "Uart.h"

#define  UART1_RX_BUF_SIZE        32
#define  UART1_TX_BUF_SIZE        256

// UART1 control block
UART_typ Uart1 ;
// UART1 RX buffer
static uint8_t  __attribute__((section(".ram_d3"))) UART1_RX_BUF[UART1_RX_BUF_SIZE] ;
// UART1 TX buffer
static uint8_t  __attribute__((section(".ram_d3"))) UART1_TX_BUF[UART1_TX_BUF_SIZE] ;


// USART11 initialization
int UART1_Init(uint32_t BaudRate)
   {
   GPIO_InitTypeDef  GPIO_InitStruct ;
//   register uint32_t  temp ;

   // Disable interrupts
   NVIC_DisableIRQ(USART1_IRQn) ;
   // Set interrupt priority
   HAL_NVIC_SetPriority(USART1_IRQn,  USART1_IRQ_PRIO, 0) ;

   // Setup UART1 control block
   memset(&Uart1, 0, sizeof(Uart1)) ;
   Uart1.hUart = USART1 ;
   Uart1.IRQn = USART1_IRQn ;
   Uart1.UART_RX_BUF_SIZE = UART1_RX_BUF_SIZE ;
   Uart1.UART_TX_BUF_SIZE = UART1_TX_BUF_SIZE ;
   Uart1.Rx_Buff = UART1_RX_BUF ;
   Uart1.Tx_Buff = UART1_TX_BUF ;

   // Peripheral clock enable for UART
   __HAL_RCC_USART1_CLK_ENABLE() ;

   // Disable UART
   USART1->CR1 = 0 ;

   // USART11 GPIO Configuration:
   //    PB14     ------> USART11_TX;  AF value 4
   //    PB15     ------> USART11_RX;  AF value 4
#if 0
   temp = GPIOB->AFR[1] ;
   temp &= 0x00FFFFFF ;
   temp |= 0x44000000 ;
   GPIOB->AFR[1] = temp ;
   // IO Direction mode: Alternate
   temp = GPIOB->MODER ;
   temp &= 0x0FFFFFFF ;
   temp |= 0xA0000000 ;
   GPIOB->MODER = temp ;
   // Configure the IO Speed: GPIO_SPEED_FREQ_VERY_HIGH (3)
   temp = GPIOB->OSPEEDR;
   temp |= 0xF0000000 ;
   GPIOB->OSPEEDR = temp ;
   // Configure the IO Output Type: Push-Pull (0)
   temp = GPIOB->OTYPER ;
   temp &= 0x3FFF ;
   GPIOB->OTYPER = temp ;
   // No push-pull mode (0)
   temp = GPIOB->PUPDR ;
   temp &= 0x0FFFFFFF ;
   GPIOB->PUPDR = temp ;
#endif

   GPIO_InitStruct.Mode = GPIO_MODE_AF_PP ;
   GPIO_InitStruct.Pull = GPIO_NOPULL ;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH ;
   GPIO_InitStruct.Alternate = GPIO_AF4_USART1 ;
   GPIO_InitStruct.Pin = GPIO_PIN_14 | GPIO_PIN_15 ;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct) ;

   // Configure UART
   return SetUARTConfig(&Uart1, BaudRate) ;
   }
