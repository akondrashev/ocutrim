#include "os.h"
#include "Uart.h"
#include "sound.h"
#include "key.h"
#include "hdlc.h"
#include "lcd.h"
#include "util.h"
#include "qspi.h"
#include "flash.h"

#ifdef _TEST_
extern volatile uint32_t  lcd_max_time ;
#endif
int BackgroundInit(void) ;

//CAN_HandleTypeDef hcan1;
//TIM_HandleTypeDef htim4;


static void SystemClock_Config(void) ;
static void MPUConfig(void) ;

#ifdef _TEST_
#define TEST_STACK_SIZE    1024
static OS_THREAD_STACK TestStack[TEST_STACK_SIZE] ;
HQUEUE  TestQueue = NULL ;

static void TestThread(void *p)
   {
   int   err, n = 0 ;
   char *pBuf ;

   TestQueue = OS_QueueCreate(5, &err) ;
   if (err)
      {
      DbgPrintf("Error %i creating Queue\r\n", err) ;
      return ;
      } ;

   while (1)
      {
      Tlog("Test thread %u running...\r\n", (uint32_t)p) ;
      pBuf = malloc(32) ;
      if (pBuf)
         {
         sprintf(pBuf, "Msg %u\r\n", ++n) ;
         err = OS_QueuePost(TestQueue, pBuf, INFINITY) ;
         if (err) DbgPrintf("Error %i posting to Queue\r\n", err) ;
         }
      else
         DbgPrintf("malloc() error\r\n") ;
      OS_Sleep(5000) ;
      } ;
   }

static OS_THREAD_STACK TestStack2[TEST_STACK_SIZE] ;
static void TestThread2(void *p)
   {
   int   err ;
   char *pBuf ;

   while (1)
      {
      pBuf = (char*)OS_QueueGet(TestQueue, INFINITY, &err) ;
      if (pBuf)
         {
         Tlog("Test thread %u running...\r\n%s\r\n", (uint32_t)p, pBuf) ;
         free(pBuf) ;
         }
      else
         DbgPrintf("Error %i getting from Queue\r\n", err) ;
      } ;
   }

static void ExitFnc(void *p)
   {
   OS_QueueDelete(TestQueue) ;
   DbgPrintf("Test thread %u exits...\r\n", (uint32_t)p) ;
   }
#endif


static struct tm tm ;


/*****************************************************************************************
 *
 *  Application entry point.
 *  This function will be converted into thread with priority MAIN_PRIO, when OS starts
 *
 *****************************************************************************************/
int main(void)
   {
   uint8_t c ;
   register int rc ;

   // Configure the system clock
   SystemClock_Config() ;
   // Configure MPU and cache
   MPUConfig() ;

   // Start kernel
   rc = OS_Start(MAIN_PRIO) ;

   // Start background thread
   rc |= BackgroundInit() ;

   // Buttons clicks
   SoundInit() ;

   // Start communication thread
   rc |= InitializePPSProtocol() ;

   // Keyboard
   rc |= KeyBoardInit() ;

   // Display initialization
   rc |= LCDInit() ;

   // Make screen red, if we could not initialize properly
   if (rc) LCDcls(COLOR_RED) ;

   goto VVV ;

   // Debugging console loop.  In production mode (no debug cable) this will be always frozen
   while (DbgGet(&c, 1, INFINITY))
      {
      switch (c)
         {
         case 'd':
            OCU_Debug ^= 1 ;
            DbgPrintf("OCU_Debug=%u\r\n", OCU_Debug) ;
            break ;

#ifdef _TEST_
         case 'f':
            TestFlash() ;
            break ;
#endif

         case 't':
            TX_Debug ^= 1 ;
            DbgPrintf("TX_Debug=%u\r\n", TX_Debug) ;
            break ;

         case 's':
            memset(&tm, 0, sizeof(tm)) ;
            tm.tm_year = 2019 - 1900 ;
            tm.tm_mon  = 4 ;
            tm.tm_mday = 28 ;
            tm.tm_hour = 15 ;
            OS_TimeSet(&tm) ;
            RTC_Get(&tm) ;
            Tlog("RTC: %04d-%02d-%02d %02d:%02d:%02d\r\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec) ;
            break ;

         case 'v':
            VVV:
            DbgPrintf("\r\nVersion %u-%u. Chip rev 0x%04X\r\n", Part_No, Version, DBGMCU->IDCODE >> 16) ;
            RTC_Get(&tm) ;
            Tlog("RTC: %04d-%02d-%02d %02d:%02d:%02d\r\n", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec) ;
            //CRC16Test() ;
            break ;

#ifdef _TEST_
         case 'z':
            TestQSPI() ;
            break ;
#endif

         case'r':
            OS_Reset() ;
            break ;

         case '\r':
            DbgPrintf("\r\n") ;
            break ;

#ifdef _TEST_
         case 'l':
            DbgPrintf("LCD time=%u\r\n", lcd_max_time) ;
            lcd_max_time = 0 ;
            break ;

         case 'y':
            {
            int err = OS_ThreadStart(TestThread, 10, TestStack, TEST_STACK_SIZE, (void*)10, ExitFnc) ;
            if (err)
               DbgPrintf("err=%i\r\n", err) ;
            else
               DbgPrintf("Test thread started\r\n") ;
            err = OS_ThreadStart(TestThread2, 11, TestStack2, TEST_STACK_SIZE, (void*)11, ExitFnc) ;
            if (err)
               DbgPrintf("err=%i\r\n", err) ;
            else
               DbgPrintf("Test thread 2 started\r\n") ;
            } ;
            break ;

         case 'x':
            {
            int err = OS_ThreadStop(11) ;
            if (err) DbgPrintf("err=%i\r\n", err) ;
            err = OS_ThreadStop(10) ;
            if (err) DbgPrintf("err=%i\r\n", err) ;
            } ;
            break ;
#endif

         default:
            DbgPrintf("%c", c) ;
         } ;
      } ;
   }



NO_INLINE int RAM_FNC SetFlashLatency(uint32_t FLatency)
   {
   if (FLatency < (FLASH->ACR & FLASH_ACR_LATENCY))
      {
      __ISB() ;
      __DSB() ;
      /* Program the new number of wait states to the LATENCY bits in the FLASH_ACR register */
      __HAL_FLASH_SET_LATENCY(FLatency);
      __ISB() ;
      __DSB() ;
      /* Check that the new number of wait states is taken into account to access the Flash memory by reading the FLASH_ACR register */
      while ((FLASH->ACR & FLASH_ACR_LATENCY) != FLatency) ;
      }
   return HAL_OK;
   }


/**
  * @brief System Clock Configuration
  * @retval None
  */
static void SystemClock_Config(void)
   {
   RCC_OscInitTypeDef RCC_OscInitStruct ;
   RCC_ClkInitTypeDef RCC_ClkInitStruct ;
   RCC_PeriphCLKInitTypeDef PeriphClkInitStruct ;

   // Supply configuration update enable
   MODIFY_REG(PWR->CR3, PWR_CR3_SCUEN, 0) ;
   // Wait till voltage level flag is set and supply configuration update flag is reset
   while (!__HAL_PWR_GET_FLAG(PWR_FLAG_ACTVOSRDY) && __HAL_PWR_GET_FLAG(PWR_FLAG_SCUEN)) ;

   // Configure the main internal regulator output voltage
   __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1) ;
   while ((PWR->D3CR & (PWR_D3CR_VOSRDY)) != PWR_D3CR_VOSRDY) ;

   // Enable D2 domain SRAM clocks (0x30000000 0x30020000 0x30040000 AXI)
   RCC->AHB2ENR |= RCC_AHB2ENR_D2SRAM1EN | RCC_AHB2ENR_D2SRAM2EN | RCC_AHB2ENR_D2SRAM3EN ;
   (void)(RCC->AHB2ENR) ;

   // Configure LSE Drive Capability
   HAL_PWR_EnableBkUpAccess() ;
   __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

   // Macro to configure the PLL clock source
   __HAL_RCC_PLL_PLLSOURCE_CONFIG(RCC_PLLSOURCE_HSE) ;

   //Initializes the CPU, AHB and APB busses clocks
   memset(&RCC_OscInitStruct, 0, sizeof(RCC_OscInitStruct)) ;
   RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
   RCC_OscInitStruct.HSEState = RCC_HSE_ON;
   RCC_OscInitStruct.LSEState = RCC_LSE_ON;
   RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
   RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
   RCC_OscInitStruct.PLL.PLLM = 2;
   RCC_OscInitStruct.PLL.PLLN = 64;
   RCC_OscInitStruct.PLL.PLLP = 2;
   RCC_OscInitStruct.PLL.PLLQ = 2;
   RCC_OscInitStruct.PLL.PLLR = 2;
   RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
   RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
   RCC_OscInitStruct.PLL.PLLFRACN = 0;
   if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
   //   Error_Handler();
    }

   //Initializes the CPU, AHB and APB busses clocks
   memset(&RCC_ClkInitStruct, 0, sizeof(RCC_ClkInitStruct)) ;
   RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2|RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
   RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
   RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
   RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
   RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
   RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
   RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
   RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

   if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
    {
   //   Error_Handler();
    }

   memset(&PeriphClkInitStruct, 0, sizeof(PeriphClkInitStruct)) ;
   PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC|RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_QSPI /*|RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_UART4|RCC_PERIPHCLK_FDCAN|RCC_PERIPHCLK_SPI2 */;
   PeriphClkInitStruct.PLL2.PLL2M = 2;
   PeriphClkInitStruct.PLL2.PLL2N = 16;
   PeriphClkInitStruct.PLL2.PLL2P = 2;
   PeriphClkInitStruct.PLL2.PLL2Q = 4;
   PeriphClkInitStruct.PLL2.PLL2R = 2;
   PeriphClkInitStruct.PLL2.PLL2RGE = RCC_PLL2VCIRANGE_3;
   PeriphClkInitStruct.PLL2.PLL2VCOSEL = RCC_PLL2VCOWIDE;
   PeriphClkInitStruct.PLL2.PLL2FRACN = 0;
   PeriphClkInitStruct.PLL3.PLL3M = 1;
   PeriphClkInitStruct.PLL3.PLL3N = 4;
   PeriphClkInitStruct.PLL3.PLL3P = 2;
   PeriphClkInitStruct.PLL3.PLL3Q = 2;
   PeriphClkInitStruct.PLL3.PLL3R = 4;
   PeriphClkInitStruct.PLL3.PLL3RGE = RCC_PLL3VCIRANGE_3;
   PeriphClkInitStruct.PLL3.PLL3VCOSEL = RCC_PLL3VCOWIDE;
   PeriphClkInitStruct.PLL3.PLL3FRACN = 0;
   PeriphClkInitStruct.QspiClockSelection = RCC_QSPICLKSOURCE_PLL2;
   //PeriphClkInitStruct.Spi123ClockSelection = RCC_SPI123CLKSOURCE_PLL;
   //PeriphClkInitStruct.FdcanClockSelection = RCC_FDCANCLKSOURCE_PLL;
   PeriphClkInitStruct.Usart234578ClockSelection = RCC_USART234578CLKSOURCE_PLL2;
   PeriphClkInitStruct.Usart16ClockSelection = RCC_USART16CLKSOURCE_D2PCLK2;
   //PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
   if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
    {
   //   Error_Handler();
    }


   // GPIO clock
   __HAL_RCC_GPIOA_CLK_ENABLE();
   __HAL_RCC_GPIOB_CLK_ENABLE();
   __HAL_RCC_GPIOC_CLK_ENABLE();
   __HAL_RCC_GPIOD_CLK_ENABLE();
   __HAL_RCC_GPIOE_CLK_ENABLE();
   __HAL_RCC_GPIOH_CLK_ENABLE();

   // DMA clock
   //__HAL_RCC_DMA1_CLK_ENABLE();
   //__HAL_RCC_DMA2_CLK_ENABLE();

   // Disable FPU
   SCB->CPACR = 0 ;

   // Lazy stacking enabled
//   FPU->FPCCR |= 0xC0000000 ;

   // 2 wait states for 200 MHz AXI bus (400 MHz / 2)
   SetFlashLatency(FLASH_LATENCY_2) ;
   }


static void MPUConfig(void)
   {
   // Disable MPU
   HAL_MPU_Disable() ;

   // Set RAM_LCD to not cacheable
   // 256K at 0x24000000
   MPU->RNR = MPU_REGION_NUMBER0 ;
   MPU->RBAR = 0x24000000 ;
   MPU->RASR = MPU_REGION_ENABLE|(MPU_REGION_SIZE_256KB<<1)|(MPU_ACCESS_NOT_SHAREABLE<<18)|(MPU_TEX_LEVEL1<<19)|(MPU_REGION_FULL_ACCESS<<24) ;
   // 64K at 0x24040000
   MPU->RNR = MPU_REGION_NUMBER1 ;
   MPU->RBAR = 0x24040000 ;
   MPU->RASR = MPU_REGION_ENABLE|(MPU_REGION_SIZE_64KB<<1)|(MPU_ACCESS_NOT_SHAREABLE<<18)|(MPU_TEX_LEVEL1<<19)|(MPU_REGION_FULL_ACCESS<<24) ;

   // Set RAM_D2 to not cacheable
   MPU->RNR = MPU_REGION_NUMBER2 ;
   // D2 region address
   MPU->RBAR = 0x30000000 ;
   // TEX=1, C=0, B=0
   MPU->RASR = MPU_REGION_ENABLE|(MPU_REGION_SIZE_256KB<<1)|(MPU_ACCESS_NOT_SHAREABLE<<18)|(MPU_TEX_LEVEL1<<19)|(MPU_REGION_FULL_ACCESS<<24) ;

   // Enable MPU with default region: all regions, except D2 and LCD, will use D-cache
   HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT) ;

   // Enable I-Cache
   SCB_EnableICache() ;
   // Enable D-Cache
   SCB_EnableDCache() ;
   }


#if 0

/* CAN1 init function */
static void MX_CAN1_Init(void)
   {
   hcan1.Instance = CAN1;
   hcan1.Init.Prescaler = 54;
   hcan1.Init.Mode = CAN_MODE_NORMAL;
   hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
   hcan1.Init.TimeSeg1 = CAN_BS1_1TQ;
   hcan1.Init.TimeSeg2 = CAN_BS2_1TQ;
   hcan1.Init.TimeTriggeredMode = DISABLE;
   hcan1.Init.AutoBusOff = DISABLE;
   hcan1.Init.AutoWakeUp = DISABLE;
   hcan1.Init.AutoRetransmission = DISABLE;
   hcan1.Init.ReceiveFifoLocked = DISABLE;
   hcan1.Init.TransmitFifoPriority = DISABLE;
   HAL_CAN_Init(&hcan1) ;
   }
#endif


#if 0
/* TIM4 init function */
static void MX_TIM4_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 0;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}
#endif

