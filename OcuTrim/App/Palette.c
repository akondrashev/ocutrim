#include "os.h"
#include "lcd.h"
#include "fonts.h"

// OCU color management

#define RGB_COLOR(i, r, g, b)    (((uint32_t)(i) << 24) | ((uint32_t)(r) << 16) | ((uint32_t)(g) << 8) | ((uint32_t)(b)))
#define RGB(r, g, b)             b, g, r


// Custom color schemes
static const uint8_t ColorSchemes[NUMBER_OF_SCHEMES][VARIABLE_COLORS_NUM*3] = {
      // Black on white
      {
      RGB(255, 255, 255),        //  0  Background
      RGB(  0,   0,   0),        //  1  Default font color
      RGB(  0,   0,   0),        //  2  Fare label
      RGB(255, 255, 255),        //  3  Fare label background
      RGB(  0,   0,   0),        //  4  Selected fare label
      RGB(255, 255, 255),        //  5  Selected fare label background
      RGB(255, 255, 255),        //  6  Current fare set
      RGB(  0,   0,   0),        //  7  Current fare set background
      RGB(  0,   0,   0),        //  8  Current preset value
      RGB(255, 255, 255),        //  9  Preset value background
      RGB(255, 255, 255),        // 10  Hastus
      RGB(  0,   0,   0),        // 11  Hastus background
      RGB(  0,   0,   0),        // 12  Driver revenue
      RGB(255, 255, 255),        // 13  Route
      RGB(  0,   0,   0),        // 14  Route background
      RGB(  0,   0,   0),        // 15  Time
      RGB(255, 255, 255),        // 16  Time background
      RGB(255, 255, 255),        // 17  Device status message
      RGB(  0,   0,   0),        // 18  Device status background
      RGB(  0,   0,   0),        // 19  Fare info
      RGB(255, 255, 255),        // 20  Fare info background
      RGB(  0,   0,   0),        // 21  Fare error message
      RGB(255, 255, 255),        // 22  Fare error message background
      RGB(  0,   0,   0),        // 23
      RGB(  0,   0,   0),        // 24
      RGB(  0,   0,   0),        // 25
      RGB(  0,   0,   0),        // 26
      RGB(  0,   0,   0),        // 27
      RGB(  0,   0,   0),        // 28
      RGB(  0,   0,   0),        // 29
      RGB(  0,   0,   0),        // 30
      RGB(  0,   0,   0)         // 31
      },
      // White on black
      {
      RGB(  0,   0,   0),        //  0  Background
      RGB(255, 255, 255),        //  1  Default font color
      RGB(255, 255, 255),        //  2  Fare label
      RGB(  0,   0,   0),        //  3  Fare label background
      RGB(255, 255, 255),        //  4  Selected fare label
      RGB(  0,   0,   0),        //  5  Selected fare label background
      RGB(  0,   0,   0),        //  6  Current fare set
      RGB(255, 255, 255),        //  7  Current fare set background
      RGB(  0,   0,   0),        //  8  Current preset value
      RGB(255, 255, 255),        //  9  Preset value background
      RGB(  0,   0,   0),        // 10  Hastus
      RGB(255, 255, 255),        // 11  Hastus background
      RGB(255, 255, 255),        // 12  Driver revenue
      RGB(  0,   0,   0),        // 13  Route
      RGB(255, 255, 255),        // 14  Route background
      RGB(255, 255, 255),        // 15  Time
      RGB(  0,   0,   0),        // 16  Time background
      RGB(  0,   0,   0),        // 17  Device status message
      RGB(255, 255, 255),        // 18  Device status background
      RGB(255, 255, 255),        // 19  Fare info
      RGB(  0,   0,   0),        // 20  Fare info background
      RGB(255, 255, 255),        // 21  Fare error message
      RGB(  0,   0,   0),        // 22  Fare error message background
      RGB(255, 255, 255),        // 23
      RGB(255, 255, 255),        // 24
      RGB(255, 255, 255),        // 25
      RGB(255, 255, 255),        // 26
      RGB(255, 255, 255),        // 27
      RGB(255, 255, 255),        // 28
      RGB(255, 255, 255),        // 29
      RGB(255, 255, 255),        // 30
      RGB(255, 255, 255)         // 31
      }
};


// Reload palette according to specified color scheme
// If bPermanent is not 0, constant colors are also generated and loaded
void LoadPalette(uint8_t PalIndex, uint8_t bPermanent)
   {
   register uint32_t  i, r, g, b ;

   // Variable colors
   if (PalIndex < NUMBER_OF_SCHEMES)
      {
      register const uint8_t  *p = ColorSchemes[PalIndex] ;
      // Load colors
      for (i = 0; i < VARIABLE_COLORS_NUM; i++)
         {
         r = *p++ ;
         r |= (*p++) << 8 ;
         r |= (*p++) << 16 ;
         r |= i << 24 ;
         LTDC_LAYER0->CLUTWR = r ;
         } ;
      } ;

   if (bPermanent)
      {
      // Generate permanent colors
      for (r = 0; r < 256; r += 51)
         {
         for (g = 0; g < 256; g += 51)
            {
            for (b = 0; b < 256; b += 51)
               {
               LTDC_LAYER0->CLUTWR = RGB_COLOR(i, r, g, b) ;
               i++ ;
               } ;
            } ;
         } ;
      // Special permanent colors for logo
      LTDC_LAYER0->CLUTWR = RGB_COLOR(i, 0, 130, 189) ;
      i++ ;
      LTDC_LAYER0->CLUTWR = RGB_COLOR(i, 0,  97, 189) ;
      i++ ;
      LTDC_LAYER0->CLUTWR = RGB_COLOR(i, 0,  65, 132) ;
      i++ ;
      LTDC_LAYER0->CLUTWR = RGB_COLOR(i, 0,  32,  66) ;
      } ;

   // Enable palette
   LTDC_LAYER0->CR |= (uint32_t)LTDC_LxCR_CLUTEN ;
   // Reload registers
   LTDC->SRCR = LTDC_SRCR_IMR ;
   }


/*
void SetPaletteColor(uint32_t index, uint32_t rgb)
   {
   LTDC_LAYER0->CLUTWR = rgb | (index << 24) ;
   LTDC->SRCR = LTDC_SRCR_IMR ;
   }


uint32_t GetPaletteColor(uint32_t index)
   {
   const uint8_t *p = &Palette[index + (index << 1)] ;
   index = *p++ ;
   index |= (*p++) << 8 ;
   index |= (*p) << 16 ;
   return index ;
   }
*/
