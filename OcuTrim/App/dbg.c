#include "os.h"
#include "Uart.h"

static HANDLE TxMutex = NULL ;
static HANDLE RXMutex = NULL ;
static char __attribute__((section(".ram_d3"))) buf[256] ;

// Debug terminal initialization
int InitDebug(void)
   {
   int   err ;

   // TX mutex
   if (TxMutex == NULL)
      {
      TxMutex = OS_MutexCreate(FALSE, &err) ;
      if (TxMutex == NULL) return err ;
      } ;
   // RX mutex
   if (RXMutex == NULL)
      {
      RXMutex = OS_MutexCreate(FALSE, &err) ;
      if (RXMutex == NULL) return err ;
      } ;
   // Debug terminal
   return UART1_Init(115200) ;
   }


// Debug output
void DbgPrintf(char *fmt, ...)
   {
   va_list  list ;
   register size_t   len ;

   if (OS_MutexClaim(TxMutex, 5000)) return ;
   va_start(list, fmt) ;
   len = vsnprintf(buf, sizeof(buf), fmt, list) ;
   va_end(list) ;
   if (len > 0)
      {
      if (len >= sizeof(buf)) len = sizeof(buf) - 1 ;
      Uart1_Put((uint8_t*)buf, len) ;
      } ;
   OS_MutexRelease(TxMutex) ;
   }


// Debug output with time stamp
void Tlog(char *fmt, ...)
   {
   struct tm tm ;
   va_list   list ;
   register size_t  len ;
   time_t    t ;

   t = OS_time(NULL) ;
   gmtime_r(&t, &tm) ;
   if (OS_MutexClaim(TxMutex, 5000)) return ;
   len = sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d ", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec) ;
   va_start(list, fmt) ;
   len += vsnprintf(&buf[len], sizeof(buf) - len, fmt, list) ;
   va_end(list) ;
   if (len >= sizeof(buf)) len = sizeof(buf) - 1 ;
   Uart1_Put((uint8_t*)buf, len) ;
   OS_MutexRelease(TxMutex) ;
   }


// Debug input
uint32_t DbgGet(uint8_t *pBuf, uint32_t size, uint32_t timeout)
   {
   register uint32_t  len ;

   if (OS_MutexClaim(RXMutex, timeout)) return 0 ;
   len = Uart1_Get(pBuf, size, timeout) ;
   OS_MutexRelease(RXMutex) ;
   return len ;
   }
