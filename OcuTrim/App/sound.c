/************************************************************************************
 *
 *                                     Buzzer
 *
 *    PORTA PIN0 is attached to the buzzer.
 *
 *    PWM with 100% output (always 1) is configured for this pin using TIMER2.
 *    When beep is needed and Beep() function is called, the PWM output is set to
 *    50% duty cycle.  The buzzer is active when pin output is low (oscillates).
 *
 *    Sound duration regulates by TIMER6.
 *
 *    When beep duration is reached, PWM output is returned back to 100% duty.
 *
 ************************************************************************************/
#include "os.h"
#include "stm32h7xx_hal_gpio.h"

#define BEEP_DURATION         25
#define Buzzer_Pin            GPIO_PIN_0     // PORTA
#define PULSE                 1000
#define TIM6_ONE_PULSE        ((1 << 3) | (1 << 2) | 1)


// Stop buzzer interrupt
void RAM_FNC TIM6_DAC_IRQHandler(void)
   {
   TIM6->SR = 0 ;                      // Clear interrupt
   TIM2->CCR1 = PULSE ;                // Turn sound off
   }


// Start beep
void Beep(void)
   {
   TIM2->CCR1 = PULSE >> 1 ;           // Start sound generation
   TIM6->CR1 = TIM6_ONE_PULSE ;        // Start timer to turn sound off
   }


// Buzzer's PWM initialization
void SoundInit(void)
   {
   GPIO_InitTypeDef  GPIO_InitStruct ;
   //register uint32_t  temp ;

   // Set buzzer pin output to 1
   GPIOA->BSRRL = Buzzer_Pin ;

   // Prepare buzzer stop timer
   __HAL_RCC_TIM6_CLK_ENABLE() ;
   TIM6->CR1 = 0 ;
   TIM6->CR2 = 0 ;
   TIM6->PSC = 19999 ;                          // Make timer clock 10000 Hz
   TIM6->ARR = BEEP_DURATION*10 ;               // Duration is in ms
   TIM6->EGR = 1 ;                              // Update registers
   TIM6->SR = 0 ;                               // Clear interrupt
   HAL_NVIC_SetPriority(TIM6_DAC_IRQn, TIM6_IRQ_PRIO, 0) ; // Interrupt priority
   NVIC_EnableIRQ(TIM6_DAC_IRQn) ;              // Enable interrupt in NVIC
   TIM6->DIER = 1 ;                             // Interrupt enable from timer

   // Enable clock for TIM2
   __HAL_RCC_TIM2_CLK_ENABLE() ;
   // Disable counter
   TIM2->CR1 = 0 ;
   TIM2->CR2 = 0 ;
   // Counter value
   TIM2->ARR = PULSE ;
   // Frequency pre-scaler
   TIM2->PSC = 40 ;
   // Clock source: internal 64MHz
   TIM2->SMCR = 0 ;
   // Disable channel. Output polarity high
   TIM2->CCER = 0 ;
   // Configure channel 1 in output PWM mode
   TIM2->CCMR1 = TIM_OCMODE_PWM1 | TIM_CCMR1_OC1FE | TIM_CCMR1_OC1PE ;
   // Capture Compare Register value - same, as period
   TIM2->CCR1 = PULSE ;

   // PIN configuration: PA0, AF value 1 (TIM2 channel 1)
#if 0
   temp = GPIOA->AFR[0] ;
   temp &= 0xFFFFFFF0 ;
   temp |= 0x00000001 ;
   GPIOA->AFR[0] = temp ;
   // IO Direction mode: Alternate
   temp = GPIOA->MODER ;
   temp &= 0xFFFFFFFC ;
   temp |= 0x00000002 ;
   GPIOA->MODER = temp ;
   // Configure the IO Speed: GPIO_SPEED_SLOW (0)
   temp = GPIOA->OSPEEDR;
   temp &= 0xFFFFFFFC ;
   GPIOA->OSPEEDR = temp ;
   // Configure the IO Output Type: Push-Pull (0)
   temp = GPIOA->OTYPER ;
   temp &= 0xFFFE ;
   GPIOA->OTYPER = temp ;
   // No push-pull mode (0)
   temp = GPIOA->PUPDR ;
   temp &= 0xFFFFFFFC ;
   GPIOA->PUPDR = temp ;
#endif

   GPIO_InitStruct.Mode = GPIO_MODE_AF_PP ;
   GPIO_InitStruct.Pull = GPIO_NOPULL ;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW ;
   GPIO_InitStruct.Alternate = GPIO_AF1_TIM2 ;
   GPIO_InitStruct.Pin = GPIO_PIN_0 ;
   HAL_GPIO_Init(GPIOA, &GPIO_InitStruct) ;

   // Start timer
   TIM2->CCER = 1 ;
   // Counter-up, clock division by 4, no buffered pre-load.
   TIM2->CR1  = (0x2 << 4) | 1 ;
   }

