#include "os.h"
#include "hdlc.h"
#include "fs_app.h"
#include "util.h"

const char CONFIG_FILE_NAME[] = "0:config.bin" ;
const char CONFIG_TEMP_NAME[] = "0:config.tmp" ;

// Thread's stack
static OS_THREAD_STACK BkgrStack[BGRD_STACK_SIZE] ;
// Local copy of file contents
static OCU_PARAMS  localCopy ;


// Saves run-time data to flash
void BackgroundThread(void *dummy)
   {
   FRESULT  rc ;
   register FIL  *fd ;

   // Wake-up on event set (this loop never exits)
   while (!WaitOnTimerEvent(SAVE_PARAMS_TIMER))
      {
      __disable_irq() ;
      memcpy(&localCopy, &OCUParameters, sizeof(OCU_PARAMS)-2) ;
      __enable_irq() ;
      localCopy.crc = crc16((uint8_t*)&localCopy, sizeof(OCU_PARAMS)-2) ;

      rc = fs_mount("0") ;
      if (rc != FR_OK)
         {
         DbgPrintf("Error %u mounting volume\r\n", rc) ;
         continue ;
         } ;
      // Create temporary file
      fd = fs_open(CONFIG_TEMP_NAME, FA_READ|FA_WRITE|FA_CREATE_ALWAYS, &rc) ;
      if (rc != FR_OK)
         {
         DbgPrintf("Error %u temp file\r\n", rc) ;
         goto FIN ;
         } ;
      // Write data
      rc = fs_write(fd, &localCopy, sizeof(localCopy)) ;
      if (rc != FR_OK)
         {
         DbgPrintf("Error %u writing file\r\n", rc) ;
         goto FIN ;
         } ;
      // Close file
      rc = fs_close(fd) ;
      if (rc == FR_OK && OCU_Debug) DbgPrintf("Settings saved\r\n") ;
      fd = NULL ;
      // Delete old file
      rc = f_unlink (CONFIG_FILE_NAME) ;
      if (rc != FR_OK && rc != FR_NO_FILE)
         {
         DbgPrintf("Error %u deleting file\r\n", rc) ;
         goto FIN ;
         } ;
      // Rename temporary file
      rc = f_rename (CONFIG_TEMP_NAME, CONFIG_FILE_NAME) ;
      if (rc != FR_OK) DbgPrintf("Error %u renaming file\r\n", rc) ;

    FIN:
      if (fd) fs_close(fd) ;
      fs_unmount("0") ;
      } ;
   }




int BackgroundInit(void)
   {
   register int err ;

   // Create timer for save event. This timer provides 5 sec delay between buttons and actual flash operations
   err = CreateTmr(SAVE_PARAMS_TIMER|TIMER_CREATE_EVENT_AUTO, 5000, NULL, NULL) ;
   if (err) return err ;
   // Create thread
   return OS_ThreadStart(BackgroundThread, BACKGROUND_PRIO, BkgrStack, BGRD_STACK_SIZE, NULL, NULL) ;
   }
