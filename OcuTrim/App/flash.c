#include "os.h"
#include "hdlc.h"
#include "flash.h"
#include "lcd.h"

#define BANK_1                      0x1
#define BANK_2                      0x2
#define BANK_BOTH                   (BANK_1 | BANK_2)
#define IS_BANK2(address)           (address & 0x100000)             // Bank size is 1MB
#define IS_SECTOR_START(address)    (0 == (address & 0x1FFFF))       // Sector size is 128K
#define SECTOR_NUM(address)         ((address >> 17) & 0x7)          // Each bank has 8 sectors



static NO_INLINE uint32_t RAM_FNC FLASH_WaitComplete(uint32_t bank)
   {
   register uint32_t status = 0 ;

   __ISB() ;
   __DSB() ;

   if (bank & BANK_1)
      {
      while ((status = FLASH->SR1) & (FLASH_FLAG_BSY | FLASH_FLAG_QW | FLASH_SR_WBNE)) ;
      FLASH->CCR1 = FLASH_FLAG_EOP_BANK1 | FLASH_FLAG_QW_BANK1 | FLASH_FLAG_WBNE_BANK1 | FLASH_FLAG_ALL_ERRORS_BANK1 | FLASH_SR_EOP ;
      FLASH->CR1 = 0 ;
      status = status & FLASH_FLAG_ALL_ERRORS_BANK1 ;
      if (status) return status ;
      } ;

   if (bank & BANK_2)
      {
      while ((status = FLASH->SR2) & (FLASH_FLAG_BSY | FLASH_FLAG_QW | FLASH_SR_WBNE)) ;
      FLASH->CCR2 = FLASH_FLAG_EOP_BANK1 | FLASH_FLAG_QW_BANK1 | FLASH_FLAG_WBNE_BANK1 | FLASH_FLAG_ALL_ERRORS_BANK1 | FLASH_SR_EOP ;
      FLASH->CR2 = 0 ;
      status = status & FLASH_FLAG_ALL_ERRORS_BANK1 ;
      if (status) return status ;
      } ;

   return status ;
   }


// Program flash at "Address" with 32-byte data pointed by "pData"
static INLINE uint32_t RAM_FNC FLASH_Program_Word(uint32_t bank, volatile uint64_t *dest_addr, volatile uint64_t *src_addr)
   {
   if (bank == BANK_1)
      FLASH->CR1 =  FLASH_VOLTAGE_RANGE_4 | FLASH_CR_PG ;
   else
      FLASH->CR2 = FLASH_VOLTAGE_RANGE_4 | FLASH_CR_PG ;

   __ISB() ;
   __DSB() ;

   *dest_addr++ = *src_addr++ ;
   *dest_addr++ = *src_addr++ ;
   *dest_addr++ = *src_addr++ ;
   *dest_addr = *src_addr ;

   return FLASH_WaitComplete(bank) ;
   }



static INLINE uint32_t RAM_FNC EraseSector(uint32_t bank, uint32_t s)
   {
   if (bank == BANK_1)
      {
      FLASH->CR1 &= ~(FLASH_CR_PSIZE | FLASH_CR_SNB) ;
      FLASH->CR1 |= FLASH_CR_SER | FLASH_VOLTAGE_RANGE_4 | (s << 8) ;
      FLASH->CR1 |= FLASH_CR_START ;
      }
   else
      {
      FLASH->CR2 &= ~(FLASH_CR_PSIZE | FLASH_CR_SNB) ;
      FLASH->CR2 |= FLASH_CR_SER | FLASH_VOLTAGE_RANGE_4 | (s << 8) ;
      FLASH->CR2 |= FLASH_CR_START ;
      } ;

   return FLASH_WaitComplete(bank) ;
   }


// Unlock flash
static INLINE uint32_t RAM_FNC FLASH_Unlock(void)
   {
   FLASH_WaitComplete(BANK_BOTH) ;

   if (FLASH->CR1 & FLASH_CR_LOCK)
      {
      FLASH->KEYR1 = FLASH_KEY1 ;
      FLASH->KEYR1 = FLASH_KEY2 ;
      if (FLASH->CR1 & FLASH_CR_LOCK) return 1 ;
      } ;

   if (FLASH->CR2 & FLASH_CR_LOCK)
      {
      FLASH->KEYR2 = FLASH_KEY1 ;
      FLASH->KEYR2 = FLASH_KEY2 ;
      if (FLASH->CR2 & FLASH_CR_LOCK) return 1 ;
      } ;

   return 0 ;
   }


// Programs new firmware using image in external flash
NO_INLINE void RAM_FNC ProgramFlash(FW_IMAGE *pProgram)
   {
   register uint32_t  flash_address = START_PROGRAM_ADDRESS ;
   register uint32_t  nWords = (pProgram->ImageSize + 31) >> 5 ;
   register uint8_t  *pRAM = (uint8_t*)pProgram + sizeof(FW_IMAGE) ;

   // Disable interrupts
   __disable_irq() ;
   // Unlock flash
   if (FLASH_Unlock()) goto ERR ;

   // Programming loop
   do
      {
      register uint32_t  bank = IS_BANK2(flash_address) ? BANK_2 : BANK_1 ;

      if (IS_SECTOR_START(flash_address))
         {
         if (EraseSector(bank, SECTOR_NUM(flash_address))) goto ERR ;
         } ;
      if (FLASH_Program_Word(bank, (uint64_t*)flash_address, (uint64_t*)pRAM)) goto ERR ;
      flash_address += 32 ;
      pRAM += 32 ;
      }
   while (--nWords) ;
   // Reset
   OS_Reset() ;

  ERR:
   __enable_irq() ;
   LCDcls(71) ;
   }

