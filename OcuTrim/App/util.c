#include "os.h"
#include "util.h"

#if 0

#define BUFFER_SIZE    114

static const uint32_t aDataBuffer[BUFFER_SIZE] =
{
  0x00001021, 0x20423063, 0x408450a5, 0x60c670e7, 0x9129a14a, 0xb16bc18c,
  0xd1ade1ce, 0xf1ef1231, 0x32732252, 0x52b54294, 0x72f762d6, 0x93398318,
  0xa35ad3bd, 0xc39cf3ff, 0xe3de2462, 0x34430420, 0x64e674c7, 0x44a45485,
  0xa56ab54b, 0x85289509, 0xf5cfc5ac, 0xd58d3653, 0x26721611, 0x063076d7,
  0x569546b4, 0xb75ba77a, 0x97198738, 0xf7dfe7fe, 0xc7bc48c4, 0x58e56886,
  0x78a70840, 0x18612802, 0xc9ccd9ed, 0xe98ef9af, 0x89489969, 0xa90ab92b,
  0x4ad47ab7, 0x6a961a71, 0x0a503a33, 0x2a12dbfd, 0xfbbfeb9e, 0x9b798b58,
  0xbb3bab1a, 0x6ca67c87, 0x5cc52c22, 0x3c030c60, 0x1c41edae, 0xfd8fcdec,
  0xad2abd0b, 0x8d689d49, 0x7e976eb6, 0x5ed54ef4, 0x2e321e51, 0x0e70ff9f,
  0xefbedfdd, 0xcffcbf1b, 0x9f598f78, 0x918881a9, 0xb1caa1eb, 0xd10cc12d,
  0xe16f1080, 0x00a130c2, 0x20e35004, 0x40257046, 0x83b99398, 0xa3fbb3da,
  0xc33dd31c, 0xe37ff35e, 0x129022f3, 0x32d24235, 0x52146277, 0x7256b5ea,
  0x95a88589, 0xf56ee54f, 0xd52cc50d, 0x34e224c3, 0x04817466, 0x64475424,
  0x4405a7db, 0xb7fa8799, 0xe75ff77e, 0xc71dd73c, 0x26d336f2, 0x069116b0,
  0x76764615, 0x5634d94c, 0xc96df90e, 0xe92f99c8, 0xb98aa9ab, 0x58444865,
  0x78066827, 0x18c008e1, 0x28a3cb7d, 0xdb5ceb3f, 0xfb1e8bf9, 0x9bd8abbb,
  0x4a755a54, 0x6a377a16, 0x0af11ad0, 0x2ab33a92, 0xed0fdd6c, 0xcd4dbdaa,
  0xad8b9de8, 0x8dc97c26, 0x5c644c45, 0x3ca22c83, 0x1ce00cc1, 0xef1fff3e,
  0xdf7caf9b, 0xbfba8fd9, 0x9ff86e17, 0x7e364e55, 0x2e933eb2, 0x0ed11ef0
};



static uint16_t Tbl[256] ;

void crc_table16( void )
{
  uint16_t i, z;

  for (i = 0; i < 256; i++)
  {
    z = (i>>4) ^ (i&0x0f);                  /* z = parity(i) ? 0xc001 : 0 */
    z = (z>>2) ^ (z&0x03);                  /* "     "           "        */
    z = ((z>>1) ^ (z&0x01)) ? 0xc001: 0;    /* "     "           "        */
    Tbl[i] = z ^ i<<6 ^ i<<7;
  }
}


/*
** Generate a crc16 checksum on n bytes of data
*/
uint16_t _crc16(uint8_t *ptr, uint32_t cnt, uint16_t crc)
   {
   while (cnt--)
      {
      crc = (crc >> 8) ^ Tbl[*ptr++ ^ (crc & 0x00ff)] ;
      } ;
   return crc ;
   }



void CRC16Test(void)
   {
   uint16_t  crc ;
   register uint32_t  len ;
   register uint8_t  *p ;

   crc_table16() ;
   p = (uint8_t*)aDataBuffer ;

   len = sizeof(aDataBuffer) ;
   crc = _crc16(p, len, 0xFFFF) ;
   DbgPrintf("CRC=0x%04X\r\n", crc) ;
   crc = crc16ex(p, len, 0xFFFF, CRC_FINAL) ;
   DbgPrintf("CRC=0x%04X\r\n", crc) ;

   len = 1 ;
   crc = _crc16(p, len, 0xFFFF) ;
   DbgPrintf("\r\nCRC=0x%04X\r\n", crc) ;
   crc = crc16ex(p, len, 0xFFFF, CRC_FINAL) ;
   DbgPrintf("CRC=0x%04X\r\n", crc) ;

   len = sizeof(aDataBuffer) ;
   crc = _crc16(p, len-7, 0xFFFF) ;
   DbgPrintf("\r\nCRC=0x%04X\r\n", crc) ;
   crc = _crc16(p + len - 7, 7, crc) ;
   DbgPrintf("CRC=0x%04X\r\n", crc) ;

   crc = crc16ex(p, len-7, 0xFFFF, CRC_PARTIAL) ;
   DbgPrintf("\r\nCRC=0x%04X\r\n", crc) ;
   crc = crc16ex(p + len - 7, 7, crc, CRC_FINAL) ;
   DbgPrintf("CRC=0x%04X\r\n", crc) ;

   p = 2 + (uint8_t*)aDataBuffer ;
   len = sizeof(aDataBuffer) - 2 ;
   crc = _crc16(p, len, 0xFFFF) ;
   DbgPrintf("\r\nCRC=0x%04X\r\n", crc) ;
   crc = crc16ex(p, len, 0xFFFF, CRC_FINAL) ;
   DbgPrintf("CRC=0x%04X\r\n", crc) ;

   p = 1 + (uint8_t*)aDataBuffer ;
   len = sizeof(aDataBuffer) - 1 ;
   crc = _crc16(p, len, 0xFFFF) ;
   DbgPrintf("\r\nCRC=0x%04X\r\n", crc) ;
   crc = crc16ex(p, len, 0xFFFF, CRC_FINAL) ;
   DbgPrintf("CRC=0x%04X\r\n", crc) ;

   }
#endif

#if 0
#define ulPolynomial       (uint32_t)0x04c11db7

// Global synchronization mutex to control access to CRC controller
static OS_SYNC *CRC_Mutex = NULL ;


// Global one-time initialation
int crc_table32(void)
   {
   int  err ;

   if (CRC_Mutex) return 0 ;
   CRC_Mutex = OS_MutexCreate(FALSE, &err) ;
   return err ;
   }


// Partial CRC for 32-bit data
uint32_t crc32ex(const void *src, uint32_t cnt, uint32_t crc, uint8_t bFinal)
   {
   register uint32_t  *ptr32 ;
   register uint16_t  *ptr16 ;
   register uint8_t   *ptr8 ;

   if (OS_MutexClaim(CRC_Mutex, INFINITY) || cnt == 0) return crc ;
   __HAL_RCC_CRC_CLK_ENABLE() ;

   CRC->INIT = crc ;
   if (bFinal)
      CRC->CR = CRC_CR_REV_IN | CRC_CR_REV_OUT ;
   else
      CRC->CR = CRC_CR_REV_IN ;
   CRC->POL = ulPolynomial ;
   CRC->CR |= CRC_CR_RESET ;

   switch (0x3 & (uint32_t)src)
      {
      case 0:
         ptr32 = (uint32_t*)src ;
         do
            {
            CRC->DR = *ptr32++ ;
            }
         while (--cnt) ;
         break ;

      case 2:
         ptr16 = (uint16_t*)src ;
         do
            {
            CRC->DR = (uint32_t)(ptr16[0]) | ((uint32_t)(ptr16[1]) << 16) ;
            ptr16 += 2 ;
            }
         while (--cnt) ;
         break ;

      default:
         ptr8 = (uint8_t*)src ;
         do
            {
            CRC->DR = (uint32_t)(ptr8[0]) | ((uint32_t)(ptr8[1]) << 8)  | ((uint32_t)(ptr8[2]) << 16)  | ((uint32_t)(ptr8[3]) << 24) ;
            ptr8 += 4 ;
            }
         while (--cnt) ;
      } ;

   crc = CRC->DR ;
   __HAL_RCC_CRC_CLK_DISABLE() ;
   OS_MutexRelease(CRC_Mutex) ;
   return crc ;
   }
#endif
