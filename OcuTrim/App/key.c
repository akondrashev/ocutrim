#include "os.h"
#include "Key.h"
#include "sound.h"
#include "hdlc.h"
#include "lcd.h"

#define  KEY_SCAN_INTERVAL_MS          2
#define  KEY_ON_INTERVAL_MS            30
#define  KEY_OFF_INTERVAL_MS           16

typedef void (*FPTR2)(uint32_t) ;

static void Dump(uint32_t Sensor_Flags) ;
static void Blue(uint32_t Sensor_Flags) ;

#define OFF_DEBOUNCE_TIME        (KEY_OFF_INTERVAL_MS/KEY_SCAN_INTERVAL_MS)
#define ON_DEBOUNCE_TIME         (KEY_ON_INTERVAL_MS/KEY_SCAN_INTERVAL_MS)
#define MAX_ROW                  5

// De-bouncing timer
typedef struct
   {
   uint16_t   t;              // timer
   uint16_t   d;              // debounce value
   const uint32_t  f;         // key flag
   const FPTR2 fptr;          // function pointer
}  DEBOUNCE_TMR;


#define  MAX_SENSOR           2

static DEBOUNCE_TMR Tmr[MAX_SENSOR] = {
   {0, ON_DEBOUNCE_TIME,  E_Pin,  Dump},
   {0, ON_DEBOUNCE_TIME,  C_Pin,  Blue}
};

static const uint16_t  row_pin[MAX_ROW] = {Row0, Row1, Row2, Row3, Row4} ;

// Keys Matrix, as it is layout on the board
#if 0
static const char KeyMapTbl[5][6] = {
      {'U', 'V', 'W', 'X', 'Y', 'Z'},
      {'1', '2', '3', '8', '7', '6'},
      {'A', '5', '4', '9', '0', 'F'},
      {'B', '*', 'E', 'D', 'C', '#'},
      {'J', 'K', 'L', 'G', 'H', 'I'},
} ;
#endif

typedef struct
   {
   const uint8_t scancode[3] ;
   const char    key ;
   }  KEYMAP;

// Keys by row (see above)
static const KEYMAP KeyMapTbl[32] =
{
   {"001",  'U'},    // 0 - left  side top
   {"002",  'V'},    // 1 -
   {"004",  'W'},    // 2 -
   {"008",  'X'},    // 3 - left  side bottom
   {"010",  'Y'},    // 4 - arrow left
   {"020",  'Z'},    // 5 - arrow right

   {"101",  '1'},    // 6
   {"102",  '2'},    // 7
   {"104",  '3'},    // 8
   {"108",  '8'},    // 9
   {"110",  '7'},    // 10
   {"120",  '6'},    // 11

   {"301",  'A'},    // 12
   {"202",  '5'},    // 13
   {"204",  '4'},    // 14
   {"208",  '9'},    // 15
   {"210",  '0'},    // 16
   {"502",  'F'},    // 17       /* blue key          */

   {"302",  'B'},    // 18
   {"201",  '*'},    // 19
   {"501",  'E'},    // 20       /* green key         */
   {"320",  'D'},    // 21
   {"308",  'C'},    // 22
   {"310",  '#'},    // 23

   {"401",  'G'},    // 24       /* right side top    */
   {"402",  'H'},    // 25       /*                   */
   {"404",  'I'},    // 26       /*                   */
   {"408",  'J'},    // 27       /* right side bottom */
   {"420",  'L'},    // 28       /* arrow down        */
   {"410",  'K'},    // 29       /* arrow up          */

   {"504",  'M'},    // 30       /* key E gone        */
   {"508",  'N'}     // 31       /* key F gone        */
} ;


static OS_THREAD_STACK KeysStack[KEYS_STACK_SIZE] ;


// Executes when change in GREEN key is detected
// Key is pressed, when value on the pin is 0
static void Dump(uint32_t Sensor_Flags)
   {
   if (Sensor_Flags & E_Pin)
      {
      // M - key
      Clr_Sflag(S_DUMP) ;
      if (OCU_Debug) DbgPrintf("Dump key OFF\r\n") ;
      }
   else
      {
      // E - key
      TrimDiagnostic(STOP_DIAGNOSTICS, 0, 0) ;
      ReloadTmr(TRIM_DIAG_TMR, 1000) ;
      LCDClear() ;
      Set_Sflag(S_DUMP) ;
      if (OCU_Debug) DbgPrintf("Dump key ON\r\n") ;
      Beep() ;
      } ;
   }


// Executes when change in BLUE key is detected
// Key is pressed, when value on the pin is 0
static void Blue(uint32_t Sensor_Flags)
   {
   if (Sensor_Flags & C_Pin)
      {
      // N - key
      Clr_Sflag(S_BLUE) ;
      if (OCU_Debug) DbgPrintf("Blue key OFF\r\n") ;
      }
   else
      {
      // F - key
      Set_Sflag(S_BLUE) ;
      if (OCU_Debug) DbgPrintf("Blue key ON\r\n") ;
      Beep() ;
      } ;
   }


// BLUE and GREEN keys processing
static INLINE uint32_t CheckBlueGreen(uint32_t Sensor_Flags)
   {
   register DEBOUNCE_TMR *pTmr = &Tmr[0] ;
   register uint32_t  i = MAX_SENSOR ;
   register uint32_t  sample = PinRead(GPIOE, C_Pin | E_Pin) ;
   register uint32_t  change = sample ^ Sensor_Flags ;

   do
      {
      // Changes require debouncing time
      if (pTmr->f & change)
         {
         // Pin change detected
         if (pTmr->t == 0)
            pTmr->t = pTmr->d ;
         else
            {
            if (--pTmr->t == 0)
               {
               Sensor_Flags &= ~pTmr->f ;
               Sensor_Flags |= pTmr->f & sample ;
               (*pTmr->fptr)(Sensor_Flags) ;
               } ;
            } ;
         }
      else
         // No changes: Reset debouncing timer
         pTmr->t = 0 ;
      pTmr++ ;
      }
   while (--i) ;
   return Sensor_Flags ;
   }



static INLINE char DecodeKey(uint32_t row, uint32_t key_data)
   {
   register char key = KeyMapTbl[row*6 + (31 -__CLZ(key_data))].key ;
   Beep() ;
   if (OCU_Debug) DbgPrintf("Key=%c\r\n", key) ;
   return key ;
   }



// Keyboard actions
static INLINE void Process_Key(char key)
   {
   // Functions with BLUE key
   if (Tst_Sflag(S_BLUE))
      {
      // Inverse screen
      if (key == 'Z')
         {
         if (++OCUParameters.ColorSheme >= NUMBER_OF_SCHEMES) OCUParameters.ColorSheme = 0 ;
         LoadPalette(OCUParameters.ColorSheme, 0) ;
         StartTmr(SAVE_PARAMS_TIMER) ;
         return ;
         } ;

      // Increase brightness
      if (key == 'Y')
         {
         if (++BackLiteLevel > MAX_DURATION) BackLiteLevel = 0 ;
       BRIGHTNESS:
         if (BackLiteLevel)
            clr_status(LAMP_OFF) ;
         else
            set_status(LAMP_OFF) ;
         SetBackLite(BackLiteLevel) ;
         StartTmr(SAVE_PARAMS_TIMER) ;
         return ;
         } ;

      // Decrease brghtness
      if (key == 'X')
         {
         if (--BackLiteLevel > MAX_DURATION) BackLiteLevel = MAX_DURATION ;
         goto BRIGHTNESS ;
         } ;

      // BLUE key + K: Turn back light on
      if (key == 'K')
         {
         clr_status(LAMP_OFF) ;
         BackLiteOn() ;
         return ;
         } ;

      // BLUE key + L: Turn back light off
      if (key == 'L')
         {
         set_status(LAMP_OFF) ;
         BackLiteOff() ;
         return ;
         } ;
      } ;

   TrimDiagnostic(OCU_KEY, key, 0);
   }


static void TrimWakeUp(void)
   {
   TrimDiagnostic(DIAGNOSTICS, 0, 0) ;
   }


// Selects next row to scan
// This is executed by ROW_SCAN_TMR timer every 2 ms
static void RowScanThread(void *dummy)
   {
   register uint32_t key_data, current_row, last_key ;
   register uint32_t Sensor_Flags = PinRead(GPIOE, C_Pin | E_Pin) ;
   uint32_t          key_off_debounce, key_debounce,  key_processed ;

   current_row = last_key = key_off_debounce = key_debounce = key_processed = 0 ;

   // Repeat keypad timer
   CreateTmr(REPEAT_KEY_TMR|TIMER_CREATE_FNC, 0, NULL, NULL) ;
   // TRIM wake-up timer
   CreateTmr(TRIM_DIAG_TMR|TIMER_CREATE_FNC, 0, TrimWakeUp, NULL) ;
   // Thread wake-up timer
   CreateTmr(ROW_SCAN_TMR|TIMER_CREATE_EVENT_AUTO|TIMER_CREATE_AUTORELOAD|TIMER_CREATE_START, KEY_SCAN_INTERVAL_MS, NULL, NULL) ;

   // This will wake-up every KEY_SCAN_INTERVAL_MS ms
   while (!WaitOnTimerEvent(ROW_SCAN_TMR))
      {
      // Reset current row to 0
      RowPort->BSRRH = row_pin[current_row] ;
      __ISB() ;
      __DSB() ;
      // Check GREEN and BLUE buttons
      Sensor_Flags = CheckBlueGreen(Sensor_Flags) ;
      // Read columns
      key_data = 0x3F & ~(PinRead(GPIOE, Col0 | Col1 | Col2 | Col3 | Col4 | Col5) >> 3) ;
      if (!key_data)
         {
         // Nothing pressed
         if (++key_off_debounce > OFF_DEBOUNCE_TIME)
            {
            // Move to the next row
            if (++current_row == MAX_ROW) current_row = 0 ;
            last_key = key_processed = 0 ;
            key_off_debounce = OFF_DEBOUNCE_TIME ;
            } ;
         }
      else if (last_key == key_data)
         {
         // Same key
         key_off_debounce = 0;
         if (!key_processed)
            {
            if (++key_debounce > ON_DEBOUNCE_TIME)
               {
               key_processed = 1 ;
               // Repeat keypad timer
               ReloadTmr(REPEAT_KEY_TMR, 750) ;
               // Decode and process key
             PROCESS:
               Process_Key(DecodeKey(current_row, key_data)) ;
               } ;
            }
         else
            {
            if (tmr_expired(REPEAT_KEY_TMR) && !Tst_Sflag(S_NO_REPEAT_KEY))
               {
               // Repeat keypad timer
               ReloadTmr(REPEAT_KEY_TMR, 250) ;
               // Decode and process key
               goto PROCESS ;
               } ;
            } ;
         }
      else
         {
         if (!key_processed)                      /* is last key still pending */
            {
            last_key = key_data;                  /* no, new key just read */
            key_debounce = key_off_debounce = 0;  /* restart key debounce */
            } ;
         } ;
      // Set all rows to 1
      RowPort->BSRRL = Row0 | Row1 | Row2 | Row3 | Row4 ;
      } ;
   }




int KeyBoardInit(void)
   {
   GPIO_InitTypeDef GPIO_InitStruct;

   // Set rows GPIO pins output level to 1
   RowPort->BSRRL = Row0 | Row1 | Row2 | Row3 | Row4 ;

   GPIO_InitStruct.Alternate = 0 ;
   GPIO_InitStruct.Pull = GPIO_PULLUP ;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW ;

   // Configure columns GPIO pins
   GPIO_InitStruct.Pin = C_Pin | E_Pin | Col0 | Col1 | Col2 | Col3 | Col4 | Col5 ;
   GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
   HAL_GPIO_Init(ColPort, &GPIO_InitStruct);

   // Configure rows GPIO pins
   GPIO_InitStruct.Pin = Row0 | Row1 | Row2 | Row3 | Row4 ;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
   HAL_GPIO_Init(RowPort, &GPIO_InitStruct);

   // Start keyboard processing thread
   return OS_ThreadStart(RowScanThread, KEYS_PRIO, KeysStack, KEYS_STACK_SIZE, NULL, NULL) ;
   }

