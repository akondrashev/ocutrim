#ifndef __FS_APP_H__
#define __FS_APP_H__
#include "ff.h"

int     FatFS_Start(void) ;
FRESULT fs_mount(const TCHAR *drive) ;
FRESULT fs_unmount(const TCHAR *pdrive) ;
FIL*    fs_open(const TCHAR* path, BYTE mode, FRESULT *prc) ;
FRESULT fs_close(FIL *pfile) ;
FRESULT fs_read(FIL *pfile, void *pBuf, uint32_t sz) ;
FRESULT fs_write(FIL *pfile, const void *pBuf, uint32_t sz) ;

#endif
