/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "qspi.h"
#include <time.h>

/* Definitions of physical drive number for each drive */
#define DEV_FLASH		0	/* Example: Map Ramdisk to physical drive 0 */
#define DEV_MMC		1	/* Example: Map MMC/SD card to physical drive 1 */
#define DEV_USB		2	/* Example: Map USB MSD to physical drive 2 */


/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/* Physical drive nmuber to identify the drive */
/*-----------------------------------------------------------------------*/
DSTATUS disk_status(BYTE pdrv)
   {
	switch (pdrv)
	   {
	   case DEV_FLASH :
	      return QSPI_Status() ;

	   default:
	      return STA_NOINIT ;
	   } ;
   }



/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/* Physical drive number to identify the drive                           */
/*-----------------------------------------------------------------------*/
DSTATUS disk_initialize(BYTE pdrv)
   {
	switch (pdrv)
	   {
	   case DEV_FLASH:
	      if (0 == QSPI_Init()) return 0 ;
	      return STA_NOINIT ;

	   default:
	      return STA_NOINIT ;
	   } ;
   }



DSTATUS disk_stop(BYTE pdrv)
   {
   switch (pdrv)
      {
      case DEV_FLASH:
         QSPI_Disable() ;
         return 0 ;

      default:
         return STA_NOINIT ;
      } ;
   }



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
   {
	switch (pdrv)
	   {
	   case DEV_FLASH:
	      if (0 == QSPI_Read(sector, buff, count)) return 0 ;
	      return 1 ;

	   default:
	      return RES_PARERR ;
	   } ;
   }



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
   {
	switch (pdrv)
	   {
	   case 0:
	      if (0 == QSPI_Write(sector, buff, count)) return 0 ;
	      return 1 ;

	   default:
	      return RES_PARERR ;
	   } ;
   }

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
   {
	switch (pdrv)
	   {
	   case DEV_FLASH:
	      {
	      switch (cmd)
	         {
	         case CTRL_SYNC:
	            return RES_OK ;

	         case GET_SECTOR_COUNT:
	            *(uint32_t*)buff = QSPI_NumberOfSectors() ;
	            return RES_OK ;

	         case GET_SECTOR_SIZE:
	            *(uint16_t*)buff = 4096 ;
	            return RES_OK ;

	         case GET_BLOCK_SIZE:
	            *(uint32_t*)buff = 1 ;
	            return RES_OK ;
	         } ;
	      } ;
	      return RES_PARERR ;

	   default:
	      return RES_PARERR ;
	   } ;
   }


