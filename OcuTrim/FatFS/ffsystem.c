/****************************************************
 *
 *       System-dependent functions for FatFS
 *
 ****************************************************/
#include "os.h"
#include "ff.h"
#include <time.h>
#include <string.h>
#include "diskio.h"

// Maximum number of simultaneously opened files
#define MAX_OPENED_FILES      2

int get_ldnumber(const TCHAR **path) ;

// File buffer. We allocate buffers in RAM_D2, which does not use data cache. So, we can use DMA without cache synchronization.
typedef struct
   {
   uint32_t  buf_id ;
   } BUF_HEADER ;

typedef struct
   {
   BUF_HEADER  id ;
   uint8_t     buf[FF_MAX_SS] ;
   } FILE_BUF ;


// Queue of file buffers
typedef struct
   {
   volatile uint32_t  nAvail ;          // Number of available buffers
   uint32_t pool[MAX_OPENED_FILES] ;    // Sequence of ID of available buffers
   } POOL ;

static uint8_t* BUF_Get(void) ;
static FRESULT  BUF_Release(uint8_t *pbuf) ;

// Mount flags per volume
static uint8_t IsMounted[FF_VOLUMES] ;
// File system objects.  We have to have as many such objects, as many volumes we hav in the system
static FATFS  fs[FF_VOLUMES] ;
// Buffers for file system objects above
static NO_CACHE uint8_t fs_buf[FF_VOLUMES][FF_MAX_SS] ;
// Private buffers for files
static POOL buf_pool ;
static NO_CACHE FILE_BUF file_buf[MAX_OPENED_FILES] ;

static HANDLE  FS_mutex = NULL ;


// Starts file system
int FatFS_Start(void)
   {
   register uint32_t  i ;
   int  err ;

   cs_enter() ;
   if (!FS_mutex)
      {
      memset(&fs[0], 0, sizeof(fs)) ;
      memset(&IsMounted[0], 0, sizeof(IsMounted)) ;
      // Assign buffers for volume objects
      for (i = 0; i < FF_VOLUMES; i++) fs[i].pwin = fs_buf[i] ;
      // Fill file buffers stack
      buf_pool.nAvail = MAX_OPENED_FILES ;
      for (i = 0; i < MAX_OPENED_FILES; i++)
         {
         buf_pool.pool[i] = i ;
         file_buf[i].id.buf_id = i ;
         } ;
      // Create global synchronization mutex
      FS_mutex = OS_MutexCreate(FALSE, &err) ;
      } ;
   cs_exit() ;
   return err ;
   }


// Mount logical volume
FRESULT fs_mount(const TCHAR *pdrive)
   {
   register FRESULT  rc = FR_OK ;
   register int vol = get_ldnumber(&pdrive) ;

   if (vol < 0 || vol >= FF_VOLUMES) return FR_INVALID_DRIVE ;
   if (OS_MutexClaim(FS_mutex, INFINITY)) return FR_INT_ERR ;
   if (!IsMounted[vol])
      {
      rc = f_mount(&fs[vol], pdrive, 1) ;
      if (rc == FR_NO_FILESYSTEM)
         {
         rc = f_mkfs(pdrive, FM_ANY|FM_SFD, 0, fs[vol].pwin, FF_MAX_SS) ;
         if (rc == FR_OK) rc = f_mount(&fs[vol], pdrive, 1) ;
         } ;
      if (rc == FR_OK) IsMounted[vol] = 1 ;
      } ;
   OS_MutexRelease(FS_mutex) ;
   return rc ;
   }


// Un-mount logical volume
FRESULT fs_unmount(const TCHAR *pdrive)
   {
   register FRESULT  rc = FR_OK ;
   register int vol = get_ldnumber(&pdrive) ;

   if (vol < 0 || vol >= FF_VOLUMES) return FR_INVALID_DRIVE ;
   if (OS_MutexClaim(FS_mutex, INFINITY)) return FR_INT_ERR ;
   if (IsMounted[vol])
      {
      rc = f_mount(NULL, pdrive, 0) ;
      if (rc == FR_OK) IsMounted[vol] = 0 ;
      disk_stop(vol) ;
      } ;
   OS_MutexRelease(FS_mutex) ;
   return rc ;
   }


// Open file
FIL* fs_open(const TCHAR* path, BYTE mode, FRESULT *prc)
   {
   register FIL     *pfile ;
   register FRESULT  rc ;

   // Allocate file descriptor
   pfile = malloc(sizeof(FIL)) ;
   if (pfile == NULL)
      {
      *prc = FR_NOT_ENOUGH_CORE ;
      return NULL ;
      } ;
   // Allocate file buffer
   pfile->pbuf = BUF_Get() ;
   if (pfile->pbuf == NULL)
      {
      free(pfile) ;
      *prc = FR_NOT_ENOUGH_CORE ;
      return NULL ;
      } ;
   // Execute
   rc = f_open(pfile, path, mode) ;
   if (rc != FR_OK)
      {
      BUF_Release(pfile->pbuf) ;
      free(pfile) ;
      pfile = NULL ;
      } ;
   *prc = rc ;
   return pfile ;
   }


// Close file
FRESULT fs_close(FIL *pfile)
   {
   register FRESULT rc = f_close(pfile) ;
   if (rc == FR_OK)
      {
      BUF_Release(pfile->pbuf) ;
      free(pfile) ;
      } ;
   return rc ;
   }


// Write file
FRESULT fs_write(FIL *pfile, const void *pBuf, uint32_t sz)
   {
   register FRESULT  rc ;
   uint32_t nBytes ;

   rc = f_write(pfile, pBuf, sz, &nBytes) ;
   if (rc != FR_OK) return rc ;
   if (sz != nBytes) return FR_DISK_ERR ;
   return FR_OK ;
   }


// Read file
FRESULT fs_read(FIL *pfile, void *pBuf, uint32_t sz)
   {
   register FRESULT  rc ;
   uint32_t nBytes ;

   rc = f_read(pfile, pBuf, sz, &nBytes) ;
   if (rc != FR_OK) return rc ;
   if (sz != nBytes) return FR_DISK_ERR ;
   return FR_OK ;
   }


// Reserves block of memory from the pool and returns pointer to block's memory area.
// Returns NULL, if block is not available
static uint8_t* BUF_Get(void)
   {
   register uint32_t  n ;

   __disable_irq() ;
   // Check if we have available buffers
   n = buf_pool.nAvail ;
   if (n == 0)
      {
      if (!_primask_level) __enable_irq() ;
      return NULL ;
      } ;
   // Take top buffer out of pool
   buf_pool.nAvail = n - 1 ;
   if (!_primask_level) __enable_irq() ;
   // Return pointer to buffer with corresponding ID
   return file_buf[buf_pool.pool[MAX_OPENED_FILES - n]].buf ;
   }


// Returns buffer back to the pool
static FRESULT BUF_Release(uint8_t *pbuf)
   {
   register uint32_t  n ;
   register FILE_BUF *pb = (FILE_BUF*)(pbuf - sizeof(BUF_HEADER)) ;

   // Check buffer header
   if (pb->id.buf_id >= MAX_OPENED_FILES) return FR_INT_ERR ;
   __disable_irq() ;
   // Check number of buffers already in the pool
   n = buf_pool.nAvail ;
   if (n == MAX_OPENED_FILES)
      {
      if (!_primask_level) __enable_irq() ;
      return FR_INT_ERR ;
      } ;
   // Return buffer to the pool
   buf_pool.nAvail = ++n ;
   buf_pool.pool[MAX_OPENED_FILES - n] = pb->id.buf_id ;
   if (!_primask_level) __enable_irq() ;
   return FR_OK ;
   }



#if !FF_FS_READONLY && !FF_FS_NORTC
// File time
uint32_t get_fattime (void)
   {
   register uint32_t  r ;
   struct tm tm ;
   time__t   t = OS_time(NULL) ;

   gmtime_r((time_t*)&t, &tm) ;
   r = (tm.tm_year - 80) << 25 ;
   r |= (tm.tm_mon + 1) << 21 ;
   r |= tm.tm_mday << 16 ;
   r |= tm.tm_hour << 11 ;
   r |= tm.tm_min << 5 ;
   r |= tm.tm_sec ;
   return r ;
   }
#endif


#if FF_FS_REENTRANT	/* Thread safety */
/*------------------------------------------------------------------------*/
/* Create a Synchronization Object                                        */
/*------------------------------------------------------------------------*/
/* This function is called in f_mount() function to create a new
/  synchronization object for the volume, such as semaphore and mutex.
/  When a 0 is returned, the f_mount() function fails with FR_INT_ERR.
*/

//const osMutexDef_t Mutex[FF_VOLUMES];	/* Table of CMSIS-RTOS mutex */
/* 1:Function succeeded, 0:Could not create the sync object */
/* Corresponding volume (logical drive number) */
/* Pointer to return the created sync object */

int ff_cre_syncobj(BYTE vol, FF_SYNC_t *sobj)
   {
   int  err ;

   *sobj = OS_MutexCreate(FALSE, &err) ;
   return (err == 0) ;
   }


/*------------------------------------------------------------------------*/
/* Delete a Synchronization Object                                        */
/*------------------------------------------------------------------------*/
/* This function is called in f_mount() function to delete a synchronization
/  object that created with ff_cre_syncobj() function. When a 0 is returned,
/  the f_mount() function fails with FR_INT_ERR.
*/
/* 1:Function succeeded, 0:Could not delete due to an error */
/* Sync object tied to the logical drive to be deleted */

int ff_del_syncobj(FF_SYNC_t sobj)
   {
   return (0 == OS_MutexDelete((HANDLE)sobj)) ;
   }


/*------------------------------------------------------------------------*/
/* Request Grant to Access the Volume                                     */
/*------------------------------------------------------------------------*/
/* This function is called on entering file functions to lock the volume.
/  When a 0 is returned, the file function fails with FR_TIMEOUT.
*/
/* 1:Got a grant to access the volume, 0:Could not get a grant */
/* Sync object to wait */

int ff_req_grant(FF_SYNC_t sobj)
   {
   return (0 == OS_MutexClaim((HANDLE)sobj, FF_FS_TIMEOUT)) ;
   }


/*------------------------------------------------------------------------*/
/* Release Grant to Access the Volume                                     */
/*------------------------------------------------------------------------*/
/* This function is called on leaving file functions to unlock the volume.
*/

int ff_rel_grant(FF_SYNC_t sobj)
   {
   return (0 == OS_MutexDrop((HANDLE)sobj)) ;
   }

#endif

