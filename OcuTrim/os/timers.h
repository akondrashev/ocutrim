#ifndef  _TIMERS_H
#define  _TIMERS_H

// Timer creation flags (for creation function argument)
#define  TIMER_CREATE_FNC           (0x00 << 16)
#define  TIMER_CREATE_SEMAPHORE     (0x01 << 16)
#define  TIMER_CREATE_EVENT_AUTO    (0x02 << 16)
#define  TIMER_CREATE_EVENT_MANUAL  (0x04 << 16)
#define  TIMER_CREATE_AUTORELOAD    (0x08 << 16)
#define  TIMER_CREATE_START         (0x10 << 16)
#define  TIMER_CREATE_ABS_TIME      (0x20 << 16)
#define  TIMER_CREATE_FNC1          (0x40 << 16)

// Timer status flags
#define  TIMER_RUNNING              0x1
#define  TIMER_WAITING              0x2
#define  TIMER_EXPIRED              0x4
#define  TIMER_STOPPED              0x8

// Timer property flags
#define  TIMER_SEMAPHORE            0x010000
#define  TIMER_EVENT                0x020000
#define  TIMER_AUTORELOAD           0x040000
#define  TIMER_ABS_TIME             0x080000
#define  TIMER_FNC_1                0x100000

typedef void (*FPTR0)(void) ;
typedef void (*FPTR1)(void*) ;


typedef struct
   {
   volatile uint32_t  f ;        // Flags
   volatile uint32_t  t ;        // Current ticks count
   volatile uint32_t  reload ;   // Original ticks value
   volatile void     *context ;  // Optional argument for FPTR1 function
   union
      {
      volatile FPTR0  fptr0 ;    // function pointer (no argument)
      volatile FPTR1  fptr1 ;    // function pointer (1 argument)
      HANDLE          SyncObj ;  // Semaphore or Event
      } ;
   } stAppTimer_t;


typedef enum
   {
   ROW_SCAN_TMR = 0,
   REPEAT_KEY_TMR,
   SCREEN_SAVER_TMR,
   SAVE_PARAMS_TIMER,
   TRIM_DIAG_TMR,
   eTmr_Max
   }  AppTimer ;


extern stAppTimer_t Timers[eTmr_Max] ;
extern volatile uint32_t TimeMS ;

/* Macros */
#define  tmr_expired(i)             (Timers[i].f & TIMER_EXPIRED)
#define  tmr_active(i)              (Timers[i].f & (TIMER_RUNNING | TIMER_WAITING | TIMER_EXPIRED))
#define  WaitOnTimerSemaphore(n)    OS_SemPend(Timers[n].pSem, INFINITY)
#define  WaitOnTimerEvent(n)        OS_EventWait(Timers[n].SyncObj, INFINITY)
#define  Tmr_GetRemTime(n)          (Timers[ntmr].t)


/* Prototypes */
int   CreateTmr(uint32_t CreationFlags, uint32_t ntime, void *fptr, void *context) ;
void  ReloadTmr(AppTimer ntemr, uint32_t ntime) ;
int   StartTmr(AppTimer ntmr) ;
void  StopTmr(AppTimer ntmr) ;


#endif

