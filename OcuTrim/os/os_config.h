#ifndef __COMMON_H__
#define __COMMON_H__

// Uncomment to include testing features
//#define _TEST_     1

// Threads priorities and stack sizes.  All sizes in WORDs
#define  MAIN_PRIO         0
// Main thread stack is 4096 bytes (1024 WORDs)

#define  LCD_PRIO          1
#define  LCD_STACK_SIZE    1024

#define  KEYS_PRIO         3
#define  KEYS_STACK_SIZE   1024

#define  HDLC_PRIO         4
#define  HDLC_STACK_SIZE   1024

#define  BACKGROUND_PRIO   30
#define  BGRD_STACK_SIZE   1024

#define  TIMERS_PRIO       31
#define  TIMERS_STACK_SIZE 1024

// Priority of IDLE thread is 32
#define  IDLE_STACK_SIZE   128


// Interrupts priorities
typedef enum
   {
   DMA2D_IRQ_PRIO = 0,              // Graphics DMA
   LTDC_IRQ_PRIO = 0,               // LCD blanking
   TIM6_IRQ_PRIO = 1,               // TIM6 (button click)
   MDMA_IRQ_PRIO = 3,               // MDMA (QUAD SPI and CRC controllers)
   USART2_IRQ_PRIO = 4,             // HDLC UART
   QUADSPI_IRQ_PRIO = 6,            // QUAD SPI (flash)
   USART1_IRQ_PRIO = 7,             // Debugging UART
   SysTick_IRQ_PRIO = 14,           // SysTick
   PendSV_IRQ_PRIO = 15             // Context switch
   } IRQ_PRIO ;

// OS objects configuration
#define  OS_MAX_EVENTS              64


#endif
