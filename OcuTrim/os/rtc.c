/************************************************************************
 *
 *                         RTC Support
 *
 *   See clock configuration in SystemClock_Config() in main.c
 *
 ************************************************************************/
#include <os.h>

#define RTC_CALR_MINUS        175
#define RTC_CALIBRATION       (RTC_CALR_CALP | RTC_CALR_MINUS)

static HANDLE  RTC_Mutex = NULL ;

// One time initialization of RTC controller.
// No concurrency protection.  Should be executed before application threads start
uint32_t RTC_Init(uint32_t RTC_CLOCK_SOURCE)
   {
   int  err ;
   register uint32_t  temp ;

   // Exit, if already initialized
   if (RTC_Mutex) return 0 ;

   // Create access mutex
   RTC_Mutex = OS_MutexCreate(FALSE, &err) ;
   if (err) return err ;

   // Enable write access to Backup domain
   PWR->CR1 |= PWR_CR1_DBP ;
   while ((PWR->CR1 & PWR_CR1_DBP) == RESET) __DSB() ;

   // Configure clock source, if different
   temp = RCC->BDCR ;
   if ((temp & RCC_BDCR_RTCSEL) !=  RCC_RTCCLKSOURCE_LSE || (temp & RCC_BDCR_LSECSSON))
      {
      // Reset backup domain
      __HAL_RCC_BACKUPRESET_FORCE() ;
      __DSB() ;
      __HAL_RCC_BACKUPRESET_RELEASE() ;
      __DSB() ;
      // Enable LSE
      RCC->BDCR = RCC_BDCR_LSEON ;
      // Wait it is ready
      while ((RCC->BDCR & RCC_FLAG_LSERDY) == 0) __DSB() ;
      // Configure
      RCC->BDCR |= RCC_RTCCLKSOURCE_LSE ;
      } ;

   // Enable RTC Clock
   __HAL_RCC_RTC_ENABLE() ;

   // Already configured?
   if (RTC->CALR != RTC_CALIBRATION)
      {
      // Disable the write protection for RTC registers
      RTC->WPR = 0xCA ;
      RTC->WPR = 0x53 ;
      __ISB() ;
      __DSB() ;

      // Enter initialization mode
      RTC->ISR = RTC_ISR_INIT ;
      // Wait till RTC is in INIT state
      while ((RTC->ISR & RTC_ISR_INITF) == 0) __DSB() ;

      // Set RTC_CR register in by-pass mode
      RTC->CR = RTC_CR_BYPSHAD ;
      // Configure the RTC PRER
      if (RTC_CLOCK_SOURCE == RCC_RTCCLKSOURCE_LSE)
         RTC->PRER = 255 | (127 << 16) ;
      else
         RTC->PRER = 0x0130 | (0x7F << 16) ;

      // Clock adjustment 174: +2 sec (4 days); 175: -1 sec (2 days)
      RTC->CALR = RTC_CALIBRATION ;

      // Exit Initialization mode
      RTC->ISR &= ((uint32_t)~RTC_ISR_INIT) ;
      // Enable the write protection for RTC registers
      RTC->WPR = 0xFF ;
      DbgPrintf("New RTC->CALR=0x%X\r\n", RTC->CALR) ;
      } ;

   return 0 ;
   }


static INLINE uint32_t ByteToBcd(uint8_t Value)
   {
   register uint32_t high = Value / 10 ;
   return  ((high << 4) | (Value - (high * 10))) ;
   }


static INLINE uint32_t BcdToByte(uint32_t Value)
   {
   return (10*(Value >> 4)) + (Value & 0x0F) ;
   }



// Set RTC time given by tm structure
uint32_t RTC_Set(struct tm *ptm)
   {
   register uint32_t date_reg, time_reg ;
   register uint32_t wday = ptm->tm_wday ;

   // Prepare date register value
   if (wday == 0) wday = 7 ;
   date_reg = (ByteToBcd(ptm->tm_year - 100) << 16) | (wday << 13) | (ByteToBcd(ptm->tm_mon + 1) << 8) | ByteToBcd(ptm->tm_mday) ;
   // Prepare time register value
   time_reg = (ByteToBcd(ptm->tm_hour) << 16) | (ByteToBcd(ptm->tm_min) << 8) | ByteToBcd(ptm->tm_sec) ;

   // Take control over RTC controller
   if (OS_MutexClaim(RTC_Mutex, INFINITY)) return 1 ;
   // Disable the write protection for RTC registers
   RTC->WPR = 0xCA ;
   RTC->WPR = 0x53 ;
   __DSB() ;
   // Enter initialization mode
   RTC->ISR = RTC_ISR_INIT ;
   // Wait till RTC is in INIT state
   while ((RTC->ISR & RTC_ISR_INITF) == 0) ;
   // Set the RTC_DR register
   RTC->DR = date_reg ;
   // Set RTC TR register
   RTC->TR = time_reg ;
   // Exit Initialization mode
   RTC->ISR &= ((uint32_t)~RTC_ISR_INIT) ;
   // Enable the write protection for RTC registers
   RTC->WPR = 0xFF ;
   // Release CRC controller
   OS_MutexRelease(RTC_Mutex) ;
   return 0 ;
   }


// Get RTC time into tm structure
uint32_t RTC_Get(struct tm *ptm)
   {
   register uint32_t  tm_reg1, tm_reg2, dt_reg1, dt_reg2 ;

   // Take control over RTC controller
   if (OS_MutexClaim(RTC_Mutex, INFINITY)) return 1 ;
//   while ((RTC->ISR & RTC_ISR_RSF) == 0) ;
   do
      {
      tm_reg1 = RTC->TR ;
      dt_reg1 = RTC->DR ;
      tm_reg2 = RTC->TR ;
      dt_reg2 = RTC->DR ;
      }
   while (tm_reg1 != tm_reg2 || dt_reg1 != dt_reg2) ;
   // Time
   tm_reg1 = tm_reg1 & RTC_TR_RESERVED_MASK ;
   ptm->tm_hour = BcdToByte((tm_reg1 & (RTC_TR_HT | RTC_TR_HU)) >> 16) ;
   ptm->tm_min = BcdToByte((tm_reg1 & (RTC_TR_MNT | RTC_TR_MNU)) >> 8) ;
   ptm->tm_sec = BcdToByte(tm_reg1 & (RTC_TR_ST | RTC_TR_SU)) ;
   // Date
   dt_reg1 = dt_reg1 & RTC_DR_RESERVED_MASK ;
   ptm->tm_year = BcdToByte((dt_reg1 & (RTC_DR_YT | RTC_DR_YU)) >> 16) + 100 ;
   ptm->tm_mon = BcdToByte((dt_reg1 & (RTC_DR_MT | RTC_DR_MU)) >> 8) - 1 ;
   ptm->tm_mday = BcdToByte(dt_reg1 & (RTC_DR_DT | RTC_DR_DU)) ;
   ptm->tm_wday = BcdToByte((dt_reg1 & (RTC_DR_WDU)) >> 13) ;
   if (ptm->tm_wday == 7) ptm->tm_wday = 0 ;
   ptm->tm_yday = 0 ;
   ptm->tm_isdst = 0 ;
//   RTC->ISR &= (uint32_t)RTC_RSF_MASK ;
   // Release CRC controller
   OS_MutexRelease(RTC_Mutex) ;
   return 0 ;
   }

