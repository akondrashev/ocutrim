#include "os.h"

// Array of application timers (static allocation)
stAppTimer_t Timers[eTmr_Max] ;


/******************************************************************************************************
 *
 *                         Creates application timer with ID "ntmr"
 *
 *  "CreatinFlags" is a combination of the following flags.
 *  These flags are mutually exclusive
 *     TIMER_CREATE_FNC          - timer has a callback function defined by "fptr" without argument
 *     TIMER_CREATE_FNC1         - timer has a callback function defined by "fptr" with argument
 *     TIMER_CREATE_SEMAPHORE    - timer increments semaphore
 *     TIMER_CREATE_EVENT_AUTO   - timer triggers automatic event
 *     TIMER_CREATE_EVENT_MANUAL - timer triggers manual event
 *  These flags can be used together:
 *     TIMER_CREATE_AUTORELOAD   - creates auto-reloading timer
 *     TIMER_CREATE_START        - timer will be started after it is created
 *     TIMER_CREATE_ABS_TIME     - timer counter expressed in seconds of RTC
 *
 *  "ntime" - timer period in ms.  If TIMER_CREATE_ABS_TIME flag is specified, "ntime" is absolute
 *             timestamp in seconds.  Timer triggers, when current time exceeds the specified time.
 *
 *  "fptr" - call-back function that executed when timer expires.  This parameter is ignored for
 *           semaphore and event timers.  This can be NULL, if no function should be executed.
 *
 *   "context" - argument of call-back function, if TIMER_CREATE_FNC1 creation flag is specified
 *
 *******************************************************************************************************/
int CreateTmr(uint32_t CreationFlags, uint32_t ntime, void *fptr, void *context)
   {
   register AppTimer ntmr = CreationFlags & 0xFFFF ;
   register stAppTimer_t *pTmr = &Timers[ntmr] ;
   register uint32_t  flags = 0 ;
   int  err ;

   cs_enter() ;
   // If semaphore or event were previously created, delete it
   if (pTmr->SyncObj)
      {
      if (pTmr->f & TIMER_SEMAPHORE)
         OS_SemDelete(pTmr->SyncObj) ;
      else if (pTmr->f & TIMER_EVENT)
         OS_EventDelete(pTmr->SyncObj) ;
      pTmr->SyncObj = NULL ;
      } ;
   // Callback type
   if (CreationFlags & TIMER_CREATE_SEMAPHORE)
      {
      // Semaphore
      pTmr->SyncObj = OS_SemCreate(0, &err) ;
      if (err) goto EXIT ;
      flags = TIMER_SEMAPHORE ;
      }
   else if (CreationFlags & TIMER_CREATE_EVENT_AUTO)
      {
      // Auto-reset event
      pTmr->SyncObj = OS_EventCreate(0, 0, &err) ;
      if (err) goto EXIT ;
      flags = TIMER_EVENT ;
      }
   else if (CreationFlags & TIMER_CREATE_EVENT_MANUAL)
      {
      // Manual reset event
      pTmr->SyncObj = OS_EventCreate(0, 1, &err) ;
      if (err) goto EXIT ;
      flags = TIMER_EVENT ;
      }
   else
      {
      // Function
      pTmr->fptr0 = fptr ;
      pTmr->context = context ;
      if (CreationFlags & TIMER_CREATE_FNC1) flags = TIMER_FNC_1 ;
      } ;
   // Auto-reload
   if (CreationFlags & TIMER_CREATE_AUTORELOAD) flags |= TIMER_AUTORELOAD ;
   // Absolute time
   if (CreationFlags & TIMER_CREATE_ABS_TIME) flags |= TIMER_ABS_TIME ;
   // Counter/time
   pTmr->reload = ntime ;
   // Activation type
   if ((CreationFlags & TIMER_CREATE_START) && ntime)
      {
      pTmr->t = ntime ;
      flags |= TIMER_RUNNING ;
      }
   else
      {
      pTmr->t = 0 ;
      flags |= TIMER_STOPPED ;
      } ;

  EXIT:
   pTmr->f = flags ;
   cs_exit() ;
   return err ;
   }


/******************************************************************************
 *
 *  If "ntime" is 0, stops and disables timer "ntmr".
 *  Otherwise, sets new interval value and starts the timer.
 *
 ******************************************************************************/
void ReloadTmr(AppTimer ntmr, uint32_t ntime)
   {
   register uint32_t      f ;
   register stAppTimer_t *pTmr = &Timers[ntmr] ;

   __disable_irq() ;
   f = pTmr->f & ~0xFF ;
   if (ntime)
      {
      pTmr->t = pTmr->reload = ntime ;
      pTmr->f = f | TIMER_RUNNING ;
      }
   else
      {
      pTmr->t = pTmr->reload = 0 ;
      pTmr->f = f | TIMER_STOPPED ;
      } ;
   if (!_primask_level) __enable_irq() ;
   }


/******************************************************************************
 *
 *  Starts (or re-starts) previously created timer.
 *
 ******************************************************************************/
int StartTmr(AppTimer ntmr)
   {
   register stAppTimer_t *pTmr = &Timers[ntmr] ;
   register uint32_t  reload ;

   __disable_irq() ;
   reload = pTmr->reload ;
   if (reload)
      {
      pTmr->t = reload ;
      pTmr->f = (pTmr->f & ~0xFF) | TIMER_RUNNING ;
      if (!_primask_level) __enable_irq() ;
      return 0 ;
      } ;
   if (!_primask_level) __enable_irq() ;
   DbgPrintf("Timer %n cannot be re-started\r\n", ntmr) ;
   return 1 ;
   }


/******************************************************************************
 *
 *  Stops execution of running timer.
 *  All timer parameters remain unchanged, so the timer can be restarted later.
 *
 ******************************************************************************/
void StopTmr(AppTimer ntmr)
   {
   register stAppTimer_t *pTmr = &Timers[ntmr] ;

   __disable_irq() ;
   pTmr->f = (pTmr->f & ~0xFF) | TIMER_STOPPED ;
   if (!_primask_level) __enable_irq() ;
   }
