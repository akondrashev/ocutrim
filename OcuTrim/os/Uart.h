#ifndef __UART_H__
#define __UART_H__

#include "stm32h7xx_hal.h"


typedef enum
   {
   STATE_IDLE = 0,      // Transmitter is idle
   STATE_BUSY,          // Transmitter is transmitting
   STATE_COMPLETE       // Transmission complete, but last byte still transmitting
   } UART_STATE ;

#define UART_FIFO_SZIE                 16

typedef struct
   {
   USART_TypeDef    *hUart ;              // Pointer to UART registers
   HANDLE            RXEvent ;            // RX has data event
   HANDLE            TXEvent ;            // TX is not full event
   HANDLE            IdleEvent ;          // UART is idle event
   uint16_t  UART_RX_BUF_SIZE ;           // Size of RX buffer
   uint16_t  UART_TX_BUF_SIZE ;           // Size of TX buffer
   short             IRQn ;
   volatile uint16_t Rx_Len ;             // Number of bytes available in application RX buffer
   volatile uint16_t Rx_Write_Idx ;       // Write index in application RX buffer
   volatile uint16_t RX_Read_Ind ;        // Read index from application RX buffer
   volatile uint16_t Tx_Len ;             // Number of bytes in application TX buffer
   volatile uint16_t Tx_Write_Idx ;       // Write index in application TX buffer
   volatile uint16_t TX_Read_Ind ;        // Read index from application TX buffer
   volatile uint16_t State ;              // Transmitter state
   uint8_t  *Rx_Buff ;                    // Application RX buffer
   uint8_t  *Tx_Buff ;                    // Application TX buffer
   } UART_typ ;


extern UART_typ Uart1 ;
extern UART_typ Uart2 ;

#define UART_TC_INT                    (1 << 6)
#define UART_IDLE_INT                  (1 << 4)
#define UART_RX_FIFO_INT               (1 << 26)
#define UART_RX_NON_EMPTY              (1 << 5)
#define UART_TX_FIFO_EMPTY             (1 << 23)


#define UART_FIFO_ENABLE               (1 << 29)
#define UART_TX_FIFO_EMPTY_IE          (1 << 30)
#define UART_RX_FIFO_THRES_IE          (1 << 28)

#define UART_CR1_SETUP                 (UART_WORDLENGTH_8B | UART_PARITY_NONE | UART_MODE_TX_RX | UART_OVERSAMPLING_16 | UART_IDLE_INT | UART_FIFO_ENABLE)
#define UART_CR1_ENABLE_NO_TX          (UART_CR1_SETUP | USART_CR1_UE)
#define UART_CR1_ENABLE_TX_COMPLETE    (UART_CR1_SETUP | USART_CR1_UE | USART_CR1_TCIE)
#define UART_CR1_ENABLE_TX             (UART_CR1_SETUP | USART_CR1_UE | UART_TX_FIFO_EMPTY_IE | USART_CR1_TCIE)


int      SetUARTConfig(UART_typ *uart, uint32_t BaudRate) ;
uint32_t Uart_Get(uint8_t *pBuf, uint32_t size, uint32_t timeout, UART_typ *uart) ;
void     Uart_Put(uint8_t *pBuf, uint32_t size, uint32_t timeout, UART_typ *uart) ;


int      UART1_Init(uint32_t BaudRate) ;
#define  Uart1_Get(pBuf, size, timeout)   Uart_Get(pBuf, size, timeout, &Uart1)
#define  Uart1_Put(pBuf, size)            Uart_Put(pBuf, size, INFINITY, &Uart1)
#define  Uart1_WaitIdle()                 OS_EventWait(Uart1.IdleEvent, INFINITY)


int      UART2_Init(uint32_t BaudRate) ;
#define  Uart2_Get(pBuf, size, timeout)   Uart_Get(pBuf, size, timeout, &Uart2)
#define  Uart2_Put(pBuf, size)            Uart_Put(pBuf, size, INFINITY, &Uart2)
#define  Uart2_WaitIdle()                 OS_EventWait(Uart2.IdleEvent, INFINITY)


#endif
