#include "os.h"
#include "fs_app.h"


typedef struct
   {
   stAppTimer_t  *tmr_queue[eTmr_Max] ;
   volatile uint16_t  write_idx ;
   volatile uint16_t  read_idx ;
   } TMR_QUEUE ;

static TMR_QUEUE waiting_timers ;

extern void IdleThread(void *context) ;

OS_RUN   OS_Run ;
volatile uint32_t  TimeMS = 0 ;                 // Ticks counter
volatile uint32_t  _T_ ;                        // Current time in seconds

volatile uint32_t _primask_level = 0 ;          // Interrupt disable nesting level

static HANDLE TmrSem = NULL ;                   // Semaphore that indicates number of timers in waiting timers queue
static HANDLE MemMutex = NULL ;                 // Memory allocation mutex

static void TimersThread(void *dummy) ;

static OS_THREAD_STACK IdleStack[IDLE_STACK_SIZE] ;

static OS_THREAD_STACK TimersStack[TIMERS_STACK_SIZE] ;

// CPU activity LED
#define SYS_LED_Pin        GPIO_PIN_13

static uint16_t  seconds ;
static uint8_t   LED_Blink = 0 ;


// System LED configuration (1 Hz blink)
static void InitSysLED(void)
   {
   GPIO_InitTypeDef GPIO_InitStruct ;

   GPIO_InitStruct.Alternate = 0 ;
   GPIO_InitStruct.Pin = SYS_LED_Pin ;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP ;
   GPIO_InitStruct.Pull = GPIO_NOPULL ;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW ;
   HAL_GPIO_Init(GPIOC, &GPIO_InitStruct) ;
   }


/**********************************************************************************
 *
 *                                 Starts OS
 *    Function that calls this function becomes thread with specified priority.
 *    Linker allocated stack is used by this thread.
 *
 **********************************************************************************/
int OS_Start(uint16_t prio)
   {
   int  err ;
   struct tm tm ;
   register uint32_t  t ;

   // Reset system tables
   memset(Timers, 0, sizeof(Timers)) ;
   waiting_timers.read_idx = waiting_timers.write_idx = 0 ;
   memset(&OS_Run, 0, sizeof(OS_Run)) ;
   // Memory allocation/de-allocation mutex
   MemMutex = OS_MutexCreate(0, &err) ;
   if (err) return err ;
   // Set Interrupt Group Priority: no grouping; so, there will be 16 levels of interrupts (0-15)
   NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4) ;
   // SysTick priority interrupt - lowest, except context switch
   HAL_NVIC_SetPriority(SysTick_IRQn, SysTick_IRQ_PRIO, 0) ;
   // Context switch interrupt - lowest priority
   HAL_NVIC_SetPriority(PendSV_IRQn, PendSV_IRQ_PRIO, 0) ;
   // PRIO must be 0-31
   if (prio > OS_MAX_THREADS-2) prio = OS_MAX_THREADS-2 ;
   // Convert calling program into thread
   OS_Run.CurrentThread = prio ;
   OS_Run.StartedThreads = OS_Run.ActiveThreads = 0x80000000 >> prio ;
   // Start system IDLE thread (priority 32)
   OS_ThreadStart(IdleThread, OS_MAX_THREADS-1, IdleStack, IDLE_STACK_SIZE, NULL, NULL) ;
   // System LED pins configuration
   InitSysLED() ;
   // Configure SysTick to 1 ms interrupts and start it
   SysTick_Config(SystemCoreClock / 1000) ;
   // Start debug terminal
   InitDebug() ;
   // RTC initialization
   RTC_Init(RCC_RTCCLKSOURCE_LSE) ;
   // Set current system time out of RTC
   while (RTC->SSR) ;
   RTC_Get(&tm) ;
   t = 1 + mktime(&tm) ;
   __disable_irq() ;
   _T_ = t ;
   seconds = 1000 ;
   __enable_irq() ;
   // Create semaphore to be used by TIMERS thread
   TmrSem = OS_SemCreate(0, &err) ;
   if (err) return err ;
   // Start timers
   err = OS_ThreadStart(TimersThread, TIMERS_PRIO, TimersStack, TIMERS_STACK_SIZE, NULL, NULL) ;
   if (err) return err ;
   // Start file system
   return FatFS_Start() ;
   }


/********************************************************************
 *
 *       This function is executed by OS, when thread terminates
 *         (possibly, after the custom termination function)
 *
 ********************************************************************/
static void OS_ThreadExit(void)
   {
   register uint16_t  ThreadNum ;
   register uint32_t  ThreadMask, n ;
   register OS_SYNC  *pSem ;

   cs_enter() ;
   ThreadNum = OS_Run.CurrentThread ;
   ThreadMask = ~((uint32_t)0x80000000 >> ThreadNum) ;
   // Remove from list of started threads
   OS_Run.StartedThreads &= ThreadMask ;
   // Remove from list of active threads
   OS_Run.ActiveThreads &= ThreadMask ;
   // Remove from list of waiting threads
   OS_Run.TimeWaitThreads &= ThreadMask ;
   // Trigger context switch (will active, when we allow interrupts)
   TriggerPendSV() ;
   // Remove this thread from waiting queues of all active objects
   n = OS_MAX_EVENTS ;
   pSem = OS_Run.OS_Events ;
   do
      {
      // Get object's signature
      register uint32_t  type = pSem->Sem.active ;
      // If object is active, remove current thread from it's queue
      if (type == OS_SEM_ACTIVE || type == OS_MUTEX_ACTIVE)
         pSem->Sem.queue &= ThreadMask ;
      else if (type == OS_MUTEX_ACTIVE)
         {
         // Additional processing for mutex (ignore error, if we do not own mutex)
         pSem->Mtx.queue &= ThreadMask ;
         if (pSem->Mtx.owner == ThreadNum) OS_MutexDrop(pSem) ;
         } ;
      pSem++ ;
      }
   while (--n) ;
   // Allow interrupts
   _primask_level = 0 ;
   __enable_irq() ;
   // Control should be taken from us and we will never return here
   while (1) __NOP() ;
   }


// Set system time (including RTC)
void OS_TimeSet(struct tm *ptm)
   {
   register uint32_t  t = mktime(ptm) ;
   RTC_Set(ptm) ;
   __disable_irq() ;
   _T_ = t ;
   seconds = 1000 ;
   if (!_primask_level) __enable_irq() ;
   }


/**************************************************************************************
 *
 *
 *                               Creates new thread
 *
 *
 **************************************************************************************/
int OS_ThreadStart(THREAD_FNC pfnc, uint16_t prio, OS_STK *sp_val, size_t size, void *context, THREAD_FNC exit_fnc)
   {
   register uint32_t  pri_mask, old_mask ;

   if (sp_val != IdleStack)
      {
      if (prio > OS_MAX_THREADS-2) return OS_ERR_INVALID_PRIO ;
      }
   else
      prio = OS_MAX_THREADS-1 ;
   pri_mask = 0x80000000 >> prio ;
   __disable_irq() ;
   old_mask = OS_Run.StartedThreads ;
   // Check, if thread with this priority was already created
   if (pri_mask & old_mask)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_ALREADY_STARTED ;
      } ;
   // Add to list of started threads
   OS_Run.StartedThreads = old_mask | pri_mask ;
   // Started thread will be active
   OS_Run.ActiveThreads |= pri_mask ;
   // Make sure SP is 8-byte aligned
   sp_val = (uint32_t*)((uint32_t)(sp_val + size) & ~(uint32_t)0x7) ;
   // Create stack frame for the new thread
   *--sp_val = (OS_STK)0x1000000 ;        // PSR
   *--sp_val = (OS_STK)pfnc ;             // PC - thread function address
   *--sp_val = (OS_STK)&OS_ThreadExit ;   // LR - thread exit function address
   sp_val -= 4 ;                          // R1, R2, R3, R12
   *--sp_val = (OS_STK)context ;          // R0 - thread function argument
   *--sp_val = (OS_STK)0xFFFFFFF9 ;       // LR - Return to thread mode no FP
   OS_Run.sp_val[prio] = sp_val - 8 ;     // R4-R11
   OS_Run.arg[prio] = context ;           // Thread's argument
   OS_Run.stop_fnc[prio] = exit_fnc ;     // Exit function
   // If created thread has a higher priority, switch to it
   if (prio < OS_Run.CurrentThread) TriggerPendSV() ;
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }


/**************************************************************************************
 *
 *
 *      Terminates thread of given priority (cannot terminate itself)
 *
 *
 **************************************************************************************/
int OS_ThreadStop(uint16_t prio)
   {
   register uint32_t    pri_mask ;
   register THREAD_FNC  exit_fnc ;
   register volatile uint32_t  *sp ;

   // Check that parameter is valid
   if (prio > OS_MAX_THREADS-2) return OS_ERR_INVALID_PRIO ;
   pri_mask = 0x80000000 >> prio ;
   __disable_irq() ;
   // Cannot be executed from the same thread
   if (prio == OS_Run.CurrentThread)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_PRIO ;
      } ;
   // Verify that thread is running
   if (0 == (pri_mask & OS_Run.StartedThreads))
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_ALREADY_TERMINATED ;
      } ;
   sp = OS_Run.sp_val[prio] ;
   sp[9]  = (OS_STK)(OS_Run.arg[prio]) ;
   exit_fnc = OS_Run.stop_fnc[prio] ;
   if (exit_fnc)
      {
      sp[14] = (OS_STK)&OS_ThreadExit ;      // LR
      sp[15] = (OS_STK)exit_fnc ;            // PC
      }
   else
      sp[15] = (OS_STK)&OS_ThreadExit ;      // PC
   // Make it active
   OS_Run.ActiveThreads |= pri_mask ;
   // Remove from list of waiting threads
   OS_Run.TimeWaitThreads &= ~pri_mask ;
   OS_Run.ticks[prio] = 0 ;
   if (prio < OS_Run.CurrentThread) TriggerPendSV() ;
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }


/**************************************************************************************
 *
 *
 *                  Suspends thread for a given number of ticks
 *
 *
 **************************************************************************************/
int OS_Sleep(uint32_t ticks)
   {
   register uint32_t  prio, old_pri_level ;

   if (!ticks) return 0 ;
   __disable_irq() ;
   // Save previous interrupts disable level
   old_pri_level = _primask_level ;
   if (old_pri_level) _primask_level = 0 ;
   prio = OS_Run.CurrentThread ;
   // Set wait timer
   OS_Run.ticks[prio] = ticks ;
   // Trigger context switch
   TriggerPendSV() ;
   // Mask for this thread
   prio = 0x80000000 >> prio ;
   // Add thread to the list of waiting threads
   OS_Run.TimeWaitThreads |= prio ;
   // Remove thread from the list of active threads
   OS_Run.ActiveThreads ^= prio ;
   // Enable interrupts and let other threads to run
   __enable_irq() ;
   // Return here, when wait time ended
   __NOP() ;
   // Restore interrupt disable level
   __disable_irq() ;
   if (old_pri_level)
      _primask_level = old_pri_level ;
   else
      __enable_irq() ;
   return 0 ;
   }


// Finds free slot in system object's table
static OS_SYNC* FindFreeObject(int *perr)
   {
   register uint32_t  n = OS_MAX_EVENTS ;
   register OS_SYNC  *pSem = OS_Run.OS_Events ;

   do
      {
      if (pSem->Mtx.active == 0)
         {
         pSem->Sem.queue = 0 ;
         *perr = 0 ;
         return pSem ;
         } ;
      pSem++ ;
      }
   while (--n) ;
   *perr = OS_ERR_NO_MORE_SYNCS ;
   return NULL ;
   }


/**************************************************************************************
 *
 *
 *                                Creates semaphore
 *
 *
 **************************************************************************************/
HANDLE OS_SemCreate(uint32_t count, int *perr)
   {
   register OS_SYNC *pSem ;

   __disable_irq() ;
   pSem = FindFreeObject(perr) ;
   if (pSem)
      {
      pSem->Sem.active = OS_SEM_ACTIVE ;
      pSem->Sem.value = count ;
      } ;
   if (!_primask_level) __enable_irq() ;
   return pSem ;
   }



/**************************************************************************************
 *
 *
 *                                Decrement semaphore
 *
 *
 **************************************************************************************/
int OS_SemPend(HANDLE pSem, uint32_t timeout)
   {
   register uint32_t  cnt, mask, v, old_pri_level ;

   __disable_irq() ;
   cnt = OS_Run.CurrentThread ;
   old_pri_level = _primask_level ;
   if (old_pri_level) _primask_level = 0 ;
   mask = 0x80000000 >> cnt ;
   while (1)
      {
      // Check, if semaphore was created (or deleted during wait)
      if (!pSem || pSem->Sem.active != OS_SEM_ACTIVE)
         {
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return OS_ERR_INVALID_HANDLE ;
         } ;

      // Check, if counter greater than 0
      v = pSem->Sem.value ;
      if (v)
         {
         pSem->Sem.value = v - 1 ;
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return 0 ;
         } ;

      // If time-out is 0, return error immediately
      if (timeout == 0)
         {
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return OS_ERR_TIMEOUT ;
         } ;

      // Trigger context switch (will be in effect when we open interrupts)
      TriggerPendSV() ;

      // Suspend current thread
      pSem->Sem.queue |= mask ;
      OS_Run.ActiveThreads ^= mask ;

      // If finite timeout specified, set timer
      if (timeout != INFINITY)
         {
         OS_Run.ticks[cnt] = timeout ;
         OS_Run.TimeWaitThreads |= mask ;
         } ;

      // Enable interrupts and let other threads to run
      __enable_irq() ;
      __NOP() ;

      // When we get here we should have semaphore value greater than 0, or time-out expired, or somebody took over the semaphore, so we should continue waiting
      __disable_irq() ;
      // Remaining wait time
      if (timeout != INFINITY) timeout = OS_Run.ticks[cnt] ;
      // Remove current thread from semaphore queue (it could be already removed)
      pSem->Sem.queue &= ~mask ;
      } ;
   }



/**************************************************************************************
 *
 *
 *                                Increment semaphore
 *
 *
 **************************************************************************************/
int OS_SemPost(HANDLE pSem)
   {
   register uint32_t  cnt, queue ;

   __disable_irq() ;
   // Check, if semaphore was created
   if (!pSem || pSem->Sem.active != OS_SEM_ACTIVE)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;

   // Increment semaphore's counter
   cnt = pSem->Sem.value + 1 ;
   pSem->Sem.value = cnt ;

   // If semaphore was 0 and there are waiting threads, release the one with highest priority
   if (cnt == 1)
      {
      queue = pSem->Sem.queue ;
      if (queue)
         {
         // Released thread ID
         cnt = __CLZ(queue) ;
         // Switch context, if released thread has higher priority than the current one
         if (cnt < OS_Run.CurrentThread) TriggerPendSV() ;
         // Create release thread mask
         cnt = 0x80000000 >> cnt ;
         // Remove tread from semaphore waiting list
         pSem->Sem.queue = queue ^ cnt ;
         // Make thread active
         OS_Run.ActiveThreads |= cnt ;
         // Remove thread from OS waiting list
         OS_Run.TimeWaitThreads &= ~cnt ;
         } ;
      } ;
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }


/**************************************************************************************
 *
 *
 *                                Reset semaphore
 *
 *
 **************************************************************************************/
int OS_SemReset(HANDLE pSem)
   {
   register int  rc ;

   __disable_irq() ;
   // Check, if semaphore was created
   if (pSem && pSem->Sem.active == OS_SEM_ACTIVE)
      {
      // Reset semaphore's counter
      pSem->Sem.value = 0 ;
      rc = 0 ;
      }
   else
      rc = OS_ERR_INVALID_HANDLE ;
   if (!_primask_level) __enable_irq() ;
   return rc ;
   }



/**************************************************************************************
 *
 *
 *                                Delete semaphore
 *
 *
 **************************************************************************************/
int OS_SemDelete(HANDLE pSem)
   {
   register uint32_t  queue ;

   __disable_irq() ;
   // Check, if semaphore was created
   if (!pSem || pSem->Sem.active != OS_SEM_ACTIVE)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;
   // Delete
   pSem->Sem.active = 0 ;
   // Release waiting threads, if any
   queue = pSem->Sem.queue ;
   if (queue)
      {
      TriggerPendSV() ;
      OS_Run.ActiveThreads |= queue ;
      OS_Run.TimeWaitThreads &= ~queue ;
      } ;
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }



/**************************************************************************************
 *
 *
 *                                Creates event
 *
 *
 **************************************************************************************/
HANDLE OS_EventCreate(uint8_t bSet, uint8_t bManual, int *perr)
   {
   register OS_SYNC *pEvt ;

   __disable_irq() ;
   pEvt = FindFreeObject(perr) ;
   if (pEvt)
      {
      pEvt->Evt.active = OS_EVENT_ACTIVE ;
      pEvt->Evt.flags = bSet ? OS_EVENT_FLAG_SET : 0 ;
      if (bManual) pEvt->Evt.flags |= OS_EVENT_FLAG_MANUAL ;
      } ;
   if (!_primask_level) __enable_irq() ;
   return pEvt ;
   }


/**************************************************************************************
 *
 *
 *                        Reset event to non-signal state
 *
 *
 **************************************************************************************/
int OS_EventReset(HANDLE pEvt)
   {
   __disable_irq() ;
   // Check, if event was created
   if (!pEvt || pEvt->Evt.active != OS_EVENT_ACTIVE)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;

   // Reset event
   pEvt->Evt.flags &= ~OS_EVENT_FLAG_SET ;
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }


/**************************************************************************************
 *
 *
 *                            Set event to signal state
 *
 *
 **************************************************************************************/
int OS_EventSet(HANDLE pEvt)
   {
   register uint32_t  flags, queue ;

   __disable_irq() ;
   // Check, if event was created
   if (!pEvt || pEvt->Evt.active != OS_EVENT_ACTIVE)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;
   // Set event, if it is not set
   flags = pEvt->Evt.flags ;
   if ((flags & OS_EVENT_FLAG_SET) == 0)
      {
      // Indicate that event is set
      pEvt->Evt.flags = flags | OS_EVENT_FLAG_SET ;
      // Check, if there are waiting threads
      queue = pEvt->Evt.queue ;
      if (queue)
         {
         register uint32_t cnt = __CLZ(queue) ;
         // Switch context, if any released thread has higher priority than the current one
         if (cnt < OS_Run.CurrentThread) TriggerPendSV() ;
         // Release by event type
         if (flags & OS_EVENT_FLAG_MANUAL)
            {
            // For manual events: release all waiting threads
            OS_Run.ActiveThreads |= queue ;
            OS_Run.TimeWaitThreads &= ~queue ;
            pEvt->Evt.queue = 0 ;
            }
         else
            {
            // For automatic events: release highest priority thread
            cnt = 0x80000000 >> cnt ;
            // Remove thread from event waiting list
            pEvt->Evt.queue = queue ^ cnt ;
            // Make tread active
            OS_Run.ActiveThreads |= cnt ;
            // Remove thread from OS waiting list
            OS_Run.TimeWaitThreads &= ~cnt ;
            } ;
         } ;
      } ;
   // Enable interrupts end exit
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }


/**************************************************************************************
 *
 *
 *                                Wait for event
 *
 *
 **************************************************************************************/
int OS_EventWait(HANDLE pEvt, uint32_t timeout)
   {
   register uint32_t  cnt, mask, flags, old_pri_level ;

   __disable_irq() ;
   // Our thread number
   cnt = OS_Run.CurrentThread ;
   old_pri_level = _primask_level ;
   if (old_pri_level) _primask_level = 0 ;
   // Mask that corresponds to this thread
   mask = 0x80000000 >> cnt ;
   while (1)
      {
      // Check, if event was created and still exists, while we were waiting
      if (!pEvt || pEvt->Evt.active != OS_EVENT_ACTIVE)
         {
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return OS_ERR_INVALID_HANDLE ;
         } ;

      // Check if event is set. Exit, if so.
      flags = pEvt->Evt.flags ;
      if ((flags & OS_EVENT_FLAG_SET))
         {
         // If event is automatic, set it to non-signaled state. Leave manual event signaled
         if ((flags & OS_EVENT_FLAG_MANUAL) == 0) pEvt->Evt.flags =  flags ^ OS_EVENT_FLAG_SET ;
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return 0 ;
         } ;

      // If time-out is 0, return error immediately
      if (timeout == 0)
         {
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return OS_ERR_TIMEOUT ;
         } ;

      // Trigger context switch (will be in effect when we open interrupts)
      TriggerPendSV() ;

      // Place this thread on event's queue
      pEvt->Evt.queue |= mask ;
      // Suspend current thread
      OS_Run.ActiveThreads ^= mask ;

      // If finite timeout specified, set timer and mark thread for time wait
      if (timeout != INFINITY)
         {
         OS_Run.ticks[cnt] = timeout ;
         OS_Run.TimeWaitThreads |= mask ;
         } ;

      // Enable interrupts and let other threads to run
      __enable_irq() ;
      __NOP() ;

      // When we get here we should have event flag set, or time-out expired
      __disable_irq() ;
      // Remaining wait time
      if (timeout != INFINITY) timeout = OS_Run.ticks[cnt] ;
      // Remove current thread from event queue (it could be already removed)
      pEvt->Evt.queue &= ~mask ;
      } ;
   }


/**************************************************************************************
 *
 *
 *                                Delete event
 *
 *
 **************************************************************************************/
int OS_EventDelete(HANDLE pEvt)
   {
   register uint32_t  queue ;

   __disable_irq() ;
   // Check, if semaphore was created
   if (!pEvt || pEvt->Evt.active != OS_EVENT_ACTIVE)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;
   // Delete
   pEvt->Evt.active = 0 ;
   // Release waiting threads, if any
   queue = pEvt->Evt.queue ;
   if (queue)
      {
      TriggerPendSV() ;
      OS_Run.ActiveThreads |= queue ;
      OS_Run.TimeWaitThreads &= ~queue ;
      } ;
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }



/**************************************************************************************
 *
 *
 *                                Creates mutex
 *
 *
 **************************************************************************************/
HANDLE OS_MutexCreate(uint32_t bOwner, int *perr)
   {
   register OS_SYNC *pMtx ;

   __disable_irq() ;
   pMtx = FindFreeObject(perr) ;
   if (pMtx)
      {
      pMtx->Mtx.active = OS_MUTEX_ACTIVE ;
      if (bOwner)
         {
         pMtx->Mtx.owner = OS_Run.CurrentThread ;
         pMtx->Mtx.count = 1 ;
         }
      else
         {
         pMtx->Mtx.owner = OS_MAX_THREADS ;
         pMtx->Mtx.count = 0 ;
         } ;
      } ;
   if (!_primask_level) __enable_irq() ;
   return pMtx ;
   }



/**************************************************************************************
 *
 *
 *                        Obtain ownership of a mutex
 *
 *
 **************************************************************************************/
int OS_MutexClaim(HANDLE pSem, uint32_t timeout)
   {
   register uint32_t  cnt, mask, owner, old_pri_level ;

   __disable_irq() ;
   cnt = OS_Run.CurrentThread ;
   old_pri_level = _primask_level ;
   if (old_pri_level) _primask_level = 0 ;
   mask = 0x80000000 >> cnt ;
   while (1)
      {
      // Check, if mutex was created and was not deleted during wait
      if (!pSem || pSem->Mtx.active != OS_MUTEX_ACTIVE)
         {
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return OS_ERR_INVALID_HANDLE ;
         } ;

      owner = pSem->Mtx.owner ;
      // Check, if it already belongs to us
      if (owner == cnt)
         {
         // Increment lock counter
         pSem->Mtx.count++ ;
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return 0 ;
         } ;
      // Check, if mutex is available
      if (owner == OS_MAX_THREADS)
         {
         pSem->Mtx.owner = (uint16_t)cnt ;
         pSem->Mtx.count = 1 ;
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return 0 ;
         } ;
      // If time-out is 0, return error immediately
      if (timeout == 0)
         {
         if (old_pri_level)
            _primask_level = old_pri_level ;
         else
            __enable_irq() ;
         return OS_ERR_TIMEOUT ;
         } ;

      // Trigger context switch
      TriggerPendSV() ;

      // Suspend current thread
      pSem->Mtx.queue |= mask ;
      OS_Run.ActiveThreads ^= mask ;

      // If finite timeout specified, set timer
      if (timeout != INFINITY)
         {
         OS_Run.ticks[cnt] = timeout ;
         OS_Run.TimeWaitThreads |= mask ;
         } ;

      // Let other threads to run
      __enable_irq() ;
      __NOP() ;

      // When we get here we should have mutex available, or time-out expired
      __disable_irq() ;
      // Remaining wait time
      if (timeout != INFINITY) timeout = OS_Run.ticks[cnt] ;
      // Remove current thread from mutex queue (may be already removed)
      pSem->Mtx.queue &= ~mask ;
      } ;
   }



/**************************************************************************************
 *
 *
 *                                Release mutex
 *
 *
 **************************************************************************************/
int OS_MutexReleaseDrop(HANDLE pSem, uint32_t bDrop)
   {
   register uint32_t  cnt, queue ;

   __disable_irq() ;
   // Check, if mutex was created
   if (!pSem || pSem->Mtx.active != OS_MUTEX_ACTIVE)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;
   // Check, if it is already released
   queue = pSem->Mtx.owner ;
   if (queue == OS_MAX_THREADS)
      {
      if (!_primask_level) __enable_irq() ;
      return 0 ;
      } ;
   // Check, if mutex belongs to us
   if (queue != OS_Run.CurrentThread)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_MUTEX_OWNERSHIP ;
      } ;

   // If bDrop is not 0, release mutex regardless of locking level
   if (!bDrop)
      {
      // Exit, if lock level is greater than 1
      cnt = pSem->Mtx.count ;
      if (cnt > 1)
         {
         // Decrement lock level
         pSem->Mtx.count = (uint16_t)(cnt - 1) ;
         if (!_primask_level) __enable_irq() ;
         return 0 ;
         } ;
      } ;

   // Release mutex
   pSem->Mtx.owner = OS_MAX_THREADS ;

   // If there are waiting threads, release the one with highest priority
   queue = pSem->Mtx.queue ;
   if (queue)
      {
      cnt = __CLZ(queue) ;
      // Switch context, if released thread has higher priority than the current one
      if (cnt < OS_Run.CurrentThread) TriggerPendSV() ;
      // Create thread mask for thread to be released
      cnt = 0x80000000 >> cnt ;
      // Remove thread from mutext waiting list
      pSem->Mtx.queue = queue ^ cnt ;
      // Make thread active
      OS_Run.ActiveThreads |= cnt ;
      // Remove thread from OS waiting list
      OS_Run.TimeWaitThreads &= ~cnt ;
      } ;

   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }



/**************************************************************************************
 *
 *
 *                                Delete mutex
 *
 *
 **************************************************************************************/
int OS_MutexDelete(HANDLE pMtx)
   {
   register uint32_t queue ;

   __disable_irq() ;
   // Check, if mutex was created
   if (!pMtx || pMtx->Mtx.active != OS_MUTEX_ACTIVE)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;
   // Check, if it is already released or belongs to us
   queue = pMtx->Mtx.owner ;
   if (queue != OS_MAX_THREADS && queue != OS_Run.CurrentThread)
      {
      if (!_primask_level) __enable_irq() ;
      return OS_ERR_MUTEX_OWNERSHIP ;
      } ;
   // Delete
   pMtx->Mtx.active = 0 ;
   // Release waiting threads, if any
   queue = pMtx->Mtx.queue ;
   if (queue)
      {
      TriggerPendSV() ;
      OS_Run.ActiveThreads |= queue ;
      OS_Run.TimeWaitThreads &= ~queue ;
      } ;
   if (!_primask_level) __enable_irq() ;
   return 0 ;
   }


#ifdef _TEST_
/**************************************************************************************
 *
 *
 *                                Creates queue
 *
 *
 **************************************************************************************/
HQUEUE OS_QueueCreate(uint32_t maxElements, int *perr)
   {
   register OS_QUEUE *pQueue ;

   if (maxElements == 0)
      {
      *perr = OS_ERR_INVALID_ARGUMENT ;
      return NULL ;
      } ;
   // Allocate memory for QUEUE object
   pQueue = (OS_QUEUE*)malloc(sizeof(OS_QUEUE) + sizeof(void*)*(maxElements - 1)) ;
   if (!pQueue)
      {
      *perr = OS_ERR_OUT_OF_MEMORY ;
      return NULL ;
      } ;
   // Create semaphores
   pQueue->QueuePostSem = OS_SemCreate(maxElements, perr) ;
   if (!pQueue->QueuePostSem)
      {
    ERR_EXIT:
      free(pQueue) ;
      return NULL ;
      } ;
   pQueue->QueueReadSem = OS_SemCreate(0, perr) ;
   if (!pQueue->QueueReadSem)
      {
      OS_SemDelete(pQueue->QueuePostSem) ;
      goto ERR_EXIT ;
      } ;
   // Initialize object
   pQueue->active = OS_QUEUE_ACTIVE ;
   pQueue->max_elements = maxElements ;
   pQueue->queue_count = pQueue->readIdx = pQueue->writeIdx = 0 ;
   // Return pointer to queue
   return pQueue ;
   }


/**************************************************************************************
 *
 *
 *                                Delete queue
 *
 *
 **************************************************************************************/
int OS_QueueDelete(HQUEUE pQueue)
   {
   cs_enter() ;
   // Check, if queue was created
   if (!pQueue || pQueue->active != OS_QUEUE_ACTIVE)
      {
      if (0 == --_primask_level) __enable_irq() ;
      return OS_ERR_INVALID_HANDLE ;
      } ;
   // Delete
   pQueue->active = 0 ;
   OS_SemDelete(pQueue->QueuePostSem) ;
   OS_SemDelete(pQueue->QueueReadSem) ;
   if (0 == --_primask_level) __enable_irq() ;
   free(pQueue) ;
   return 0 ;
   }


/**************************************************************************************
 *
 *
 *                          Post new element to the queue
 *
 *
 **************************************************************************************/
int OS_QueuePost(HQUEUE pQueue, void *pElem, uint32_t timeout)
   {
   register int  rc ;

   // Check, if queue was created
   if (!pQueue || pQueue->active != OS_QUEUE_ACTIVE) return OS_ERR_INVALID_HANDLE ;
   // Wait for space in the queue
   rc = OS_SemPend(pQueue->QueuePostSem, timeout) ;
   if (rc) return rc ;
   // Insert new element
   cs_enter() ;
   pQueue->data[pQueue->writeIdx++] = pElem ;
   if (pQueue->writeIdx == pQueue->max_elements) pQueue->writeIdx = 0 ;
   pQueue->queue_count++ ;
   if (0 == --_primask_level) __enable_irq() ;
   // Post reading semaphore
   return OS_SemPost(pQueue->QueueReadSem) ;
   }


/**************************************************************************************
 *
 *
 *                            Gets element from the queue
 *
 *
 **************************************************************************************/
void *OS_QueueGet(HQUEUE pQueue, uint32_t timeout, int *perr)
   {
   register int  rc ;
   register void  *pElem ;

   // Check, if queue was created
   if (!pQueue || pQueue->active != OS_QUEUE_ACTIVE)
      {
      *perr = OS_ERR_INVALID_HANDLE ;
      return NULL ;
      } ;
   // Wait for available element in the queue
   rc = OS_SemPend(pQueue->QueueReadSem, timeout) ;
   if (rc)
      {
      *perr = rc ;
      return NULL ;
      } ;
   // Get top element
   cs_enter() ;
   pElem = pQueue->data[pQueue->readIdx++] ;
   if (pQueue->readIdx == pQueue->max_elements) pQueue->readIdx = 0 ;
   if (0 == --_primask_level) __enable_irq() ;
   // Post free space semaphore
   rc = OS_SemPost(pQueue->QueuePostSem) ;
   *perr = rc ;
   // Success
   if (!rc) return pElem ;
   return NULL ;
   }

#endif


/**************************************************************************************************
 *
 *
 *                     System tick timer interrupt.  1 ms frequency
 *
 *   This ISR implements system milliseconds timer.  The current value is in TimeMS variable:
 *   time in ms since system start-up.  _T_ variable keeps current time in seconds. This value
 *   is periodically synchronized with RTC.
 *
 *   ISR executes application timers.  For object-type timers, the ISR increments the value
 *   of corresponding semaphore or signal event.  If any threads are waiting on those objects,
 *   they will be unblocked.
 *
 *   If active executable timer is expired, the ISR will wake-up timers thread, so it could
 *   execute function attached to this timer.
 *
 *   Finally, ISR checks time-outs of sleeping threads.  It will wake-up threads whose timeouts
 *   expired.
 *
 *
 **************************************************************************************************/
void RAM_FNC SysTick_Handler(void)
   {
   register stAppTimer_t *p ;
   register uint32_t      n, ActiveThreads, TimeWaitThreads ;

   // Increment current ticks count
   TimeMS++ ;
   n = seconds ;
   if (0 == --n)
      {
      // Increment seconds timer
      _T_++ ;
      seconds = 1000 ;
      // On-board heart beat LED
      LED_Blink ^= 1 ;
      if (LED_Blink)
         GPIOC->BSRRL = SYS_LED_Pin ;
      else
         GPIOC->BSRRH = SYS_LED_Pin ;
      }
   else
      seconds = n ;

   // Check application timers. This will signal TmrEvent event, if any of executable timers expired.
   // This also can make some threads active, if semaphore or event type timers expired.
   n = eTmr_Max ;
   p = Timers ;

   // Disable interrupts
   __disable_irq() ;
   // List of currently active threads
   ActiveThreads = OS_Run.ActiveThreads ;
   // List of time-waiting threads
   TimeWaitThreads = OS_Run.TimeWaitThreads ;

   // Go thru list of application timers and perform action according to the timer type
   do
      {
      // Get timer's flags
      register uint32_t  f = p->f ;
      // Do nothing, if timer is not running
      if (f & TIMER_RUNNING)
         {
         register uint32_t  t = p->t ;
         // Compare current time or decrement timer counter. Do nothing, if timer is not expired
         if (((f & TIMER_ABS_TIME) && t >= _T_) || 0 == (p->t = t - 1))
            {
            // Timer expired. Take action according to timer type
            if (f & TIMER_SEMAPHORE)
               {
               // Semaphore-type timer
               register OS_SYNC *pSem = p->SyncObj ;
               // Must be created and have proper signature
               if (pSem && pSem->Sem.active == OS_SEM_ACTIVE)
                  {
                  register uint32_t  cnt, queue ;
                  // Increment semaphore's counter
                  pSem->Sem.value++ ;
                  // If there are waiting threads, release the one with highest priority
                  queue = pSem->Sem.queue ;
                  if (queue)
                     {
                     // Mask of thread to release
                     cnt = 0x80000000 >> __CLZ(queue) ;
                     // Remove from queue
                     pSem->Sem.queue = queue ^ cnt ;
                     // Add tread to active threads
                     ActiveThreads |= cnt ;
                     // Remove from list of time waiting threads (may not be there for INFINITE timeout)
                     TimeWaitThreads &= ~cnt ;
                     } ;
                  } ;
               // Restart timer, if it is has auto-reload property
               if (f & TIMER_AUTORELOAD)
                  p->t = p->reload ;            // Re-load and keep it running
               else
                  p->f = f ^ TIMER_RUNNING ;    // Stop timer
               }
            // Event-type timer
            else if (f & TIMER_EVENT)
               {
               register OS_SYNC *pEvt = p->SyncObj ;
               // Event must be created and have valid signature
               if (pEvt && pEvt->Evt.active == OS_EVENT_ACTIVE)
                  {
                  register uint32_t  cnt, queue ;

                  cnt = pEvt->Evt.flags ;
                  // Only process, if event is not signaled
                  if (0 == (cnt & OS_EVENT_FLAG_SET))
                     {
                     // Set event
                     pEvt->Evt.flags = cnt | OS_EVENT_FLAG_SET ;
                     // Check, if there are waiting threads
                     queue = pEvt->Evt.queue ;
                     if (queue)
                        {
                        if (cnt & OS_EVENT_FLAG_MANUAL)
                           {
                           // For manual event release all waiting threads
                           ActiveThreads |= queue ;
                           // Remove from list of time waiting threads (may not be there for INFINITE timeout)
                           TimeWaitThreads &= ~queue ;
                           pEvt->Evt.queue = 0 ;
                           }
                        else
                           {
                           // For automatic event release only highest priority thread
                           cnt = 0x80000000 >> __CLZ(queue) ;
                           // Remove thread from queue
                           pEvt->Evt.queue = queue ^ cnt ;
                           // Add thread to pool of active threads
                           ActiveThreads |= cnt ;
                           // Remove from list of time waiting threads (may not be there for INFINITE timeout)
                           TimeWaitThreads &= ~cnt ;
                           } ;
                        } ;
                     } ;
                  } ;
               // Restart timer, if it is has auto-reload property
               if (f & TIMER_AUTORELOAD)
                  p->t = p->reload ;
               else
                  p->f = f ^ TIMER_RUNNING ;
               }
            // Executable timer
            else
               {
               f ^= TIMER_RUNNING ;
               if (p->fptr0 != NULL)
                  {
                  register uint16_t idx = waiting_timers.write_idx ;
                  // Function attached. Mark this timer, as waiting
                  p->f = f | TIMER_WAITING ;
                  // Place timer's number on the processing queue
                  waiting_timers.tmr_queue[idx] = p ;
                  if (++idx == eTmr_Max) idx = 0 ;
                  waiting_timers.write_idx = idx ;
                  // Increment timers semaphore value
                  TmrSem->Sem.value++ ;
                  // Release timers thread
                  ActiveThreads |= 0x80000000 >> TIMERS_PRIO ;
                  }
               else
                  // No function attached.  Simply indicate EXPIRED condition
                  p->f = f | TIMER_EXPIRED ;
               } ;
            } ;
         } ;
      p++ ;
      }
   while (--n) ;

   // Check, if any thread should wake-up, because its waiting period ended.  Main thread must be already running
   n = TimeWaitThreads ;                           // Combined mask of waiting threads
   while (n)
      {
      register uint32_t i = __CLZ(n) ;             // Next waiting thread number
      register uint32_t t = OS_Run.ticks[i] - 1 ;  // Decrement wait counter
      OS_Run.ticks[i] = t ;                        // Update wait counter
      i = 0x80000000 >> i ;                        // Mask of this thread
      if (0 == t)
         {
         ActiveThreads |= i ;                      // If thread wait counter is 0, wake up this thread
         TimeWaitThreads ^= i ;                    // Remove threads from waiting list
         } ;
      n ^= i ;                                     // Remove thread from combined mask
      } ;

   // Switch context, if necessary
   if (OS_Run.ActiveThreads != ActiveThreads)
      {
      OS_Run.ActiveThreads = ActiveThreads ;
      OS_Run.TimeWaitThreads = TimeWaitThreads ;
      // If any new thread, has higher priority than running thread, trigger context switch
      if (__CLZ(ActiveThreads) < OS_Run.CurrentThread) TriggerPendSV() ;
      } ;
   // Enable interrupts
   __enable_irq() ;
   }


/**************************************************************************************
 *
 *
 *                              System timers thread
 *                Executes functions attached to application timers
 *    This thread has lowest possible priority.  Therefore, functions only executed
 *    when all other threads are inactive.
 *
 *
 **************************************************************************************/
static void RAM_FNC TimersThread(void *dummy)
   {
   register stAppTimer_t   *tmr ;
   register uint32_t        f ;
   register volatile FPTR0  fptr ;

   // Semaphore is signaled, when we have at least one waiting timer
   while (!OS_SemPend(TmrSem, INFINITY))
      {
      register uint16_t  idx = waiting_timers.read_idx ;
      // Get pointer to the timer from the waiting queue
      tmr = waiting_timers.tmr_queue[idx] ;
      // Remove pointer from the queue
      if (++idx == eTmr_Max) idx = 0 ;
      waiting_timers.read_idx = idx ;
      // Read timer data
      __disable_irq() ;
      f = tmr->f ;
      // Only process timers in waiting state
      if (f & TIMER_WAITING)
         {
         // Clear waiting flag
         tmr->f = f ^ TIMER_WAITING ;
         // Function pointer
         fptr = tmr->fptr0 ;
         __enable_irq() ;
         _primask_level = 0 ;
         // Execute call-back, if defined
         if (fptr)
            {
            if (f & TIMER_FNC_1)
               ((FPTR1)fptr)((void*)(tmr->context)) ;
            else
               fptr() ;
            } ;
         // Call-back function could change timer flags
         __disable_irq() ;
         f = tmr->f ;
         // Re-start auto-reload timer, if it is not running and it is not stopped
         if (!(f & TIMER_RUNNING) && (f & TIMER_AUTORELOAD) && !(f & TIMER_STOPPED))
            {
            tmr->t = tmr->reload ;        // Reload timer value
            tmr->f = f | TIMER_RUNNING ;  // Run it
            } ;
         } ;
      __enable_irq() ;
      } ;
   // Cannot happen
   //DbgPrintf("Timers thread exited?!\r\n") ;
   }


// System Reset
void RAM_FNC OS_Reset(void)
   {
   __DSB() ;
   SCB->AIRCR = 0x05FA0000 | (SCB->AIRCR & 0x00000700) | 0x4 ;
   for(;;) __DSB() ;
   }


uint16_t OS_SchedLock(void)
   {
   register uint16_t level ;

   __disable_irq() ;
   level = OS_Run.SchedLockCnt + 1 ;
   OS_Run.SchedLockCnt = level ;
   if (level == 1) OS_Run.LockedThreadMask = 0x80000000 >> OS_Run.CurrentThread ;
   if (!_primask_level) __enable_irq() ;
   return level ;
   }


uint16_t OS_SchedUnLock(void)
   {
   register uint16_t level ;

   __disable_irq() ;
   level = OS_Run.SchedLockCnt ;
   if (level)
      {
      level-- ;
      OS_Run.SchedLockCnt = level ;
      } ;
   if (level == 0 && __CLZ(OS_Run.ActiveThreads) < OS_Run.CurrentThread) TriggerPendSV() ;
   if (!_primask_level) __enable_irq() ;
   return level ;
   }


/****************************************************************************
 *
 *           Support for standard malloc() and free() functions
 *     Heap starts at _heap_start address and ends at _heap_end address,
 *     as defined in linker script.
 *
 ****************************************************************************/
// These symbols defined in linker script: application heap start and end
extern unsigned char _user_heap_start, _user_heap_end ;

caddr_t _sbrk(int incr)
   {
   static uint8_t *currHeapEnd = &_user_heap_start ;
   register uint8_t *prevHeapEnd ;

   if ((uint32_t)(currHeapEnd + incr) >= (uint32_t)&_user_heap_end) return (caddr_t)(-1) ;
   prevHeapEnd = currHeapEnd ;
   currHeapEnd += incr ;
   return (caddr_t)prevHeapEnd ;
   }


// Get exclusive access to malloc()
void __malloc_lock(struct _reent *REENT)
   {
   OS_MutexClaim(MemMutex, INFINITY) ;
   }


// Release access to malloc()
void __malloc_unlock(struct _reent *_r)
   {
   OS_MutexRelease(MemMutex) ;
   }

/*********************  End of memory allocation functions  ***********************************/


void ExceptionProcess(uint32_t type, uint32_t address)
   {
   DbgPrintf("Exception type %u at 0x%X\r\n", type, address) ;
   while (1) ;
   }

