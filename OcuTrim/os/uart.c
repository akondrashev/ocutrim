/***************************************************************
 *
 *                        UART Driver
 *
 *
 ***************************************************************/
#include "os.h"
#include "Uart.h"


#define UART_GETCLOCKSRC(__HANDLE__,__CLOCKSOURCE__) \
  do {                                                        \
    if((__HANDLE__) == USART1)                      \
    {                                                         \
       switch(__HAL_RCC_GET_USART1_SOURCE())                  \
       {                                                      \
        case RCC_USART1CLKSOURCE_D2PCLK2:                      /*sasa  RCC_USART1CLKSOURCE_D2PCLK2*/  \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;        /*sasa  RCC_USART1CLKSOURCE_PLL2*/     \
          break;                                              \
        case RCC_USART1CLKSOURCE_HSI:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART1CLKSOURCE_PLL3:                         /*sasa  RCC_USART1CLKSOURCE_PLL3*/     \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_PLL3;           /*sasa  UART_CLOCKSOURCE_SYSCLK*/       \
          break;                                              \
        case RCC_USART1CLKSOURCE_LSE:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                         \
    else if((__HANDLE__) == USART2)                 \
    {                                                         \
       switch(__HAL_RCC_GET_USART2_SOURCE())                  \
       {                                                      \
        case  RCC_USART234578CLKSOURCE_D2PCLK1:             /* sasa  RCC_USART2CLKSOURCE_PCLK1*/  \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;         /* sasa  UART_CLOCKSOURCE_PCLK1;*/    \
          break;                                              \
        case RCC_USART2CLKSOURCE_HSI:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART2CLKSOURCE_PLL2:                   /* sasa  RCC_USART2CLKSOURCE_SYSCLK*/  \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_PLL2;        /* sasa  UART_CLOCKSOURCE_SYSCLK*/     \
          break;                                              \
        case RCC_USART2CLKSOURCE_LSE:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                         \
    else if((__HANDLE__) == USART3)                 \
    {                                                         \
       switch(__HAL_RCC_GET_USART3_SOURCE())                  \
       {                                                      \
        case  RCC_USART234578CLKSOURCE_D2PCLK1:             /* sasa RCC_USART3CLKSOURCE_PCLK1*/    \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;        /*UART_CLOCKSOURCE_PCLK1*/         \
          break;                                              \
        case RCC_USART3CLKSOURCE_HSI:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART2CLKSOURCE_PLL2:                   /* sasa  RCC_USART3CLKSOURCE_SYSCLK*/  \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_PLL2;        /* sasa  UART_CLOCKSOURCE_SYSCLK*/     \
          break;                                              \
        case RCC_USART3CLKSOURCE_LSE:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                         \
    else if((__HANDLE__) == UART4)                  \
    {                                                         \
       switch(__HAL_RCC_GET_UART4_SOURCE())                   \
       {                                                      \
        case RCC_USART234578CLKSOURCE_D2PCLK1:             /* sasa RCC_UART4CLKSOURCE_PCLK1*/       \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_PCLK1*/         \
          break;                                              \
        case RCC_UART4CLKSOURCE_HSI:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART2CLKSOURCE_PLL2:                  /* sasa RCC_UART4CLKSOURCE_SYSCLK*/      \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasaUART_CLOCKSOURCE_SYSCLK*/         \
          break;                                              \
        case RCC_UART4CLKSOURCE_LSE:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                         \
    else if ((__HANDLE__) == UART5)                 \
    {                                                         \
       switch(__HAL_RCC_GET_UART5_SOURCE())                   \
       {                                                      \
        case RCC_USART234578CLKSOURCE_D2PCLK1:             /* sasa RCC_UART5CLKSOURCE_PCLK1*/       \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_PCLK1*/         \
          break;                                              \
        case RCC_UART5CLKSOURCE_HSI:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART2CLKSOURCE_PLL2:                  /* sasa RCC_UART5CLKSOURCE_SYSCLK*/      \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_SYSCLK*/        \
          break;                                              \
        case RCC_UART5CLKSOURCE_LSE:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                         \
    else if((__HANDLE__) == USART6)                 \
    {                                                         \
       switch(__HAL_RCC_GET_USART6_SOURCE())                  \
       {                                                      \
        case RCC_USART234578CLKSOURCE_D2PCLK1:              /* sasa RCC_USART6CLKSOURCE_PCLK2*/      \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa RCC_USART1CLKSOURCE_PLL2*/       \
          break;                                              \
        case RCC_USART6CLKSOURCE_HSI:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART2CLKSOURCE_PLL2:                  /* sasa RCC_USART6CLKSOURCE_SYSCLK*/     \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_SYSCLK*/        \
          break;                                              \
        case RCC_USART6CLKSOURCE_LSE:                         \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                         \
    else if ((__HANDLE__) == UART7)                 \
    {                                                         \
       switch(__HAL_RCC_GET_UART7_SOURCE())                   \
       {                                                      \
        case RCC_USART234578CLKSOURCE_D2PCLK1:             /* sasa RCC_UART7CLKSOURCE_PCLK1*/        \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_PCLK1*/          \
          break;                                              \
        case RCC_UART7CLKSOURCE_HSI:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART2CLKSOURCE_PLL2:                  /* sasa RCC_UART7CLKSOURCE_SYSCLK*/      \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_SYSCLK*/        \
          break;                                              \
        case RCC_UART7CLKSOURCE_LSE:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                                                     \
    else if ((__HANDLE__) == UART8)                 \
    {                                                         \
       switch(__HAL_RCC_GET_UART8_SOURCE())                   \
       {                                                      \
        case RCC_USART234578CLKSOURCE_D2PCLK1:             /* sasa RCC_UART8CLKSOURCE_PCLK1*/        \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_PCLK1*/          \
          break;                                              \
        case RCC_UART8CLKSOURCE_HSI:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_HSI;           \
          break;                                              \
        case RCC_USART2CLKSOURCE_PLL2:                   /* sasa RCC_UART8CLKSOURCE_SYSCLK*/      \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_D2PCLK1;       /* sasa UART_CLOCKSOURCE_SYSCLK*/        \
          break;                                              \
        case RCC_UART8CLKSOURCE_LSE:                          \
          (__CLOCKSOURCE__) = UART_CLOCKSOURCE_LSE;           \
          break;                                              \
        default:                                              \
          break;                                              \
       }                                                      \
    }                                                                                     \
  } while(0)




int SetUARTConfig(UART_typ *uart, uint32_t BaudRate)
   {
   UART_ClockSourceTypeDef clocksource = UART_CLOCKSOURCE_UNDEFINED ;
   register USART_TypeDef *U = uart->hUart ;
   int  err ;

   while (U->ISR & UART_RX_NON_EMPTY) U->RDR ;
   // Clear all interrupts
   U->ICR = 0x123DFF ;
   // Line settings.  Allow IDLE interrupt
   U->CR1 = UART_CR1_SETUP ;
   U->CR2 = 0 ;
   // Allow RX FIFO threshold interrupt at FIFO full
   U->CR3 = UART_RX_FIFO_THRES_IE | (5 << 25) ;

   //-------------------------- USART BRR Configuration -----------------------
   UART_GETCLOCKSRC(U, clocksource) ;

   switch (clocksource)
      {
      case UART_CLOCKSOURCE_D2PCLK1:    /*sasa UART_CLOCKSOURCE_PCLK1*/
         U->BRR = (uint16_t)(UART_DIV_SAMPLING16(HAL_RCC_GetPCLK1Freq(), BaudRate,0/*sasa*/));
         break ;
      case UART_CLOCKSOURCE_D2PCLK2:   /* sasa UART_CLOCKSOURCE_PCLK2*/
         U->BRR = (uint16_t)(UART_DIV_SAMPLING16(HAL_RCC_GetPCLK2Freq(), BaudRate,0/*sasa*/));
         break ;
      case UART_CLOCKSOURCE_HSI:
         U->BRR = (uint16_t)(UART_DIV_SAMPLING16(HSI_VALUE, BaudRate,0/*sasa*/));
         break ;
      case UART_CLOCKSOURCE_PLL3:       /* sasaUART_CLOCKSOURCE_SYSCLK*/
         U->BRR = (uint16_t)(UART_DIV_SAMPLING16(HAL_RCC_GetSysClockFreq(), BaudRate,0/*sasa*/));
         break ;
      case UART_CLOCKSOURCE_PLL2:  //sasa
         U->BRR = (uint16_t)(UART_DIV_SAMPLING16(HAL_RCC_GetPCLK2Freq(), BaudRate,1/*sasa*/));
         break ;
      case UART_CLOCKSOURCE_LSE:
         U->BRR = (uint16_t)(UART_DIV_SAMPLING16(LSE_VALUE, BaudRate/*sasa*/,0));
         break ;
      case UART_CLOCKSOURCE_UNDEFINED:
      default:
         return 1 ;
      } ;

   // RX event
   uart->RXEvent = OS_EventCreate(0, 0, &err) ;
   if (err) return err ;

   // TX event
   uart->TXEvent = OS_EventCreate(0, 0, &err) ;
   if (err) return err ;

   // TX line idle event (manual, preset)
   uart->IdleEvent = OS_EventCreate(1, 1, &err) ;
   if (err) return err ;

   // Enable interrupts
   NVIC_EnableIRQ(uart->IRQn) ;

   // Enable UART
   U->CR1 = UART_CR1_ENABLE_NO_TX ;
   return 0 ;
   }



// Fills TX FIFO from TX buffer
static void INLINE UART_Transmit(UART_typ *uart, USART_TypeDef *UART, uint16_t prev_cnt)
   {
   register uint16_t  cnt, n, msk ;

   msk = uart->UART_TX_BUF_SIZE - 1 ;
   // Max can transmit UART_FIFO_SZIE bytes
   if (prev_cnt > UART_FIFO_SZIE)
      cnt = UART_FIFO_SZIE ;
   else
      cnt = prev_cnt ;
   // Update buffer length
   uart->Tx_Len = prev_cnt - cnt ;
   // Current buffer index
   n = uart->TX_Read_Ind ;
   // Write FIFO
   do
      {
      UART->TDR = uart->Tx_Buff[n] ;
      n = (n + 1) & msk ;
      }
   while (--cnt) ;
   // Update buffer index
   uart->TX_Read_Ind = n ;
   // If TX buffer was previously full, rise TX event
   if (prev_cnt == uart->UART_TX_BUF_SIZE) OS_EventSet(uart->TXEvent) ;
   }




// ISR for UART
static void RAM_FNC UART_IRQHandler(UART_typ *uart)
   {
   register USART_TypeDef *UART = uart->hUart ;
   register uint32_t status = UART->ISR ;

   // RX interrupt?
   if (status & (UART_RX_FIFO_INT | UART_IDLE_INT))
      {
      // Any bytes available?
      if (status & UART_RX_NON_EMPTY)
         {
         // Read FIFO
         register uint16_t  cnt = uart->Rx_Len ;
         register uint16_t  idx = uart->Rx_Write_Idx ;
         register uint16_t  prev_cnt = cnt ;
         register uint16_t  msk = uart->UART_RX_BUF_SIZE - 1 ;

         // Read data into buffer
         do
            {
            register uint8_t  c = UART->RDR ;
            if (cnt < uart->UART_RX_BUF_SIZE)
               {
               uart->Rx_Buff[idx] = c ;
               idx = (idx + 1) & msk ;
               cnt++ ;
               } ;
            }
         while (UART->ISR & UART_RX_NON_EMPTY) ;

         // Update control block
         uart->Rx_Write_Idx = idx ;
         uart->Rx_Len = cnt ;
         // If RX buffer was previously empty, rise RX event
         if (prev_cnt == 0) OS_EventSet(uart->RXEvent) ;
         status = UART->ISR ;
         } ;
      // Clear IDLE line interrupt
      UART->ICR = UART_IDLE_INT ;
      } ;

   // Check this only when we are transmitting data
   if (uart->State == STATE_BUSY && (status & UART_TX_FIFO_EMPTY))
      {
      register uint16_t  len = uart->Tx_Len ;
      // Transmit characters from buffer, if it is not empty
      if (len)
         {
         UART_Transmit(uart, UART, len) ;
         return ;
         } ;
      // Buffer is empty: end transmission
      uart->State = STATE_COMPLETE ;
      UART->CR1 = UART_CR1_ENABLE_TX_COMPLETE ;
      } ;

   // Transmitter idle interrupt
   if (status & UART_TC_INT)
      {
      uart->State = STATE_IDLE ;
      UART->CR1 = UART_CR1_ENABLE_NO_TX ;
      UART->ICR = UART_TC_INT ;
      OS_EventSet(uart->IdleEvent) ;
      } ;
   }



// ISR for UART1
void RAM_FNC USART1_IRQHandler(void)
   {
   UART_IRQHandler(&Uart1) ;
   }


// ISR for UART2
void RAM_FNC USART2_IRQHandler(void)
   {
   UART_IRQHandler(&Uart2) ;
   }


uint32_t Uart_Get(uint8_t *pBuf, uint32_t size, uint32_t timeout, UART_typ *uart)
   {
   register uint32_t  idx, len, msk ;

   if (size == 0) return 0 ;
   len = uart->Rx_Len ;
   if (len == 0)
      {
      if (timeout == 0) return 0 ;
      do
         {
         if (OS_ERR_TIMEOUT == OS_EventWait(uart->RXEvent, timeout)) return 0 ;
         len = uart->Rx_Len ;
         }
      while (len == 0) ;
      } ;
   if (size > len) size = len ;
   idx = uart->RX_Read_Ind ;
   len = size ;
   msk = uart->UART_RX_BUF_SIZE - 1 ;
   do
      {
      *pBuf++ = uart->Rx_Buff[idx] ;
      idx = (idx + 1) & msk ;
      }
   while (--size) ;

   uart->RX_Read_Ind = idx ;
   //__disable_irq() ;
   NVIC_DisableIRQ(uart->IRQn) ;
   uart->Rx_Len -= len ;
   //__enable_irq() ;
   NVIC_EnableIRQ(uart->IRQn) ;
   return len ;
   }



void Uart_Put(uint8_t *pBuf, uint32_t size, uint32_t timeout, UART_typ *uart)
   {
   register uint32_t  idx, n, nBytes, msk ;

   // Position of free space in TX buffer
   idx = uart->Tx_Write_Idx ;
   msk = uart->UART_TX_BUF_SIZE - 1 ;
   // Transmit while "size" is not 0
   while (size)
      {
      // Wait for space in TX buffer
      while (0 == (n = uart->UART_TX_BUF_SIZE - uart->Tx_Len))
         {
         if (OS_EventWait(uart->TXEvent, timeout)) return ;
         } ;
      // Only use number of bytes we need
      if (n > size) n = size ;
      // Copy bytes in TX buffer
      nBytes = n ;
      do
         {
         uart->Tx_Buff[idx] = *pBuf++ ;
         idx = (idx + 1) & msk ;
         }
      while (--nBytes) ;
      // Disable interrupts from UART
      NVIC_DisableIRQ(uart->IRQn) ;
      // Update number of bytes in TX buffer
      uart->Tx_Len += n ;
      // Start new transmission, if the previous one already finished
      if (uart->State != STATE_BUSY)
         {
         UART_Transmit(uart, uart->hUart, uart->Tx_Len) ;
         uart->hUart->CR1 = UART_CR1_ENABLE_TX ;
         uart->State = STATE_BUSY ;
         // Reset IDLE event to non-signaled state
         OS_EventReset(uart->IdleEvent) ;
         } ;
      // Enable interrupts from UART
      NVIC_EnableIRQ(uart->IRQn) ;
      // More bytes to send?
      size -= n ;
      } ;
   // Update TX buffer position
   uart->Tx_Write_Idx = idx ;
   }

