		.syntax unified
  		.cpu cortex-m7
  		.fpu softvfp
  		.thumb

		.global  	PendSV_Handler
		.global		IdleThread
		.global		memcpy
		.global     memset
		.global		HardFault_Handler
		.global		NMI_Handler
		.global		MemManage_Handler
		.global		BusFault_Handler
		.global		UsageFault_Handler
		.global     MoveCRC

	    .section	.fncram
		.type  		PendSV_Handler, %function

		// OS context switcher (PendSV interrupt)
PendSV_Handler:
		ldr		r0, =OS_Run						// r0 - execution table address
		cpsid 	i								// Disable interrupts
		ldmia	r0, {r1,r2,r12}					// r1_h - scheduler lock counter; r1_l - current thread number, r2 - mask of active threads, r12 - lock thread mask
		cmp		r1, #32							// Check, if scheduler lock count greater than 0
		bhi.n	psv1
		clz		r3, r2							// r3 - ID of highest priority ready-to-run thread
		cmp		r3, r1							// Switch context if current thread ID in r1 is not equal to runnable thread ID in r3
		beq.n	exit
psv0:	strh	r3, [r0], #20					// Replace current running tread number and move pointer r0 to array of SPs
		push	{r4-r11,lr}						// Save general registers of current thread
		str		sp, [r0, r1, LSL #2]			// Save stack pointer of current thread
		ldr		sp, [r0, r3, LSL #2]			// Load stack pointer of new thread
		cpsie	i								// Enabe interrupts
		pop		{r4-r11,pc}						// Restore thread's general registers and exit
		// Special case: scheduler locked. Only run locking thread or idle thread
psv1:	and		r1, r1, #0xFF					// r1 - currently running thread number
		tst		r2, r12							// Check, if locking thread is active
		ite		NE
		clzne	r3, r12							// Locking thread active. Will run locking thread. r3 - locking thread number
		moveq	r3, #32							// Locking thread is not active. Will run idle thread
		cmp		r3, r1							// Check, if required thread is already running
		bne.n	psv0							// If not, switch to it
exit:	cpsie	i								// Enabe interrupts
		bx		lr								// Exit (requred thread already running)


		.section	.fncram
		.type  		HardFault_Handler, %function

HardFault_Handler:
		mov		r0, #0
hf1:	ldr		r2, =ExceptionProcess
		ldr		r1, [sp, #24]
		str		r0, [sp]
		str		r2, [sp, #24]
		str		r1, [sp, #4]
		bx		lr

		// Map all bad exceptions to ExceptionProcess() function
UsageFault_Handler:
		mov		r0, #4
		b.n		hf1

BusFault_Handler:
		mov		r0, #3
		b.n		hf1

MemManage_Handler:
		mov		r0, #2
		b.n		hf1

NMI_Handler:
		mov		r0, #1
		b.n		hf1



		.section	.fncram
		.type  		IdleThread, %function
		// System idle thread (does nothing)
IdleThread:
		b.n		IdleThread


		.section	.fncram
		.type  		memcpy, %function

		// void* memcpy(void *DST, void const *SRC, uint32_t COUNT)
memcpy: cbz		r2, mc_e						// Exit, if COUNT is 0
		orr		r3, r0, r1						// Make OR on SRC and DST
		push    {r0}							// Save original dst address to return
		and		r3, r3, #3						// Branch according to minimum alignment
		tbb		[pc, r3]
mc_t:	.byte	(mc_w0 - mc_t)/2
		.byte	(mc_b - mc_t)/2
		.byte	(mc_h0 - mc_t)/2
		.byte	(mc_b - mc_t)/2
		// Everything word-aligned.  Do word transfer
mc_w0:	and		r3, r2, #3						// r3 - number of bytes to transfer
		lsrs	r2, r2, #2						// r2 - number of words to transfer
		beq.n	mc_w2
mc_w1:  ldr		r12, [r1], #4					// Move words
		subs	r2, #1
		str		r12, [r0], #4
		bne.n	mc_w1
        cbz		r3, mc_w3
        // Transfer remaining bytes
mc_w2:  ldrb	r12, [r1], #1
		subs	r3, #1
		strb	r12, [r0], #1
		bne.n	mc_w2
mc_w3:	pop		{r0}
		bx		lr
		// Everything half-word aligned.  Do half-word transfer
mc_h0:	and		r3, r2, #1						// r3 - number of bytes to transfer
		lsrs	r2, r2, #1						// r2 - number of halfwords to transfer
		beq.n	mc_h2
mc_h1:	ldrh	r12, [r1], #2
		subs	r2, #1
		strh	r12, [r0], #2
		bne.n	mc_h1
		cbz		r3, mc_h3
        // Transfer remaining byte
mc_h2:  ldrb	r12, [r1]
		strb	r12, [r0]
mc_h3:	pop		{r0}
		bx		lr
		// Do byte transfer
mc_b:	ldrb	r3, [r1], #1
		subs	r2, #1
		strb	r3, [r0], #1
		bne.n	mc_b
        pop		{r0}
mc_e:	bx		lr


		.section	.fncram
		.type  		memset, %function

		// void* memset(void *Dst, int value, uint32_t Count)
memset: cbz		r2, mz_e						// Exit, if COUNT is 0
		and		r3, r0, #3						// r3 - alignment bits
		mov		r12, r0							// Remember orignal dst to return
		tbb		[pc, r3]
mz_t:	.byte	(mz_w0 - mz_t)/2
		.byte	(mz_b - mz_t)/2
		.byte	(mz_h0 - mz_t)/2
		.byte	(mz_b - mz_t)/2
		// DST is word-aligned.  Do word transfer
mz_w0:  and		r3, r2, #3						// r3 - number of bytes to transfer
		lsrs	r2, r2, #2						// r2 - number of words to transfer
		beq.n	mz_w2
		orr		r1, r1, r1, lsl #8				// Replicate value
		orr		r1, r1, r1, lsl #16
mz_w1: 	subs	r2, #1
		str		r1, [r0], #4
		bne.n	mz_w1
		// Remaining bytes
		cbz		r3, mz_w3
mz_w2:  subs	r3, #1
		strb	r1, [r0], #1
		bne.n	mz_w2
mz_w3:	mov		r0, r12
		bx		lr
		// Everything half-word aligned.  Do half-word transfer
mz_h0:	and		r3, r2, #1						// r3 - number of bytes to transfer
		lsrs	r2, r2, #1						// r2 - number of halfwords to transfer
		beq.n	mz_h2
		orr		r1, r1, r1, lsl #8				// Replicate value
mz_h1:  subs	r2, #1
		strh	r1, [r0], #2
		bne.n	mz_h1
		// Remaining byte
		cbz		r3, mz_h3
mz_h2:  strb	r1, [r0]
mz_h3:	mov		r0, r12
		bx		lr
		// Do byte transfer
mz_b:	subs	r2, #1
		strb	r1, [r0], #1
		bne.n	mz_b
		mov		r0, r12
mz_e:   bx		lr


		.section	.text
		.type  		MoveCRC, %function

// uint32_t MoveCRC(uint8_t *p, uint32_t cnt, void *DR)
// CRC controller communication procedure
MoveCRC:
		push	{lr}
		lsrs	lr, r1, #2						// lr - number of WORDs to move
		beq.n	crc_r
		and		r3, r0, #3						// r3 - alignment bits
		tbb		[pc, r3]
crc_t:	.byte	(crc_w - crc_t)/2
		.byte	(crc_b - crc_t)/2
		.byte	(crc_h - crc_t)/2
		.byte	(crc_b - crc_t)/2
		// WORDS
crc_w:	ldr		r3, [r0], #4
		subs	lr, #1
		str		r3, [r2]
		bne.n	crc_w
		// Tail in BYTEs
crc_r:	ands	r1, r1, #3						// r1 - number of bytes to move
		beq.n	crc_e
		ldr		r3, [r2, #8]					// r3 - CRC->CR regster value
		bfc		r3, #6, #1						// Set mode to BYTES
		str		r3, [r2, #8]					// Re-write CR
		// Write by BYTES
crc_f:	ldrb	r3, [r0], #1
		subs	r1, #1
		strb	r3, [r2]
		bne.n	crc_f
crc_e:	ldr		r0, [r2]						// Get final CRC
		pop		{pc}
		// HALFWORDS
crc_h:	ldrh	r3, [r0], #2
		ldrh	r12,[r0], #2
		subs	lr, #1
		orr		r3, r3, r12, LSL #16
		str		r3, [r2]
		bne.n	crc_h
		b.n		crc_r
		// BYTES
crc_b:	push	{r4}
crc_z:	ldrb	r3, [r0], #1
		ldrh	r4, [r0], #2
		ldrb	r12,[r0], #1
		subs	lr, #1
		orr		r3, r3, r4, LSL #8
		orr		r3, r3, r12,LSL #24
		str		r3, [r2]
		bne.n	crc_z
		pop		{r4}
		b.n		crc_r

