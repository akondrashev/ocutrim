#ifndef __OS_H__
#define __OS_H__
#include <stm32h7xx_hal.h>
#include "os_config.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define  TriggerPendSV()            SCB->ICSR = 1 << 28

typedef uint32_t  OS_STK ;

#define OS_THREAD_STACK             __attribute__((aligned(8))) OS_STK
#define RAM_FNC                     __attribute__((section(".fncram")))
#define INLINE                      __attribute__((always_inline)) inline
#define NO_INLINE                   __attribute__ ((noinline))
#define NO_CACHE                    __attribute__((section(".ram_d2"))) __attribute__((aligned(4)))

#define FALSE                       0
#define TRUE                        1

#define  OS_MAX_THREADS             33

// Thread timer status bits (sleep and waiting objects)
#define OS_TIMER_FLAG_RUNNING       0x1
#define OS_TIMER_FLAG_EXPIRED       0x2

#define INFINITY                    0xFFFFFFFF

#define DMA_BUFFER(t, v)             __attribute__((aligned(32))) __attribute__((section(".ram_d2"))) t v

typedef uint32_t  time__t ;
extern volatile time__t   _T_ ;
extern volatile uint32_t  TimeMS ;

#define GetTickCount()              TimeMS


static INLINE time__t OS_time(time__t *t)
   {
   if (t) *t = _T_ ;
   return _T_ ;
   }


typedef struct
   {
   volatile uint32_t  timer_flag ;
   volatile uint32_t  timer_ticks ;
   } OS_TIMER ;

typedef void (*THREAD_FNC)(void *context) ;

#define  OS_SEM_ACTIVE           0xACACACAC
typedef struct
   {
   volatile uint32_t  active ;
   volatile uint32_t  queue ;
   volatile uint32_t  value ;
   } OS_SEM ;

#define  OS_MUTEX_ACTIVE         0x55555555
typedef struct
   {
   volatile uint32_t  active ;
   volatile uint32_t  queue ;
   volatile uint16_t  owner ;
   volatile uint16_t  count ;
   } OS_MUTEX ;

#define  OS_EVENT_ACTIVE         0xCACACACA
#define  OS_EVENT_FLAG_SET       0x1
#define  OS_EVENT_FLAG_MANUAL    0x2
typedef struct
   {
   volatile uint32_t  active ;
   volatile uint32_t  queue ;
   volatile uint32_t  flags ;
   } OS_EVENT ;

typedef union
   {
   OS_SEM   Sem ;
   OS_MUTEX Mtx ;
   OS_EVENT Evt ;
   } OS_SYNC ;

typedef OS_SYNC*  HANDLE ;

// Queue object (dynamically allocated from heap)
#define  OS_QUEUE_ACTIVE         0xAAAAAAAA
typedef struct
   {
   volatile uint32_t  active ;
   OS_SYNC  *QueuePostSem ;
   OS_SYNC  *QueueReadSem ;
   uint16_t  max_elements ;            // Max queue len
   uint16_t  queue_count ;             // Number of elements currently in the queue
   uint16_t  readIdx ;                 // Peak index
   uint16_t  writeIdx ;                // Insert index
   void*     data[1] ;                 // Queue data: array of length max_elements
   } OS_QUEUE ;

typedef OS_QUEUE*  HQUEUE ;

// Kernel control structure
typedef struct
   {
   volatile uint16_t  CurrentThread ;              // Cannot change offset from this to sp_val (used in context.s)  <---+
   volatile uint16_t  SchedLockCnt ;               // Scheduler lock counter                                            |
   volatile uint32_t  ActiveThreads ;              // Mask of ready to run threads                                      |
   volatile uint32_t  LockedThreadMask ;           // Thread that locked scheduler                                      |
   volatile uint32_t  StartedThreads ;             // Mask of started threads                                           |
   volatile uint32_t  TimeWaitThreads ;            // Mask of threads that suspended on wait timers                     |
   volatile uint32_t *sp_val[OS_MAX_THREADS] ;     // Array of stack pointers for threads                           <---+
   THREAD_FNC         stop_fnc[OS_MAX_THREADS] ;   // Array of thread termination functions
   uint32_t          *arg[OS_MAX_THREADS] ;        // Array of thread arguments
   uint32_t           ticks[OS_MAX_THREADS] ;      // Array of sleep timers for threads
   OS_SYNC            OS_Events[OS_MAX_EVENTS] ;   // Synchronization objects
   } OS_RUN ;

extern OS_RUN    OS_Run ;
extern volatile uint32_t _primask_level ;

static INLINE uint16_t OS_GetCurrentThreadID(void)
   {
   return OS_Run.CurrentThread ;
   }

// Disable interrupts, if they are enabled, and increase nested level
static INLINE uint32_t cs_enter(void)
   {
   register uint32_t  level ;

   __disable_irq() ;
   level = _primask_level + 1 ;
   _primask_level = level ;
   return level ;
   }


// Enable interrupts, if nested level went back to 0. Maybe unsafe, if executed outside of critical section
static INLINE uint32_t cs_exit(void)
   {
   register uint32_t  level = _primask_level ;

   if (level)
      {
      level-- ;
      _primask_level = level ;
      } ;
   if (0 == level) __enable_irq() ;
   return level ;
   }


int     OS_Start(uint16_t prio) ;
void    OS_Reset(void) ;
int     OS_ThreadStart(THREAD_FNC pfnc, uint16_t prio, OS_STK *sp_val, size_t size, void *context, THREAD_FNC exit_fnc) ;
int     OS_ThreadStop(uint16_t prio) ;
int     OS_Sleep(uint32_t ticks) ;

HANDLE  OS_SemCreate(uint32_t count, int *perr) ;
int     OS_SemPend(HANDLE pSem, uint32_t timeout) ;
int     OS_SemPost(HANDLE pSem) ;
int     OS_SemReset(HANDLE pSem) ;
int     OS_SemDelete(HANDLE pSem) ;

HANDLE  OS_EventCreate(uint8_t bSet, uint8_t bManual, int *perr) ;
int     OS_EventReset(HANDLE pEvt) ;
int     OS_EventSet(HANDLE pEvt) ;
int     OS_EventWait(HANDLE pEvt, uint32_t timeout) ;
int     OS_EventDelete(HANDLE pEvt) ;

HANDLE  OS_MutexCreate(uint32_t bOwner, int *perr) ;
int     OS_MutexClaim(HANDLE pMtx, uint32_t timeout) ;
int     OS_MutexReleaseDrop(HANDLE pSem, uint32_t bDrop) ;
int     OS_MutexDelete(HANDLE pMtx) ;

#define OS_MutexRelease(h)    OS_MutexReleaseDrop(h, 0)
#define OS_MutexDrop(h)       OS_MutexReleaseDrop(h, 1)

#ifdef _TEST_
HQUEUE  OS_QueueCreate(uint32_t maxElements, int *perr) ;
int     OS_QueueDelete(HQUEUE hQueue) ;
int     OS_QueuePost(HQUEUE hQueue, void *pElem, uint32_t timeout) ;
void   *OS_QueueGet(HQUEUE hQueue, uint32_t timeout, int *perr) ;
#endif

uint16_t OS_SchedLock(void) ;
uint16_t OS_SchedUnLock(void) ;
void     OS_TimeSet(struct tm *ptm) ;

// RTC
uint32_t RTC_Init(uint32_t RTC_CLOCK_SOURCE) ;
uint32_t RTC_Set(struct tm *ptm) ;
uint32_t RTC_Get(struct tm *ptm) ;

#define  OS_ERR_TIMEOUT            -1
#define  OS_ERR_OK                  0
#define  OS_ERR_INVALID_PRIO        1
#define  OS_ERR_ALREADY_STARTED     2
#define  OS_ERR_NO_MORE_SYNCS       3
#define  OS_ERR_INVALID_HANDLE      4
#define  OS_ERR_MUTEX_OWNERSHIP     5
#define  OS_ERR_OBJECT_DELETED      6
#define  OS_ERR_INVALID_ARGUMENT    7
#define  OS_ERR_OUT_OF_MEMORY       8
#define  OS_ERR_QUEUE_OWNERSHIP     9
#define  OS_ERR_QUEUE_EMPTY         10
#define  OS_ERR_ALREADY_TERMINATED  11
#define  OS_ERR_CRITICAL_SECTION    12


#define  GetTickCount()             TimeMS

static INLINE uint32_t TicksSince(uint32_t ms)
   {
   register int32_t  delta = TimeMS - ms ;
   if (delta < 0) delta = -delta ;
   return delta ;
   }

#include "timers.h"
#include "dbg.h"
#endif
